<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stefandebruin\JsonApi\Controllers\JsonController;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;
use Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface;
use Stefandebruin\PouleSystem\Requests\CreateUpdateGroupRequest;
use Stefandebruin\PouleSystem\Requests\ConnectTeamToGroupsRequest;
use Stefandebruin\PouleSystem\Services\TranslationService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GroupController extends JsonController
{
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;
    /**
     * @var TranslationService
     */
    private $translationService;

    /**
     * GroupController constructor.
     * @param GroupRepositoryInterface $groupRepository
     * @param TranslationService $translationService
     */
    public function __construct(
        GroupRepositoryInterface $groupRepository,
        TranslationService $translationService
    )
    {
        $this->groupRepository = $groupRepository;
        $this->translationService = $translationService;
    }

    /**
     * @return AbstractJsonApiCollection
     */
    public function index(Request $request)
    {
        $model = app(Group::class);

        return new AbstractJsonApiCollection(
            app()->make(FilterCollectionService::class)->filter($model)->jsonPaginate()
        );
    }

    /**
     * @param Group $group
     * @return AbstractJsonApi
     */
    public function show(Group $group)
    {
        return new AbstractJsonApi($group);
    }

    /**
     * TODO-stefan
     * @param Competition $competition
     * @param Phase $phase
     * @return AbstractJsonApiCollection
     */
    public function phaseIndex(Competition $competition, Phase $phase)
    {
        $phase->load('groups');

        /** @var Group $group */
        foreach ($phase->getRelation('groups') as $group) {
            /** @var Fixture $fixture */
            foreach ($group->fixtures as $fixture) {
                $fixture->load('settings');
                $fixture->settings->fixture_id = $fixture->id;
            }
        }

        return new AbstractJsonApiCollection($phase->getRelation('groups'));
    }

    /**
     * @param Competition $competition
     * @return AbstractJsonApiCollection
     */
    public function competitionShow(Competition $competition)
    {
        return new AbstractJsonApiCollection($competition->groups()->jsonPaginate());
    }

    /**
     * TODO-stefan
     * @param Competition $competition
     * @param Phase $phase
     * @return AbstractJsonApi
     */
    public function phaseGroups(Competition $competition, Phase $phase)
    {
        return new AbstractJsonApi($phase->groups);
    }

    public function phaseRelationList(Phase $phase)
    {
        return new AbstractJsonApiCollection($phase->groups()->jsonPaginate());
    }

    /**
     * @param CreateUpdateGroupRequest $request
     * @return AbstractJsonApi
     */
    public function store(CreateUpdateGroupRequest $request)
    {
        return parent::createModel($request);
    }

    /**
     * @param CreateUpdateGroupRequest $request
     * @param Group $group
     * @return AbstractJsonApi
     */
    public function update(CreateUpdateGroupRequest $request, Group $group)
    {
        $group->fill($request->allAttributes());
        $group = $this->groupRepository->save($group);
        return new AbstractJsonApi($group);
    }

    /**
     * @param Group $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Group $group)
    {
        $group->load('allFixtures');
        if (count($group->getRelation('allFixtures')) === 0) {
            $group->forceDelete();
            return response()->json()->setStatusCode(204);
        }
        throw new BadRequestHttpException('Group has connected fixtures.');
    }

    private function getNewGroupId($postData, $homeTeam, $awayTeam)
    {
        $incorrect = false;
        foreach ($postData as $postedGroupData) {
            $check = 0;

            if (in_array($homeTeam, $postedGroupData['teams'])) {
                $check++;
            }
            if (in_array($awayTeam, $postedGroupData['teams'])) {
                $check++;
            }

            if ($check === 2) {
                return ['found' => true, 'id' => $postedGroupData['group_id']];
            } elseif ($check === 1) {
                $incorrect = true;
            }
        }
        return ['found' => false, 'incorrect' => $incorrect];
    }

    public function storeTeamConnection(ConnectTeamToGroupsRequest $request, $competitionId, $phaseId)
    {
        /** @var Competition $competition */
        $competition = Competition::find($competitionId);

        if (!$competition instanceof Competition) {
            return response()->json(['message' => 'competition not found'], 404);
        }

        $success = true;
        $extraMessage = [];
        try {
            /** @var Phase $phase */
            $phase = $competition->phases->where('id', $phaseId)->first();
            if (!$phase instanceof Phase) {
                return response()->json(['message' => 'phase not found in the competition'], 404);
            }

            $movedFixtures = [];
            $unposibleCompination = [];
            PouleSystemServiceProvider::setLoadFixtureRelation(true);
            /** @var Phase $storedData */
            $storedData = clone $phase;
            $storedData->load('fixtures');
            $storedData->load('groups');

            /** @var Fixture $fixture */
            foreach ($storedData->fixtures as $fixture) {
                $found = false;
                foreach ($request->all() as $postedGroupData) {
                    if ($postedGroupData['group_id'] === $fixture->group_id) {
                        $check = 0;

                        if (in_array($fixture->home_team, $postedGroupData['teams'])) {
                            $check++;
                        }
                        if (in_array($fixture->away_team, $postedGroupData['teams'])) {
                            $check++;
                        }

                        if ($check === 1) {
                            $unposibleCompination[] = [
                                'fixture' => $fixture,
                                'newGroup' => $postedGroupData['group_id']
                            ];
                        } elseif ($check === 0) {
                            $groupId = $this->getNewGroupId($request->all(), $fixture->home_team, $fixture->away_team);
                            if ($groupId['found']) {
                                $movedFixtures[$fixture->id] = ['group_id' => $groupId['id'], 'phase_id' => $phase->id];
                            } else {
                                if ($groupId['incorrect']) {
                                    $unposibleCompination[] =
                                        [
                                            'fixture' => $fixture,
                                            'newGroup' => $postedGroupData['group_id']
                                        ];
                                }
                                break;
                            }
                        }

                        $found = true;
                        break;
                    }
                }

//                if (!$found) {
//                    $groupId = $this->getNewGroupId($request->all(), $fixture->home_team, $fixture->away_team);
//                    if ($groupId['found']) {
//                        $movedFixtures[$fixture->id] = ['group_id' => $groupId['id'], 'phase_id' => $phase->id];
//                    } else {
//                        if ($groupId['incorrect']) {
//                            $unposibleCompination[] = [
//                                'fixture' => $fixture,
//                                'newGroup' => $postedGroupData['group_id']
//                            ];
//                        } else {
//                            $movedFixtures[$fixture->id] = ['group_id' => null, 'phase_id' => null];
//                        }
//                    }
//                }
            }

            if (count($unposibleCompination) > 0) {
                $message = [];
                foreach ($unposibleCompination as $item) {
                    $message[$item['newGroup']] = ['The teams combination for this group is invalid'];
                }
                return response()->json(
                    [
                        'message' => 'The given data was invalid.',
                        'validation' => 'teamGroupFixture',
                        'errors' => $message
                    ],
                    422
                );
            }

            $index = 1;
            foreach ($request->all() as $groupData) {
                $teams = [];
                foreach ($groupData['teams'] as $team) {
                    $teams[$team] = ['competition_id' => $competitionId, 'phase_id' => $phaseId];
                }

                $index++;
            }

            $notVisible = [];
            foreach ($movedFixtures as $fixtureId => $movedFixture) {
                /** @var Fixture $fixture */
                $fixture = $storedData->where('id', $fixtureId)->first();
                $fixture->fill($movedFixture);

                if (is_null($movedFixture['group_id'])) {
                    $notVisible[] = $fixture;
                }
            }

            if (count($notVisible) > 0) {
                $extraMessage['code'] = 'matchesNotVisible';
                $extraMessage['message'] = $notVisible;
            }
        } catch (\Exception $exception) {
            $success = false;
            $extraMessage['error']['message'] = $exception->getMessage();
            $extraMessage['error']['line'] = $exception->getLine();
        }

        return response()->json(['success' => $success, 'info' => $extraMessage]);
    }
}
