<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * TODO-stefan aanpassen naar een resource an keys vast leggen wat wel en niet. alleen basis/nodige informatie
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $settings = array_except(config('poulesystem'), ['domain']);
        return response()->json($settings);
    }
}
