<?php

namespace Stefandebruin\PouleSystem\Test;

use Illuminate\Contracts\Console\Kernel;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return ['Stefandebruin\PouleSystem\PouleSystemServiceProvider'];
    }
}
