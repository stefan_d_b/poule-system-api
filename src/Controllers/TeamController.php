<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;
use Stefandebruin\JsonApi\Controllers\JsonController;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Team;
use Stefandebruin\PouleSystem\Requests\UpdateTeamRequest;

/**
 * Class TeamController
 * @package Stefandebruin\PouleSystem\Controllers
 */
class TeamController extends JsonController
{
    /**
     * @return AbstractJsonApiCollection
     */
    public function index()
    {
        $model = app(Team::class);

        return new AbstractJsonApiCollection(
            app()->make(FilterCollectionService::class)->filter($model)->jsonPaginate()
        );
    }

    /**
     * @param UpdateTeamRequest $request
     * @return AbstractJsonApi
     */
    public function store(UpdateTeamRequest $request)
    {
        return parent::createModel($request);
    }

    /**
     * @param Team $team
     * @return \Stefandebruin\JsonApi\Resources\AbstractJsonApi
     */
    public function show(Team $team)
    {
        return new AbstractJsonApi($team);
    }

    /**
     * @param UpdateTeamRequest $request
     * @param Team $team
     * @return \Stefandebruin\JsonApi\Resources\AbstractJsonApi
     */
    public function update(UpdateTeamRequest $request, Team $team)
    {
        $team->fill($request->allAttributes());
        $team->save();
        return new AbstractJsonApi($team);
    }

    public function groupsRelationList(Group $group)
    {
        return new AbstractJsonApiCollection($group->teams()->jsonPaginate());
    }
}
