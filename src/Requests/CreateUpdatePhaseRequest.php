<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Stefandebruin\JsonApi\Requests\JsonApiRequest;

class CreateUpdatePhaseRequest extends JsonApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function attributeRules()
    {
        return [
            'title' => [
                'unique:phases,title,'. $this->phase_id
            ],
        ];
    }
}