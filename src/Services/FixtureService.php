<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 26-06-18
 * Time: 21:15
 */

namespace Stefandebruin\PouleSystem\Services;

use Carbon\Carbon;
use Stefandebruin\PouleSystem\Models\Competition;
use Illuminate\Database\Eloquent\Collection;
use Stefandebruin\PouleSystem\Models\Fixture;

class FixtureService
{
    /**
     * @param Competition $competition
     * @return Collection
     */
    public function getAllActiveFixtures(Competition $competition)
    {
        $start = Carbon::now()->subMinutes(20)->setTimezone('UTC');
        $end = Carbon::now()->addMinutes(30)->setTimezone('UTC');

        $todayFixtures = $competition->fixtures
            ->where('date', '>=', $start->format("Y-m-d H:i:s"))
            ->Where('date', '<=', $end->format("Y-m-d H:i:s"));
//            ->whereNotIn('status', [Fixture::STATUS_FINISHED, Fixture::STATUS_CANCELED]);

        $inPLayFixtures = $competition->fixtures->where('status', Fixture::STATUS_IN_PLAY);

        return $todayFixtures->merge($inPLayFixtures);
    }
}
