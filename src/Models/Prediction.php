<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

namespace Stefandebruin\PouleSystem\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use Stefandebruin\PouleSystem\Facades\OverrideModels;
use Stefandebruin\PouleSystem\Scopes\PredictionScope;

/**
 * Class Prediction
 *
 * @method LengthAwarePaginator jsonPaginate
 * @package Stefandebruin\PouleSystem\Models
 * @property int $id
 * @property int $fixture_id
 * @property int|null $user_id
 * @property int $home_goals
 * @property int $away_goals
 * @property int $halftime_home_goals
 * @property int $halftime_away_goals
 * @property int $extratime_home_goals
 * @property int $extratime_away_goals
 * @property int $penalties_home
 * @property int $penalties_away
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Stefandebruin\PouleSystem\Models\Fixture $fixture
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction currentUser()
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereExtratimeAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereExtratimeHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereFixtureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereHalftimeAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereHalftimeHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction wherePenaltiesAway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction wherePenaltiesHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prediction whereUserId($value)
 * @mixin \Eloquent
 */
class Prediction extends Score
{
    protected $table = 'scores';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PredictionScope());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\User
     */
    public function user()
    {
        return $this->belongsTo(OverrideModels::getUserModel(), 'user_id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeCurrentUser(Builder $query)
    {
        return $query->where('user_id', \Auth::id());
    }

    public function fixture()
    {
        return $this->belongsTo('Stefandebruin\PouleSystem\Models\Fixture');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        \Log::debug("Prediction model");
        \Log::debug(get_class($this));
        \Log::debug($this->toArray());

//        $id = 0;
//        if (array_has($this->getAttributes(), 'id')) {
//            $id = $this->getAttribute('id');
//        }

        return route('poulesystem.predictions.show', $this->id);
    }
}
