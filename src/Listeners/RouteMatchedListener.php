<?php namespace Stefandebruin\PouleSystem\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Routing\Events\RouteMatched;
use Stefandebruin\PouleSystem\Services\DomainService;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 06-11-17
 * Time: 20:22
 */
class RouteMatchedListener
{
    /**
     * @param RouteMatched $info
     * @internal param \Illuminate\Routing\Events\RouteMatched $login
     */
    public function handle(RouteMatched $info)
    {
        $name = $info->route->getName();
        if (!in_array($name, config('poulesystem.domain.block-routes', []))) {
            $domain = app()->make(DomainService::class)->registerDomain();
        }
    }
}
