<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 22-11-17
 * Time: 19:49
 */
/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\Stefandebruin\PouleSystem\Models\Competition::class, function (Faker\Generator $faker) {
    return [
        'api_id' => $faker->randomNumber(3),
        'title' => $faker->title,
        'key' => $faker->randomElement(['DED', 'AAL', 'PL', 'ELC']),
        'year' => $faker->year,
        'visible' => 0,
        'type' => 2,
    ];
});

$factory->define(\Stefandebruin\PouleSystem\Models\Domain::class, function (Faker\Generator $faker) {
    return [
        'domain' => $faker->domainName,
        'enabled' => 0,
    ];
});

$factory->define(\Stefandebruin\PouleSystem\Models\Fixture::class, function (Faker\Generator $faker) {
    return [
        'api_id' => $faker->randomNumber(3),
        'matchday' => $faker->randomNumber(2),
        'date' => $faker->dateTime,
    ];
});

$factory->define(\Stefandebruin\PouleSystem\Models\Group::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->title,
        'system_created' => 1,
    ];
});


/**
 * @property Collection teams
 * @property Collection competitions
 * @property Collection phase
 * @property Collection fixtures
 * @property Collection translations
 */
$factory->define(\Stefandebruin\PouleSystem\Models\Phase::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->title,
        'system_created' => 1,
    ];
});


$factory->define(\Stefandebruin\PouleSystem\Models\Score::class, function (Faker\Generator $faker) {
    return [
        'user_id' => null,
        'fixture_id' => 0,
        'home_goals' => $faker->numberBetween(0,4),
        'away_goals' => $faker->numberBetween(0,4),
        'halftime_home_goals' => $faker->numberBetween(0,4),
        'halftime_away_goals' => $faker->numberBetween(0,4),
        'extratime_home_goals' => $faker->numberBetween(0,4),
        'extratime_away_goals' => $faker->numberBetween(0,4),
        'penalties_away' => 0,
        'penalties_home' => 0,
    ];
});


/**
 * @property Collection homeFixtures
 * @property Collection awayFixtures
 * @property Collection competitions
 * @property Collection groups
 * @property Collection translations
 */
$factory->define(\Stefandebruin\PouleSystem\Models\Team::class, function (Faker\Generator $faker) {
    return [
        'api_id' => $faker->randomNumber(4),
        'code' => null,
        'title' => $faker->title,
        'image' => null,
    ];
});