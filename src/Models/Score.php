<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Stefandebruin\JsonApi\JsonApi;
use Stefandebruin\JsonApi\Models\JsonModel;

/**
 * @method LengthAwarePaginator jsonPaginate
 * @property int|null user_id
 * @property int fixture_id
 * @property int home_goals
 * @property int away_goals
 * @property int halftime_home_goals
 * @property int halftime_away_goals
 * @property int extratime_home_goals
 * @property int extratime_away_goals
 * @property int penalties_home
 * @property int penalties_away
 * @property Fixture fixture
 * @property User user
 */
abstract class Score extends JsonModel
{
    use JsonApi;

    protected $fillable = [
        'fixture_id',
        'home_goals',
        'away_goals',
        'halftime_home_goals',
        'halftime_away_goals',
        'extratime_home_goals',
        'extratime_away_goals',
        'penalties_home',
        'penalties_away',
    ];

    protected $casts = [
        'home_goals' => 'integer',
        'away_goals' => 'integer',
        'halftime_home_goals' => 'integer',
        'halftime_away_goals' => 'integer',
        'extratime_home_goals' => 'integer',
        'extratime_away_goals' => 'integer',
        'penalties_home' => 'integer',
        'penalties_away' => 'integer',
    ];

    protected $hidden = [
        'fixture_id',
        'user_id'
    ];

    protected $attributes = [
        'halftime_home_goals' => 0,
        'halftime_away_goals' => 0,
    ];

    protected $hiddenRelations = ['user'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Stefandebruin\PouleSystem\Models\Fixture
     */
    public function fixture()
    {
        return $this->belongsTo('Stefandebruin\PouleSystem\Models\Fixture', 'fixture_id');
    }

    /**
     * TODO-stefan verplaatsen naar een servive en verkleinen
     * @param Score $result
     * @param $key
     * @return \Illuminate\Config\Repository|int|mixed
     */
    public function calculate(Score $result, $key)
    {
        $prefix = "";
        if ($key !== "result") {
            if (!config('poulesystem.points' . $key . 'enabled', false)) {
                return 0;
            }

            $prefix = "_" . $key;
        }

        $homeKey = $prefix . "home_goals";
        $awayKey = $prefix . "away_goals";

        $score = 0;
        if (!is_null($result->$homeKey) && !is_null($result->$awayKey)) {
            if ($this->$homeKey == $this->$homeKey &&
                $this->$awayKey == $result->$awayKey) {
                //uitslag goed voorspeld
                $score += config('poulesystem.points.' . $key . '.correct', 0);
            } elseif ($this->$homeKey > $this->$awayKey &&
                $result->$homeKey > $result->$awayKey) {
                //goede winnaar thuis ploeg
                $score += config('poulesystem.points.' . $key . '.winner', 0);
            } elseif ($this->$homeKey < $this->$awayKey &&
                $result->$homeKey < $result->$awayKey) {
                //goede winnaar uit ploeg
                $score += config('poulesystem.points.' . $key . '.winner', 0);
            } elseif ($this->$homeKey == $this->$awayKey &&
                $result->$homeKey == $result->$awayKey) {
                //Gelijk spel voorspeld
                $score += config('poulesystem.points.' . $key . '.winner', 0);
            }
        }

        return $score;
    }

    /**
     * TODO-stefan verplaatsen naar een servive en verkleinen
     * @param Score $result
     * @return \Illuminate\Config\Repository|int|mixed
     */
    public function calculatePenalties(Score $result)
    {
        $score = 0;
        if (config('poulesystem.penaltyModes') == 'winner') {
            if ($this->penalties_home > $this->penalties_away &&
                $result->penalties_home > $result->penalties_away) {
                $score += config('poulesystem.points.penalties.corrects');
            } elseif ($this->penalties_home < $this->penalties_away &&
                $result->penalties_home < $result->penalties_away) {
                $score += config('poulesystem.points.penalties.corrects');
            }
        } elseif (config('poulesystem.penaltyModes') == "score") {
            if ($this->penalties_home == $result->penalties_home &&
                $this->penalties_away == $result->penalties_away) {
                $score += config('poulesystem.points.penalties.corrects');
            } elseif ($this->penalties_home > $this->penalties_away &&
                $result->penalties_home > $result->penalties_away) {
                $score += config('poulesystem.points.penalties.winner');
            } elseif ($this->penalties_home < $this->penalties_away &&
                $result->penalties_home < $result->penalties_away) {
                $score += config('poulesystem.points.penalties.winner');
            }
        }

        return $score;
    }
}
