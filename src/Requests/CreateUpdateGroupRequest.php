<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Requests;

use Illuminate\Validation\Rule;
use Stefandebruin\JsonApi\Requests\JsonApiRequest;

class CreateUpdateGroupRequest extends JsonApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributeRules()
    {
        return [
            'title' => [
                Rule::unique('groups', 'title')->ignore(request()->group_id),
            ],
            'key' => ['string']
        ];
    }
}