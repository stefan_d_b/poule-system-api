<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Routing\ImplicitRouteBinding;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Stefandebruin\PouleSystem\Controllers\CompetitionController;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Domain;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Services\DomainService;
use Stefandebruin\PouleSystem\Services\TranslationService;


/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 15-12-17
 * Time: 17:44
 */

class CompetitionControllerTest extends \Stefandebruin\PouleSystem\Test\TestCase{

    /**
     * @var TranslationService
     */
    private $translationService;

    /**
     * @var CompetitionController
     */
    private $competitionController;

    /**
     * @var Competition
     */
    private $competitionModel;

    /**
     * @var DomainService
     */
    private $domainService;

    /**
     * @var User
     */
    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->competitionModel = Mockery::mock(Competition::class.'[all,visibleForUser]');
        $this->app->instance(Competition::class, $this->competitionModel);

        $this->translationService = Mockery::mock(TranslationService::class);
        $this->competitionController = new CompetitionController($this->translationService);

        $this->setUpDomainService();
        $this->makeUser();
        $this->withoutMiddleware([\App\Http\Middleware\JWTAuthGetUserFromToken::class, \Stefandebruin\PouleSystem\Middleware\PouleSystem::class]);
    }

    protected function tearDown()
    {
        $this->translationService = NULL;
        $this->competitionController = NULL;
    }

    public function testGetIndexAllVisibleForUser(){
        $collection = $this->buildCompetitionCollection();
        $builder = Mockery::mock(\Illuminate\Database\Eloquent\Builder::class);
        $builder->shouldReceive('paginate')->once()->andReturn($collection);
        $this->competitionModel
            ->shouldReceive('visibleForUser')
            ->once()
            ->andReturn($builder);
        $response = $this->actingAs($this->user)->getJson('/api/competitions', $this->getHeaders());
//
        $response->assertStatus(200);
        $response->assertJson($this->buildResponseFromCollection($collection));
    }
//
    public function testGetIndexAll(){
        $collection = $this->buildCompetitionCollection();
        $this->competitionModel
            ->shouldReceive('visibleForUser')
            ->never();
        $this->competitionModel
            ->shouldReceive('paginate')
            ->once()
            ->andReturn($collection);
        $response = $this->actingAs($this->user)
            ->getJson(
            '/api/competitions?filter[scope]=all',
            $this->getHeaders()
        );

        $response->assertStatus(200);
        $response->assertJson($this->buildResponseFromCollection($collection));
    }

    public function testGetIndexAllWithSpecificColumns(){
        $collection = $this->buildCompetitionCollection();
        $builder = Mockery::mock(\Illuminate\Database\Eloquent\Builder::class);
        $builder->shouldReceive('paginate')->once()->andReturn($collection);
        $this->competitionModel
            ->shouldReceive('visibleForUser')
            ->once()
            ->andReturn($builder);
        $response = $this->actingAs($this->user)
            ->getJson(
                '/api/competitions?fields[competition]=title,year,type',
                $this->getHeaders()
            );

        $response->assertStatus(200);
        $response->assertJson($this->buildResponseFromCollection($collection, ['title', 'year', 'type']));
    }

    public function testShowACompetition(){
        $collection = $this->buildCompetitionCollection();

        $competition = $collection->first();

        $route = app()->make(\Illuminate\Routing\Route::class);
        $route->setParameter('competition', $competition);
        $this->withoutMiddleware([SubstituteBindings::class]);

        $response = $this->actingAs($this->user)
            ->getJson(
                '/api/competitions/'. $competition->id,
                $this->getHeaders()
            );

        dd($response);
        $response->assertStatus(200);
        $response->assertJson($this->buildResponseFromSingle($competition));
    }


    public function testUpdateTheVisibilityOfACompetition(){
//        $collection = $this->buildCompetitionCollection();
//        $competition = $collection->first();
//
//        $postData = [
//            'type' => 'competitions',
//            'id' => $competition->id,
//            'attributes' => [
//                'visible' => true
//            ],
//        ];
//
//        $this->patchJson(
//            '/api/competitions/'.$competition->id
//        );
    }


    public function testShowANotExistingCompetition(){

    }

    public function testUpdateACompetition(){

    }



    private function setUpDomainService(){
        $this->domainService = Mockery::mock(Competition::class);

        $domain = factory(Domain::class)->states('id')->make();
        $this->domainService
            ->shouldReceive('registerDomain')
            ->once()
            ->andReturn($domain);

        app()->singleton('pouleDomain', function() use($domain){
            return $domain;
        });

        $this->app->instance(DomainService::class, $this->domainService);
    }

    private function makeUser(){
        $this->user = factory(User::class)->states(['id'])->make();
    }

    private function getHeaders(){
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    private function buildResponseFromCollection($competitions, $columns = []){
        $responseData = [];
        /** @var Competition $competition */
        foreach ($competitions as $competition){
            $attributes = [
                'title' => $competition->title,
                'key' => $competition->key,
                'year' => $competition->year,
                'visible' => $competition->visible,
                'type' => $competition->type,
                'typeLabel' => $competition->typeLabel,
                'created_at' => $competition->created_at->toIso8601String(),
                'updated_at' => $competition->updated_at->toIso8601String(),
            ];

            $attributes = count($columns) > 0 ? array_only($attributes, $columns) : $attributes;

            $responseData[] = [
                'type' => 'competitions',
                'id' => $competition->id,
                'attributes' => $attributes,
                'links' => [
                    'self' => route('poulesystem.competitions.show', ['competition' => $competition->id])
                ]
            ];


        }
        return ['data' => $responseData];
    }

    private function buildResponseFromSingle($competition, $columns = []){
        /** @var Competition $competition */
        $attributes = [
            'title' => $competition->title,
            'key' => $competition->key,
            'year' => $competition->year,
            'visible' => $competition->visible,
            'type' => $competition->type,
            'typeLabel' => $competition->typeLabel,
            'created_at' => $competition->created_at->toIso8601String(),
            'updated_at' => $competition->updated_at->toIso8601String(),
        ];

        $attributes = count($columns) > 0 ? array_only($attributes, $columns) : $attributes;

        $responseData = [
            'type' => 'competitions',
            'id' => $competition->id,
            'attributes' => $attributes,
            'links' => [
                'self' => route('poulesystem.competitions.show', ['competition' => $competition->id])
            ]
        ];
        return ['data' => $responseData];
    }

    private function buildCompetitionCollection(){
        $date = Carbon::create(2018, 5, 28, 0, 0, 0);

        $id = 13;
        $competitions = factory(Competition::class, 2)
            ->make()
            ->each(function($competition) use($date){
                $competition->visible = array_random([0,1]);
                $competition->created_at = $date->addWeeks(rand(1, 52));
                $competition->updated_at = $date->addWeeks(rand(1, 52));
            });

        foreach($competitions as $competition){
            $competition->id = $id;
            $id++;
        }
        return $competitions;
    }
}