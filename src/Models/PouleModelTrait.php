<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 28-12-17
 * Time: 15:37
 */

trait PouleModelTrait
{
    public function getPersistentRelations()
    {
        if (property_exists($this, 'persistentRelations')) {
            return $this->persistentRelations;
        }
        return [];
    }

    /**
     * @return string
     */
    public function getResourceClass()
    {
        if (property_exists($this, 'resource')) {
            return $this->resource;
        }
        return null;
    }
}
