<?php

namespace Stefandebruin\PouleSystem\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Stefandebruin\PouleSystem\Models\Competition;

class PodiumUpdate implements ShouldBroadcast
{
    use SerializesModels;
    /**
     * @var Competition
     */
    private $competition;

    /**
     * @var array
     */
    public $updatedRows;


    public $virtual;

    /**
     * Create a new event instance.
     *
     * @param Competition $competition
     * @param $updatedRows
     * @param $virtual
     * @internal param $updates
     */
    public function __construct(Competition $competition, $updatedRows, $virtual)
    {
        $this->competition = $competition;
        $this->updatedRows = $updatedRows;
        $this->virtual = $virtual;

        echo 1234;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('competition.'.$this->competition->id);
    }

    public function broadcastAs()
    {
        return 'podium.update';
    }
}
