<?php

namespace App\Notifications;

use App\NotificationSettings;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewTopUser extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    private $newUser;
    /**
     * @var User
     */
    private $oldUser;

    /**
     * Create a new notification instance.
     *
     * @param User $newUser
     * @param User $oldUser
     */
    public function __construct(User $newUser, User $oldUser)
    {
        $this->newUser = $newUser;
        $this->oldUser = $oldUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->getNotificationProviders(NotificationSettings::NEW_TOP_USER);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
