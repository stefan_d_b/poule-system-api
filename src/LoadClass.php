<?php namespace Stefandebruin\PouleSystem;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 21-07-17
 * Time: 17:24
 */

class LoadClass
{
    public function loadModel($modelClass)
    {
        return $this->findConfig('models', $modelClass);
    }

    public function loadNotification($notificationClass)
    {
        return $this->findConfig('notifications', $notificationClass);
    }

    public function findConfig($model, $class)
    {
        $nameOnly = $this->getClassName($class);
        $path = ('poulesystem.override.'.$model.'.'.$nameOnly);
        $foundClass = config($path, $class);
        return class_exists($foundClass) ? $foundClass : $class;
    }

    private function getClassName($modelClass)
    {
        $arrayClass = explode("\\", $modelClass);
        return last($arrayClass);
    }
}
