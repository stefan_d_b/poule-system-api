<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:42
 */

namespace Stefandebruin\PouleSystem\Console\Import;

use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Services\ApiGateway\ApiGatewayInterface;
use Stefandebruin\PouleSystem\Services\ApiGatewayFactory;

class Competitions extends Command
{
    /**
     * The name of the console command.
     *
     * @var string
     */
    protected $name = 'poulesystem:import:competitions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the competitions';

    /**
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        /** @var ApiGatewayInterface $gateway */
        foreach (ApiGatewayFactory::all() as $gateway) {
            $gateway->getLeagues($this->output);
        }
    }
}
