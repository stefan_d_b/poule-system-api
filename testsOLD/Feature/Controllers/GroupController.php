<?php

use Stefandebruin\PouleSystem\Controllers\GroupController;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface;
use Stefandebruin\PouleSystem\Services\TranslationService;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 18-12-17
 * Time: 07:42
 */

class GroupControllerTest extends \Stefandebruin\PouleSystem\Test\TestCase{
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepositoryInterface;

    /**
     * @var TranslationService
     */
    private $translationService;

    /**
     * @var GroupController
     */
    private $groupController;

    /**
     * @var Group
     */
    private $groupModel;

    /**
     * @var \Illuminate\Support\Collection
     */
    private $groups;

    /**
     * @var Competition
     */
    private $competition;

    /**
     * @var Phase
     */
    private $phase;

    protected function setUp()
    {
        parent::setUp();
        $this->groupModel = Mockery::mock(Group::class.'[all]');
        $this->app->instance(Group::class, $this->groupModel);

        $this->competitionModel = Mockery::mock(Group::class);
        $this->app->instance(Competition::class, $this->competitionModel);

        $this->groupRepositoryInterface = Mockery::mock(GroupRepositoryInterface::class);
        $this->translationService = Mockery::mock(TranslationService::class);
        $this->groupController = new GroupController($this->groupRepositoryInterface, $this->translationService);
        $this->_mockGroups();
        $this->_mockCompetition();
        $this->_mockPhase();
    }

    protected function tearDown()
    {
        $this->groupRepositoryInterface = NULL;
        $this->groupRepositoryInterface = NULL;
        $this->groupController = NULL;
    }

    private function _mockGroups(){
        $id = 21;
        $this->groups = factory(Group::class, 7)
            ->make()
            ->each(function($group){
                $group->system_created = 0;
            });

        foreach($this->groups as $group){
            $group->id = $id;
            $id++;
        }
    }

    public function _mockCompetition(){
        $this->competition = factory(Competition::class);
        $this->competition->id = 45;
    }

    public function _mockPhase(){
        $phase = app()->make(Phase::class, factory(Phase::class)->make()->getAttributes());
        $phase->id = 33;

        $this->phase = $phase;
    }

    public function testIndex(){
        $this->groupModel
            ->shouldReceive('all')
            ->once()
            ->andReturn($this->groups);

        $response = $this->groupController->index();
        $this->assertInstanceOf(\Stefandebruin\PouleSystem\Resources\Group::class, $response);
        $this->assertEquals($this->groups, $response->resource);
    }

    public function testPhaseGroups(){
//        $this->groupController->phaseGroups($this->competition, $this->phase);

        $this->assertTrue(true);
    }

    public function testStore(){
        $this->assertTrue(true);
    }

    public function testUpdate(){
        $this->assertTrue(true);
    }

    public function testConnect(){
        $this->assertTrue(true);
    }

    public function testStoreTeamConnection(){
        $this->assertTrue(true);
    }

    public function testDestroy(){
        $this->assertTrue(true);
    }
}