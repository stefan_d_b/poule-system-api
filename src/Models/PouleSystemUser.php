<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:17
 */

namespace Stefandebruin\PouleSystem\Models;

trait PouleSystemUser
{
    public function score()
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Score', 'user_id');
    }

    public function competitions()
    {
        return $this->belongsToMany('Stefandebruin\PouleSystem\Models\Competition', 'competition_user');
    }

    public function domains()
    {
        return $this->hasMany(Domain::class, 'id', 'user_id');
    }
}
