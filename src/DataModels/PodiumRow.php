<?php namespace Stefandebruin\PouleSystem\DataModels;

use App\User;
use Illuminate\Support\Str;
use Stefandebruin\JsonApi\Models\JsonModel;
use Stefandebruin\PouleSystem\Facades\OverrideModels;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 19-11-17
 * Time: 13:24
 * @property int userId
 * @property int points
 */
class PodiumRow extends JsonModel
{

    protected $fillable = [
        'user_id', 'points', 'matches', 'user', 'positionChangeDirection', 'positionChange', 'virtual'
    ];

    protected $appends = [
        'positionChange', 'pointsChange'
    ];

    protected $hidden = ['matches'];


//    public function user()
//    {
//        $this->attributes['user'] = ;
//        return $this;
//    }

    public function addMatch($fixtureId)
    {
        $this->attributes['matches'][] = $fixtureId;
    }

    public function addPoints($points)
    {
        $rowPoints = (int)$this->attributes['points'];
        $rowPoints += $points;
        $this->attributes['points'] = $rowPoints;
    }

    public function user()
    {
        if (!$this->relationLoaded('user')) {
            $this->setRelation('user', app()->make(OverrideModels::getUserModel())->find($this->user_id));
        }
        return $this->getRelation('user');
    }

    public function getPositionChangeAttribute(){
        return 0;
    }

    public function getPointsChangeAttribute(){
        return 0;
    }

}