<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Stefandebruin\PouleSystem\Exceptions\UserPredictionValidationException;

class SaveUserPredictionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            '*.fixture_id' => [
//                'required',
//                Rule::exists('fixtures', 'id')->where(function ($query) {
//                    if(request()->competition_id !== null){
//                        $query->where('competition_id', request()->competition_id);
//                    }
//                }),
//            ],
            '*.home_goals' => ['required', 'integer', 'min:0', 'max:30',],
            '*.away_goals' => ['required', 'integer', 'min:0', 'max:30',],
            '*.halftime_home_goals' => ['integer', 'min:0', 'max:30'],
            '*.halftime_away_goals' => ['integer', 'min:0', 'max:30'],
            '*.extratime_home_goals' => ['integer', 'min:0', 'max:30'],
            '*.extratime_away_goals' => ['integer', 'min:0', 'max:30'],
            '*.penalties_home' => ['penalty_required', 'integer', 'min:0', 'max:5'],
            '*.penalties_away' => ['penalty_required', 'integer', 'min:0', 'max:5'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'body.required'  => 'A message is required',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new UserPredictionValidationException($validator));
            //->errorBag($this->errorBag)
            //->redirectTo($this->getRedirectUrl());
    }
}