<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PodiumRow extends Resource
{
    /**
     * @var array
     */
    protected $primaryRelations = [
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->dataRoot = $dataRoot;
    }

    public function toArray($request)
    {
        $relations = $this->getRelationships($request);
        return [
            'type' => 'podiumRows',
            'id' => (string) sha1($request->competition->id.$this->resource->user_id),
            'attributes' => array_except($this->resource->toArray(), ['user', 'user_id']), //
//            'links' => [
//                'self' => route('poulesystem.podium.show', ['competition' => $request->competition->id])
//            ],
            'relationships' => $this->when(count($relations) > 0, $relations)
        ];
    }

    public function getRelationships($request){
        return [
            'user' => [
                'links' => [
                    'self' => 'google.nl'
                ],
                'data' => [
                    'type' => 'users',
                    'id' => (string)$this->resource->user_id
                ]
            ]
        ];
    }

}
