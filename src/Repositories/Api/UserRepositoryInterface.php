<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories\Api;


use App\User;

interface UserRepositoryInterface
{

    public function model(): User;

    public function save(User $user):User;

    public function find(int $id);

    public function findBy(String $column, $value);
}