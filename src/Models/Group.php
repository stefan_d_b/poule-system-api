<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\Translatable\HasTranslations;
use Stefandebruin\JsonApi\JsonApi;
use Stefandebruin\PouleSystem\Facades\OverrideModels;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;

/**
 * Stefandebruin\PouleSystem\Models\Group
 *
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Fixture[] $allFixtures
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Competition[] $competitions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Fixture[] $fixtures
 * @property-read \App\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Phase[] $phase
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Team[] $teams
 * @method static \Illuminate\Database\Eloquent\Builder|Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Group whereSystemCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Group whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Group whereUpdatedAt($value)
 * @method LengthAwarePaginator jsonPaginate
 * @mixin \Eloquent
 * @property int $id
 * @property array $title
 * @property bool $system_created
 * @property string $key
 * @method static \Illuminate\Database\Eloquent\Builder|\Stefandebruin\PouleSystem\Models\Group whereKey($value)
 */
class Group extends PouleModel
{
    use HasTranslations, JsonApi;

    public $translatable = ['title'];

    protected $fillable = [
        'title',
        'key'
    ];

    protected $hidden = [
        'pivot',
        'system_created',
        'order_by',
        'group_id',
        'phase_id',
        'competition_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'system_created' => 'boolean',
    ];

    protected $attributes = [
        'system_created' => 0
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * TODO-stefan boundCompetitionId && boundPhaseId
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @throws \Exception
     */
    public function teams()
    {
        $relation = $this->belongsToMany('Stefandebruin\PouleSystem\Models\Team')
            ->withPivot('competition_id', 'phase_id');

        if (app()->bound('competitionId')) {
            $relation->where('competition_id', PouleSystemServiceProvider::boundCompetitionId());
        }

        if (app()->bound('phaseId')) {
            $relation->where('phase_id', PouleSystemServiceProvider::boundPhaseId());
        }
        return $relation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @throws \Exception
     */
    public function phase()
    {
        $relation = $this->belongsToMany('Stefandebruin\PouleSystem\Models\Phase', 'competition_phase_group')
            ->withPivot('competition_id');

        if (app()->bound('competitionId')) {
            $relation->where('competition_id', PouleSystemServiceProvider::boundCompetitionId());
        }

        return $relation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function competitions()
    {
        return $this->belongsToMany(
            'Stefandebruin\PouleSystem\Models\Competition',
            'competition_phase_group'
        )->withPivot('phase_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @throws \Exception
     */
    public function fixtures()
    {
        $relation = $this->hasMany('Stefandebruin\PouleSystem\Models\Fixture');

        if (app()->bound('competitionId')) {
            $relation->where('competition_id', PouleSystemServiceProvider::boundCompetitionId());
        }

        if (app()->bound('phaseId')) {
            $relation->where('phase_id', PouleSystemServiceProvider::boundPhaseId());
        }

        return $relation;
    }

    public function owner()
    {
        return $this->belongsTo(OverrideModels::getUserModel(), 'owner_id');
    }

    /**
     * @return $this
     */
    public function setWith()
    {
        $this->with = [];
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('poulesystem.group.show', ['group' => $this->id]);
    }
}
