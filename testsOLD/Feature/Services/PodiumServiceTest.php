<?php

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Stefandebruin\PouleSystem\DataModels\Podium;
use Stefandebruin\PouleSystem\DataModels\PodiumRow;
use Stefandebruin\PouleSystem\Events\PodiumUpdate;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Domain;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Prediction;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;
use Stefandebruin\PouleSystem\Repositories\Api\ScoreRepositoryInterface;
use Stefandebruin\PouleSystem\Services\PodiumService;
use stefandebruin\PouleSystem\tests\Services\PodiumServiceTestData;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 22-11-17
 * Time: 17:18
 */

class PodiumServiceTest extends \Stefandebruin\PouleSystem\Test\TestCase{
    protected $podiumCacheKey = 'podium_58';
    protected $virtualPodiumCacheKey = 'virtual_podium_58';
    protected $competitionId = 58;

    private $countFixtures = 5;
    private $countUsers = 5;

    /**
     * @var PodiumService
     */
    private $_classPodiumService = null;

    /**
     * @var ScoreRepositoryInterface
     */
    private $_classScoreRepositoryInterface = null;

    /**
     * @var Competition
     */
    private $competition = null;

    private $users = null;

    protected function setUp()
    {
        parent::setUp();
        config::set('poulesystem.domain.userConnected', false);
        $this->_classScoreRepositoryInterface = Mockery::mock(ScoreRepositoryInterface::class);
        $this->_classPodiumService = new PodiumService($this->_classScoreRepositoryInterface);

        $this->users = $this->_mockUsers($this->countUsers);
        $this->competition = $this->_mockCompetitionModel();
        $this->_mockUserFind();
        $this->_addgetAllUsersMockFunctionToObject();
    }

    protected function tearDown()
    {
        $this->_classScoreRepositoryInterface = NULL;
        $this->_classPodiumService = NULL;
        $this->users = NULL;
        $this->competition = NULL;
    }

    private function _mockCompetition(){
        $competition = factory(\Stefandebruin\PouleSystem\Models\Competition::class, 1)
            ->states(['id', 'visible'])
            ->make();

        $this->podiumCacheKey = 'podium_'. $competition->first()->id;
        $this->virtualPodiumCacheKey = 'virtual_podium_'. $competition->first()->id;
        return $competition->first();
    }

    private function _mockPhaseGroup(Competition $competition){
        $phases = factory(\Stefandebruin\PouleSystem\Models\Phase::class, 1)->states(['id'])->make();
        $phases->each(function ($phase) use ($competition){
            $phase->setRelation('competition', clone $competition);
            $groups = factory(\Stefandebruin\PouleSystem\Models\Group::class, 1)->states(['id'])->make();
            $phase->setRelation('groups', $groups);
            return $phase;
        });
        $competition->setRelation('phases', $phases);
    }

    private function _mockFixtures($competition, $count, $statuses = []){
        $fixtures = factory(Fixture::class, $count)->make(
            [
                'competition_id' => $competition->id,
                'phase_id' => $competition->phases->first()->id,
                'group_id' => $competition->phases->first()->groups->first()->id,
            ]
        );

        if(!is_array($statuses)) $statuses = [];

        for($index = 0; $index < $fixtures->count(); $index++){
            $fixture = $fixtures->get($index);
            $fixture->id = $index + 1;
            $fixture->status = array_get($statuses, $index, Fixture::STATUS_SCHEDULED);
        }

        $fixtures->each(function ($fixture) use ($competition, $statuses, $index){
            $team1 = factory(\Stefandebruin\PouleSystem\Models\Team::class)->states(['id'])->make();
            $team2 = factory(\Stefandebruin\PouleSystem\Models\Team::class)->states(['id'])->make();
            $fixture->setRelation('competition', clone $competition);
            $fixture->setRelation('phase', clone $competition->phases->first());
            $fixture->setRelation('group', clone $competition->phases->first()->groups->first());
            $fixture->setRelation('homeTeam', clone $team1);
            $fixture->setRelation('awayTeam', clone $team2);

            $index++;
            return $fixture;
        });

        $competition->setRelation('fixtures', $fixtures);
    }

    private function _mockUsers($count){
        $users = factory(\App\User::class, $count)->make();
        for($index = 0; $index < $users->count(); $index++){
            $user = $users->get($index);
            $user->id = $index + 1;
        }

        return $users;
    }

    private function _mockPrediction(Competition $competition, Fixture $fixture, User $user, $prediction = []){
        $factory = factory(\Stefandebruin\PouleSystem\Models\Prediction::class)->states(['id'])->make(
            [
                'fixture_id' => $fixture->id,
                'user_id' => $user->id,
                'home_goals' => array_get($prediction, 'home'),
                'away_goals' => array_get($prediction, 'away'),
            ]
        );

        foreach($competition->getRelation('fixtures') as $_fixture){
            if($_fixture->id === $fixture->id){
                if(!$fixture->relationLoaded('predictions')){
                    $_fixture->setRelation('predictions', collect());
                }
                $_fixture->getRelation('predictions')->push($factory);
            }
        }
    }

    private function _mockPredictions($competition, $users){
        $this->_mockPrediction($competition, $competition->fixtures->get(0), $users->get(0), ['home' => 2, 'away' => 4]);
        $this->_mockPrediction($competition, $competition->fixtures->get(0), $users->get(1), ['home' => 2, 'away' => 0]);
        $this->_mockPrediction($competition, $competition->fixtures->get(3), $users->get(2), ['home' => 1, 'away' => 1]);
        $this->_mockPrediction($competition, $competition->fixtures->get(2), $users->get(3), ['home' => 1, 'away' => 2]);
        $this->_mockPrediction($competition, $competition->fixtures->get(2), $users->get(4), ['home' => 3, 'away' => 3]);
    }

    private function _mockResult(Competition $competition, Fixture $fixture, $prediction = []){
        $factory = factory(\Stefandebruin\PouleSystem\Models\Result::class)->states(['id'])->make(
            [
                'fixture_id' => $fixture->id,
                'user_id' => null,
                'home_goals' => array_get($prediction, 'home'),
                'away_goals' => array_get($prediction, 'away'),
            ]
        );

        $competition->fixtures->where('id', $fixture->id)->first()->result = $factory;
    }

    private function _mockCompetitionModel($countFixtures = null, $statuses = []){
        if(is_null($countFixtures)) $countFixtures = $this->countFixtures;
        /** @var Competition $competition */
        $competition = $this->_mockCompetition();
        $this->_mockPhaseGroup($competition);
        $this->_mockFixtures($competition, $countFixtures, $statuses);
        return $competition;
    }

    private function _addgetAllUsersMockFunctionToObject(){
        $usersIdsCollection = [];
        foreach($this->users as $user){
            $usersIdsCollection[] = (object)['user_id' => $user->id];
        }

        $this->_classScoreRepositoryInterface
            ->shouldReceive('getAllUsers')
            ->once()
            ->with($this->competition->id)
            ->andReturn(collect($usersIdsCollection));
    }

    private function _mockCachePodiumPut(){
        $podiumCacheData = [
            1 => ['points' => 0, 'matches' => []],
            2 => ['points' => 0, 'matches' => []],
            3 => ['points' => 0, 'matches' => []],
            4 => ['points' => 0, 'matches' => []],
            5 => ['points' => 0, 'matches' => []],
        ];

        Cache::shouldReceive('put')->once()->with($this->podiumCacheKey, $podiumCacheData, 1440);
    }

    private function _mockUserFind(){
        $fakeUserModel = Mockery::mock(User::class);
        foreach($this->users as $user) {
            $fakeUserModel
                ->shouldReceive('find')
                ->with($user->id)
                ->once()
                ->andReturn($user);
        }

        $this->app->instance(user::class, $fakeUserModel);
    }

    private function _getEmptyPodium($data = [], $null = true){
        $countUserMapping = 0;
        foreach($data as $index => $item){
            $countUserMapping += (int) array_has($item, 'user_id');
        }


        $index = 0;
        $podiumResponse = new Podium();
        $podiumResponseRows = collect();
        foreach($this->users as $user){
            $dataRow = array_get($data, $index);
            if($countUserMapping == $this->users->count()){
                $found = array_where($data, function($value, $key) use($user){
                    return ($value['user_id'] === $user->id);
                });
                $dataRow = (array_first($found));
            }

            $rowData = [
                'positionChangeDirection' => array_get($dataRow, 'positionChangeDirection', $null ? null : 'same'),
                'positionChange' => array_get($dataRow, 'positionChange', $null ? null : 0),
                'userId' => $user->id,
                'points' => array_get($dataRow, 'points', 0),
                'matches' => array_get($dataRow, 'matches', []),
                'user' => $user,
            ];

            if(array_has($dataRow, 'virtual')){
                $rowData['virtual'] = $dataRow['virtual'];
            }

            $podiumResponseRows->push(
                (new PodiumRow())->fill($rowData)
            );
            $index++;
        }
        $podiumResponse->connectRows($podiumResponseRows);
        $podiumResponse->sortRowsByDesc();
        return $podiumResponse;
    }

    /**
     *  check if the function returns false if no cache exist
     */
    public function testCheckIfFunctionReturnFalseIfCacheNoExist(){
        Cache::shouldReceive('has')
            ->once()
            ->with($this->podiumCacheKey)
            ->andReturn(false);
        $this->assertFalse($this->_classPodiumService->cacheExist($this->podiumCacheKey));
    }

    /**
     * check if the function return now rows with no cache and predictions
     */
    public function testGetPodiumDataNoRowsNoCache(){
        Cache::shouldReceive('has')->once()->with($this->podiumCacheKey)->andReturn(false);
        $this->_mockCachePodiumPut();
        $this->_addgetAllUsersMockFunctionToObject();
        $podium = $this->_classPodiumService->getPodiumData($this->competition);
        $this->assertEquals(5, count($podium->rows));
    }

    /**
     *
     */
    public function testGetPodiumWithPredictionWithoutResult(){
        $this->_mockPredictions($this->competition, $this->users);
        Cache::shouldReceive('has')->once()->with($this->podiumCacheKey)->andReturn(false);
        $this->_mockCachePodiumPut();
        $this->_addgetAllUsersMockFunctionToObject();
        $podium = $this->_classPodiumService->getPodiumData($this->competition);

        $this->assertInstanceOf(Podium::class, $podium);
        $this->assertEquals($this->_getEmptyPodium(), $podium);
        $this->assertEquals(5, count($podium->rows));
    }





    public function testGetPodiumWithPredictionWithResultStatusWait(){
        $this->_mockPredictions($this->competition, $this->users);
        $this->_mockResult($this->competition, $this->competition->fixtures->get(0), ['home' => 3, 'away' => 3]);
        $this->_addgetAllUsersMockFunctionToObject();

        Cache::shouldReceive('has')->once()->with($this->podiumCacheKey)->andReturn(false);
        $this->_mockCachePodiumPut();

        $podium = $this->_classPodiumService->getPodiumData($this->competition);
        $this->assertEquals($this->_getEmptyPodium(), $podium);
        $this->assertEquals(5, count($podium->rows));
    }

    public function testGetPodiumWithPredictionWithResultStatusFinished(){
        $competition = $this->_mockCompetitionModel(null, [Fixture::STATUS_FINISHED]);
        $this->_mockPredictions($competition, $this->users);
        $this->_mockResult($competition, $competition->fixtures->get(0), ['home' => 3, 'away' => 0]);
        $this->_addgetAllUsersMockFunctionToObject();

        $this->_classScoreRepositoryInterface
            ->shouldReceive('model')
            ->andReturn(app()->make(Prediction::class));

        $testData = [
            1 => ['points' => 0, 'matches' => [1]],
            2 => ['points' => 3, 'matches' => [1]],
            3 => ['points' => 0, 'matches' => []],
            4 => ['points' => 0, 'matches' => []],
            5 => ['points' => 0, 'matches' => []],
        ];
        Cache::shouldReceive('put')
            ->once()
            ->with($this->podiumCacheKey, $testData, 1440);

        Cache::shouldReceive('has')
            ->once()
            ->with($this->podiumCacheKey)
            ->andReturn(false);

        $podium = $this->_classPodiumService->getPodiumData($competition);

        $this->assertInstanceOf(Podium::class, $podium);
        $this->assertEquals(5, count($podium->rows));
        $this->assertEquals($this->_getEmptyPodium([['points' => 0, 'matches' => [1]], ['points' => 3, 'matches' => [1]]]), $podium);
    }

    public function testgetPodiumDataExist(){
        $competition = $this->_mockCompetitionModel(null, [Fixture::STATUS_FINISHED]);
        $this->_mockPredictions($competition, $this->users);
        $this->_mockResult($competition, $competition->fixtures->get(0), ['home' => 3, 'away' => 0]);

        $testData = [
            1 => [
                'points' => 0,
                'matches' => [1]
            ],
            2 => [
                'points' => 3,
                'matches' => [1]
            ]
        ];

        Cache::shouldReceive('has')
            ->once()
            ->with($this->podiumCacheKey)
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->andReturn($testData);

        $testDataPut = [
            1 => ['points' => 0, 'matches' => [1]],
            2 => ['points' => 3, 'matches' => [1]],
            3 => ['points' => 0, 'matches' => []],
            4 => ['points' => 0, 'matches' => []],
            5 => ['points' => 0, 'matches' => []],
        ];
        Cache::shouldReceive('put')
            ->never()
            ->with($this->podiumCacheKey, $testDataPut, 1440)
            ->andReturn($testDataPut);

        $podium = $this->_classPodiumService->getPodiumData($competition);

        $this->assertInstanceOf(Podium::class, $podium);
        $this->assertEquals(5, count($podium->rows));
        $this->assertEquals($this->_getEmptyPodium([['points' => 0, 'matches' => [1]], ['points' => 3, 'matches' => [1]]]), $podium);

    }

    /**
     * TODO FIX
     */
    public function testBuildPodiumWithVirtualWithoutChanges(){
        $competition = $this->_mockCompetitionModel(null, [Fixture::STATUS_FINISHED]);
        $this->_mockPredictions($competition, $this->users);
        $this->_mockResult($competition, $competition->fixtures->get(0), ['home' => 3, 'away' => 0]);

        $testData = [
            $this->users->get(0)->id => [
                'points' => 0,
                'matches' => [1,2,3,]
            ],
            $this->users->get(1)->id => [
                'points' => 3,
                'matches' => [1,2,3]
            ],
            $this->users->get(2)->id => [
                'points' => 7,
                'matches' => [1,2,3],
            ],
            $this->users->get(3)->id => [
                'points' => 5,
                'matches' => [1,2,3],
            ],
            $this->users->get(4)->id => [
                'points' => 0,
                'matches' => [1,2,3],
            ]
        ];

        $testDataVirtual = [
            $this->users->get(1)->id => [4 => 3],
            $this->users->get(0)->id => [4 => 1],
        ];

        Cache::shouldReceive('has')
            ->once()
            ->with($this->podiumCacheKey)
            ->andReturn(true);

        Cache::shouldReceive('has')
            ->once()
            ->with($this->virtualPodiumCacheKey)
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->with($this->podiumCacheKey, null)
            ->andReturn($testData);

        Cache::shouldReceive('get')
            ->never()
            ->with($this->virtualPodiumCacheKey, [])
            ->andReturn($testDataVirtual);

        $testDataPut = [
            1 => ['points' => 0, 'matches' => [1,2,3]],
            2 => ['points' => 3, 'matches' => [1,2,3]],
            3 => ['points' => 7, 'matches' => [1,2,3]],
            4 => ['points' => 5, 'matches' => [1,2,3]],
            5 => ['points' => 0, 'matches' => [1,2,3]],
        ];

        Cache::shouldReceive('put')
            ->never()
            ->with($this->podiumCacheKey, $testDataPut, 1440)
            ->andReturn(true);

        $podium = $this->_classPodiumService->buildPodium($competition);
        $this->assertInstanceOf(Podium::class, $podium);
        $this->assertEquals(5, count($podium->rows));
        $exceptedPodium = $this->_getEmptyPodium(
            [
                [
                    'user_id' => $this->users->get(2)->id, 'points' => 7,
                    'matches' => [1,2,3,4], 'positionChangeDirection' => 'same',
                    'positionChange' => 0,
                    'podium' => 7, 'virtual' => 0
                ],
                [
                    'user_id' => $this->users->get(1)->id, 'points' => 6,
                    'matches' => [1,2,3,4], 'positionChangeDirection' => 'up',
                    'positionChange' => 1,
                    'podium' => 3, 'virtual' => 3
                ],
                [
                    'user_id' => $this->users->get(3)->id, 'points' => 5,
                    'matches' => [1,2,3,4], 'positionChangeDirection' => 'down',
                    'positionChange' => 1,
                    'podium' => 5, 'virtual' => 0
                ],
                [
                    'user_id' => $this->users->get(0)->id, 'points' => 1,
                    'matches' => [1,2,3,4], 'positionChangeDirection' => 'same',
                    'positionChange' => 0,
                    'podium' => 0, 'virtual' => 1
                ],
                [
                    'user_id' => $this->users->get(4)->id, 'points' => 0,
                    'matches' => [1,2,3,4], 'positionChangeDirection' => 'same',
                    'positionChange' => 0,
                    'podium' => 0, 'virtual' => 0
                ],
            ],
            true
        );

        foreach($exceptedPodium->rows as $row){
            unset($row->matches);
        }

//        dd(
//            [
//                'equals' => $exceptedPodium->toArray(),
//                'result' => $podium->toArray(),
//            ]
//        );
        $this->assertEquals($exceptedPodium, $podium);
    }


    public function testStoreVirtualPodiumScores(){
        $competition = $this->_mockCompetitionModel(null, [Fixture::STATUS_FINISHED, Fixture::STATUS_IN_PLAY]);
        $this->_mockPredictions($competition, $this->users);

        $fixture = $competition->fixtures->get(2);
        $this->_mockResult($competition, $fixture, ['home' => 3, 'away' => 3]);
        $result = $competition->fixtures->where('id', $fixture->id)->first()->result;

        $this->_classScoreRepositoryInterface
            ->shouldReceive('findBy')
            ->once()
            ->with("fixture_id", $fixture->id)
            ->andReturn($fixture->predictions);

        Cache::shouldReceive('has')
            ->once()
            ->with($this->podiumCacheKey)
            ->andReturn(true);

        Cache::shouldReceive('put')
            ->once()
            ->with(
                $this->virtualPodiumCacheKey,
                [
                    4 => [3 => 0],
                    5 => [3 => 3]
                ],
                60 * 6
            )
            ->andReturn(true);

        Cache::shouldReceive('put')
            ->once()
            ->with(
                $this->podiumCacheKey,
                [
                    1 => ['points' => 0, 'matches' => []],
                    2 => ['points' => 0, 'matches' => []],
                    3 => ['points' => 0, 'matches' => []],
                    4 => ['points' => 0, 'matches' => []],
                    5 => ['points' => 0, 'matches' => []],
                ],
                60 * 24
            )
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->with($this->podiumCacheKey, null)
            ->andReturn([]);

        Cache::shouldReceive('get')
            ->once()
            ->with($this->virtualPodiumCacheKey, [])
            ->andReturn([]);

        $response = $this->_classPodiumService->storeVirtualPodiumScores($fixture, $result);
        $this->assertTrue($response);
    }

    public function testStoreVirtualPodiumScoresMOveToPodium(){
        $competition = $this->_mockCompetitionModel(null, [Fixture::STATUS_FINISHED, Fixture::STATUS_SCHEDULED, Fixture::STATUS_FINISHED]);
        $this->_mockPredictions($competition, $this->users);
        $fixture = $competition->fixtures->where('status', Fixture::STATUS_FINISHED)->last();
        $this->_mockResult($competition, $fixture, ['home' => 3, 'away' => 3]);
        $result = $competition->fixtures->where('id', $fixture->id)->first()->result;

        $this->_classScoreRepositoryInterface
            ->shouldReceive('findBy')
            ->once()
            ->with("fixture_id", $fixture->id)
            ->andReturn($fixture->predictions);

        Cache::shouldReceive('has')
            ->once()
            ->with($this->podiumCacheKey)
            ->andReturn(true);

        Cache::shouldReceive('put')
            ->once()
            ->with(
                $this->virtualPodiumCacheKey,
                [4 => [], 5 => []],
                60 * 6
            )
            ->andReturn(true);

        Cache::shouldReceive('put')
            ->once()
            ->with(
                $this->podiumCacheKey,
                [
                    1 => [
                        'points' => 0,
                        'matches' => [1]
                    ],
                    2 => [
                        'points' => 3,
                        'matches' => [1]
                    ],
                    3 => [
                        'points' => 0,
                        'matches' => [],
                    ],
                    4 => [
                        'points' => 0,
                        'matches' => [],
                    ],
                    5 => [
                        'points' => 0,
                        'matches' => [],
                    ]
                ],
                60 * 24
            )
            ->andReturn(true);

        $testData = [
            1 => [
                'points' => 0,
                'matches' => [1]
            ],
            2 => [
                'points' => 3,
                'matches' => [1]
            ]
        ];

        Cache::shouldReceive('get')
            ->once()
            ->with($this->podiumCacheKey, null)
            ->andReturn($testData);

        Cache::shouldReceive('get')
            ->once()
            ->with($this->virtualPodiumCacheKey, [])
            ->andReturn([4 => [3 => 0], 5 => [3 => 3]]);

        $response = $this->_classPodiumService->storeVirtualPodiumScores($fixture, $result);
        $this->assertTrue($response);
    }

    /**
     * @test
     * get the difference between 2 podiums. before and after
     */
    public function testBroadcast(){
        $this->_mockPredictions($this->competition, $this->users);
        $this->_mockResult($this->competition, $this->competition->fixtures->get(0), ['home' => 3, 'away' => 3]);
        $this->_addgetAllUsersMockFunctionToObject();

        $podiumBefore = $this->_getEmptyPodium(
            [
                ['user_id' => $this->users->get(2)->id, 'points' => 7, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(3)->id, 'points' => 5, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(1)->id, 'points' => 4, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(4)->id, 'points' => 0, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(0)->id, 'points' => 0, 'matches' => [1,2,3]],
            ]
        );

        $podiumAfter = $this->_getEmptyPodium(
            [
                ['user_id' => $this->users->get(2)->id, 'points' => 7, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(3)->id, 'points' => 5, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(1)->id, 'points' => 6, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(4)->id, 'points' => 0, 'matches' => [1,2,3]],
                ['user_id' => $this->users->get(0)->id, 'points' => 1, 'matches' => [1,2,3]],
            ]
        );

        $changes = [
            [
                'positionChangeDirection' => 'up',
                'positionChange' => 1,
                'userId' => $this->users->get(1)->id,
                'points' => 6,
                'user' => $this->users->get(1)->toArray(),
                'pointsChange' => 2,
                'newPosition' => 2,
            ],
            [
                'positionChangeDirection' => 'down',
                'positionChange' => 1,
                'userId' => $this->users->get(3)->id,
                'points' => 5,
                'user' => $this->users->get(3)->toArray(),
                'pointsChange' => 0,
                'newPosition' => 3
            ],
            [
                'positionChangeDirection' => 'same',
                'positionChange' => 0,
                'userId' => $this->users->get(0)->id,
                'points' => 1,
                'user' => $this->users->get(0)->toArray(),
                'pointsChange' => 1,
                'newPosition' => 4
            ]
        ];
        $normal = new Podium();

        Event::fake();

        $podium = $this->_classPodiumService->broadcastChanges($this->competition, $normal, $podiumBefore, $podiumAfter);
        $competition = $this->competition;
        $u = $this->users;
        \Illuminate\Support\Facades\Event::assertDispatched(PodiumUpdate::class, function ($e) use ($competition, $changes, $podiumBefore, $podiumAfter, $u) {
            return $e->competition === $competition && $e->updatedRows === $changes;
        });
    }
}
