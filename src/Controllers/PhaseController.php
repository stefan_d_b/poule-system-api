<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stefandebruin\JsonApi\Controllers\JsonController;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\PhaseRepositoryInterface;
use Stefandebruin\PouleSystem\Requests\ConnectGroupsRequest;
use Stefandebruin\PouleSystem\Requests\CreateUpdatePhaseRequest;
use Stefandebruin\PouleSystem\Requests\SavePhaseOrderRequest;
use Stefandebruin\PouleSystem\Services\TranslationService;

class PhaseController extends JsonController
{
    /**
     * @var PhaseRepositoryInterface
     */
    private $phaseRepository;
    /**
     * @var CompetitionRepositoryInterface
     */
    private $competitionRepository;
    /**
     * @var TranslationService
     */
    private $translationService;

    /**
     * PhaseController constructor.
     * @param PhaseRepositoryInterface $phaseRepository
     * @param CompetitionRepositoryInterface $competitionRepo
     * @param TranslationService $translationService
     */
    public function __construct(
        PhaseRepositoryInterface $phaseRepository,
        CompetitionRepositoryInterface $competitionRepo,
        TranslationService $translationService
    ) {
        $this->phaseRepository = $phaseRepository;
        $this->competitionRepository = $competitionRepo;
        $this->translationService = $translationService;
    }

    /**
     * @return AbstractJsonApiCollection
     */
    public function index(Request $request)
    {
        $model = app(Phase::class);

        return new AbstractJsonApiCollection(
            app()->make(FilterCollectionService::class)->filter($model)->jsonPaginate()
        );
    }

    /**
     * @param Competition $competition
     * @return AbstractJsonApiCollection
     */
    public function competitionShowCollection(Competition $competition)
    {
        $competition->load('phases');

        $phases = $competition->phases;
        foreach ($phases as $phase) {
            $settings = $phase->pivot->toArray();
            unset($settings['competition_id']);
            unset($settings['phase_id']);
            $collection = collect($settings);
            $phase->setRelation('settings', $collection);
        }

        return new AbstractJsonApiCollection($phases);
    }

    /**
     * TODO-stefan controleren of dit gebruikt wordt
     * @param Competition $competition
     * @param Phase $phase
     * @return AbstractJsonApi
     */
    public function competition(Competition $competition, Phase $phase)
    {
        return new AbstractJsonApi($phase);
    }

    /**
     * @param Phase $phase
     * @return AbstractJsonApi
     */
    public function show(Phase $phase)
    {
        return new AbstractJsonApi($phase);
    }

    public function groupRelationList(Group $group){
        return new AbstractJsonApiCollection($group->phase);
    }

    /**
     * @param CreateUpdatePhaseRequest $request
     * @return AbstractJsonApi
     */
    public function store(CreateUpdatePhaseRequest $request)
    {
        return parent::createModel($request);
    }

    /**
     * @param CreateUpdatePhaseRequest $request
     * @param Phase $phase
     * @return AbstractJsonApi
     */
    public function update(CreateUpdatePhaseRequest $request, Phase $phase)
    {
        $phase->fill($request->allAttributes());
        $phase->save();

        return new AbstractJsonApi($phase);
    }

    /**
     * @param ConnectGroupsRequest $request
     * @param Competition $competition
     * @param Phase $phase
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function connectGroups(ConnectGroupsRequest $request, Competition $competition, Phase $phase)
    {
        $ids = array_pluck($request->input('data'), 'id');
        $relation = [];
        $index = 1;
        foreach ($ids as $id) {
            $relation[$id] = ['order_by' => $index, 'competition_id' => $competition->id];
            $index++;
        }
        try {
            $phase->groups()->sync($relation);
        } catch (\Exception $e) {
            throw $e;
        }

        return response()->json()->setStatusCode(204);
    }

    /**
     * @param SavePhaseOrderRequest $request
     * @param Competition $competition
     * @return Competition
     */
    public function storeOrder(SavePhaseOrderRequest $request, Competition $competition)
    {
        $competition->phases()->detach();
        $index = 1;
        $phases = [];
        foreach ($request->all() as $phase) {
            $phases[$phase['phase_id']] = $phase;
            unset($phases[$phase['phase_id']]['phase_id']);
            $phases[$phase['phase_id']]['order_by'] = $index;
            $index++;
        }

        $competition->phases()->sync($phases);

        return response()->json(['saved' => true, 'competition' => $competition]);
    }

    /**
     * @param Phase $phase
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy(Phase $phase)
    {
        try {
            $phase->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return response('', 204);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function destroyBulk(Request $request)
    {
        /** @var Phase $phase */
        $response = [];
        //TODO-stefan kijken of dit werkt en ombouwen naar iets zonder opmerking
        foreach ($this->phaseRepository->model()->find($request) as $item) {
            $item->delete();

            $response[$item->id] = $item->exists;
        }
        return $response;
    }

    public function removeConnections($competitionId)
    {
        /** @var Competition $competition */
        $competition = $this->competitionRepository->find($competitionId);

        if (!$competition instanceof Competition) {
            return response()->json(['message' => 'competition not found'], 404);
        }

        $success = true;
        try {
            $competition->phases()->sync([]);
        } catch (\Exception $exception) {
            $success = false;
        }

        return response()->json(['saved' => $success, 'competition' => $competition]);
    }
}
