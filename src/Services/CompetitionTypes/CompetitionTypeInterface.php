<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-07-18
 * Time: 13:40
 */

namespace Stefandebruin\PouleSystem\Services\CompetitionTypes;

use Illuminate\Support\Collection;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;

interface CompetitionTypeInterface
{
    /**
     * @param Phase $phase
     * @param $fixture
     * @return mixed
     */
    public function connectedToPhase(Phase $phase, $fixture): bool;

    /**
     * @param Phase $phase
     * @param $fixture
     * @return Group
     */
    public function findCorrectGroup(Phase $phase, $fixture): Group;

    /**
     * @param Collection $fixtures
     * @return Competition
     */
    public function beforeCreatingFixtures(Collection $fixtures): Competition;

    public function needSwitchPhase(Phase $phase, $index): bool;
}
