<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoulesystemScoreTableDefaultAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->smallInteger('halftime_home_goals')->nullable()->change();
            $table->smallInteger('halftime_away_goals')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->smallInteger('halftime_home_goals')->nullable(false)->change();
            $table->smallInteger('halftime_away_goals')->nullable(false)->change();
        });
    }
}
