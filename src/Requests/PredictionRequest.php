<?php

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Stefandebruin\PouleSystem\Exceptions\UserPredictionValidationException;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Prediction;
use \Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Stefandebruin\JsonApi\Requests\JsonApiRequest;
use Stefandebruin\PouleSystem\Repositories\Api\FixtureRepositoryInterface;
use Stefandebruin\PouleSystem\Rules\MatchesWithResultGoals;
use Stefandebruin\PouleSystem\Rules\PenaltyWinnerExist;
use Stefandebruin\PouleSystem\Services\PredictionService;


class PredictionRequest extends JsonApiRequest
{
    protected $dataType = 'prediction';
    /**
     * @var FixtureRepositoryInterface
     */
    private $fixtureRepository;

    private $fixtureInPast = false;
    private $predictionExist = false;
    /**
     * @var PredictionService
     */
    private $predictionService;

    /**
     * PredictionRequest constructor.
     * @param FixtureRepositoryInterface $fixtureRepository
     * @param PredictionService $predictionService
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     */
    public function __construct(
        FixtureRepositoryInterface $fixtureRepository,
        PredictionService $predictionService,
        array $query = array(),
        array $request = array(),
        array $attributes = array(),
        array $cookies = array(),
        array $files = array(),
        array $server = array(),
        $content = null
    ){
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->fixtureRepository = $fixtureRepository;
        $this->predictionService = $predictionService;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

        if($this->method() === 'POST'){
            $fixtureId = $this->input('data.attributes.fixture_id');
            if(is_null($fixtureId)) return true;

            $exists = $this->predictionService->hasPrediction(auth()->id(), $fixtureId);
            if($exists) {
                $this->predictionExist = true;
                return false;
            }
        }else{
            $prediction = $this->predictionService->find($this->input('data.id'));
            if(!$prediction instanceof Prediction) return true;
            $fixtureId = $prediction->fixture_id;
        }

        /** @var Fixture $fixture */
        $fixture = $this->fixtureRepository->find($fixtureId);
        if(!$fixtureId instanceof Fixture) return true;
        $result = $fixture->canSetPrediction();
        if(!$result)$this->fixtureInPast = true;
        return $result;
    }

    public function attributeRules(){
        return [];

        $rules = [
            'fixture_id' => ['required_without:data.id', 'exists:fixtures,id'],
            'home_goals' => ['required', 'min:0', 'max:20', 'integer'],
            'away_goals' => ['required', 'min:0', 'max:20', 'integer'],
            'halftime_home_goals' => [
                'nullable',
                'required_with:data.attributes.halftime_away_goals',
                'min:0',
                'max:20',
                'integer',
                new MatchesWithResultGoals
            ],
            'halftime_away_goals' => [
                'nullable',
                'required_with:data.attributes.halftime_home_goals',
                'min:0',
                'max:20',
                'integer',
                new MatchesWithResultGoals
            ],
            'extratime_home_goals' => [
                'nullable',
                'required_with:data.attributes.extratime_away_goals',
                'min:0',
                'max:20',
                'integer',
            ],
            'extratime_away_goals' => [
                'nullable',
                'required_with:data.attributes.extratime_home_goals',
                'min:0',
                'max:20',
                'integer',
            ],
        ];

        if(config('poulesystem.penaltyModes') === 'winner'){
            $rules['penalty_winner'] = [
                'integer',
                app()->make(PenaltyWinnerExist::class)
            ];
        }else{
            //TODO-stefan implement other penalty type
            $rules['home_penalties'] = ['integer', 'min:0'];
            $rules['away_penalties'] = ['integer', 'min:0'];
        }

        return $rules;
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws AuthorizationException
     */
    protected function failedAuthorization()
    {
        $message = $this->fixtureInPast ? 'This fixture is already started' : 'This action is unauthorized.';
        if($this->predictionExist){
            $message = 'Prediction already exist for this fixture. update it with the update call.';
        }
        throw new AuthorizationException($message);
    }
}
