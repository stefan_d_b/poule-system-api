<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Stefandebruin\PouleSystem\Models\Team;

class TeamRepository
{
    public function model()
    {
        return new Team();
    }

    public function save(Team $team)
    {
        $team->save();
        return $team;
    }

    /**
     * @param $apiId
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function getByApiId($apiId)
    {
        $model = $this->model();
        $model = $model->where('api_id', $apiId);
        $team = $model->get()->first();

        if (is_null($team)) {
            throw new ModelNotFoundException();
        }
        return $team;
    }
}
