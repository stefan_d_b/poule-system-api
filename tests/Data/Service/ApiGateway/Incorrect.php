<?php namespace Stefandebruin\PouleSystem\Test\Data\Service\ApiGateway;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Services\ApiGateway\ApiGatewayInterface;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-19
 * Time: 08:02
 */

class Incorrect
{

    /**
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection|mixed
     */
    public function getLeagues($output): \Illuminate\Support\Collection
    {
        // TODO: Implement getLeagues() method.
    }

    /**
     * @param Competition $competition
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection
     */
    public function getTeams(Competition $competition, $output): \Illuminate\Support\Collection
    {
        // TODO: Implement getTeams() method.
    }

    /**
     * @param Competition $competition
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection
     */
    public function getFixtures(Competition $competition, $output): \Illuminate\Support\Collection
    {
        // TODO: Implement getFixtures() method.
    }

    /**
     * @param Competition $competition
     * @param $output
     * @return \Illuminate\Support\Collection
     */
    public function getScores(Competition $competition, $output): \Illuminate\Support\Collection
    {
        // TODO: Implement getScores() method.
    }
}