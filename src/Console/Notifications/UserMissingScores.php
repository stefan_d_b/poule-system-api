<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 19-07-17
 * Time: 20:11
 */

namespace Stefandebruin\PouleSystem\Console\Notifications;

use App\NotificationSettings;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Score;
use Stefandebruin\PouleSystem\Notifications\UserEmptyScores;

class UserMissingScores extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poulesystem:notification:user_empty_scores {period}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give the user a notification';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $period = (int)$this->argument('period');
        if ($period != 1 && $period != 7) {
            $this->error("period must be 1 or 7");
            return false;
        }

        $hour = 0;
        if ($period == 1) {
            $hour = Carbon::now()->format("H");
            if (!in_array($hour, [0, 6, 12, 18])) {
                return false;
            }
        }

        $usersForThisNotification = User::join('notification_settings', 'notification_settings.user_id', '=', 'users.id')
            ->where('notification', NotificationSettings::USER_EMPTY_SCORE)
            ->where('providers', '!=', "[]")
            ->where('providers', '!=', "{}")
            ->where('options', 'LIKE', '%"hour":' . $hour . '%')
            ->get();

        $startDate = Carbon::create(2017, 7, 14, 14, 0, 0);
        $endDate = Carbon::create(2017, 7, 14, 14, 0, 0)->addDays($period);

        $startInPeriod = Fixture::whereBetween('date', array($startDate->toDateTimeString(), $endDate->toDateTimeString()))->get();
        if ($startInPeriod->count() === 0) {
            return false;
        }


        /** @var Score $userModel */
        $scoreModel = app()->make(Score::class);

        /** @var User $userModel */
        $userModel = app()->make(User::class);

        $whereQuery = DB::table('fixtures')->select(DB::raw('COUNT(*)'))
            ->whereBetween('date', array($startDate->toDateTimeString(), $endDate->toDateTimeString()))->toSql();

        /**
         * SELECT users.id, GROUP_CONCAT(scores.fixture_id) as matches, COUNT(scores.fixture_id) as count FROM users /
         * LEFT JOIN scores ON users.id = scores.user_id /
         * WHERE users.id IN (3, 65) /
         * AND scores.fixture_id IN (14,15,16) /
         * GROUP BY scores.user_id /
         * having COUNT(scores.fixture_id) < 3
         */
        $users = $userModel->select(
            [
                'users.*',
                DB::raw('GROUP_CONCAT(scores.fixture_id) as match_ids'),
                DB::raw('COUNT(scores.fixture_id) as count'),
            ]
        )->leftjoin('scores', 'users.id', '=', 'scores.user_id')
            ->whereNotNull('user_id')
            ->groupBy('scores.user_id')
            ->whereIn('fixture_id', $startInPeriod->pluck('id')->toArray())
            ->whereIn('user_id', $usersForThisNotification->pluck('user_id')->toArray())
            ->having('count', '<', $startInPeriod->count())
            ->get();

        /** @var User $user */
        foreach ($users as $user) {
            $setUpMatches = $startInPeriod->whereNotIn('id', explode(',', $user->match_ids));

            if ($setUpMatches->count() > 0) {
                $user->notify(new UserEmptyScores($setUpMatches));
            }
        }
        return true;
    }
}
