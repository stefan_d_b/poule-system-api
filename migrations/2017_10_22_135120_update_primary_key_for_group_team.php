<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePrimaryKeyForGroupTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_team', function (Blueprint $table) {
            $table->dropPrimary(['group_id', 'team_id']);
            $table->primary(['group_id', 'team_id', 'competition_id', 'phase_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_team', function (Blueprint $table) {
            $table->primary(['group_id', 'team_id']);
        });
    }
}
