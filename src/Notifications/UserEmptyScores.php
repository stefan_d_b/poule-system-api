<?php

namespace Stefandebruin\PouleSystem\Notifications;

use App\NotificationSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserEmptyScores extends Notification
{
    use Queueable;
    /**
     * @var
     */
    private $fixtures;

    /**
     * Create a new notification instance.
     *
     * @param $fixtures
     */
    public function __construct($fixtures)
    {
        $this->fixtures = $fixtures;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->getNotificationProviders(NotificationSettings::USER_EMPTY_SCORE);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        return (new MailMessage)->markdown('mail.invoice.paid');
        return (new MailMessage)
            ->subject('Invoice Paid')
            ->markdown('mail.notifications.user_empty_scores', ['fixtures' => $this->fixtures]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Je hebt nog niet alle wedstrijden voorspeld',
            'message' => 'We hebben gezien dat je nog niet alle wedstrijden voorsteld hebt. doe dat snel om gene punten mis te lopen. ',
            'extra_info' => view('notifications.fixtures-table', ['fixtures' => $this->fixtures])->render(),
            'fixtures' => $this->fixtures->toArray(),
        ];
    }

    public function toDatabase($notifiable){
        return [
            'title' => 'Je hebt nog niet alle wedstrijden voorspeld',
            'message' => 'We hebben gezien dat je nog niet alle wedstrijden voorsteld hebt. doe dat snel om gene punten mis te lopen. ',
            'extra_info' => view('notifications.fixtures-table', ['fixtures' => $this->fixtures])->render(),
            'fixtures' => $this->fixtures->toArray(),
        ];
    }
}
