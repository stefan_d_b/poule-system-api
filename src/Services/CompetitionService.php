<?php namespace Stefandebruin\PouleSystem\Services;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 30-06-18
 * Time: 21:43
 */

use Stefandebruin\PouleSystem\Models\Competition;

class CompetitionService
{
    public function getFilteredCollection($scope)
    {
        switch ($scope) {
            case 'connected':
                $collection = app()->make(Competition::class)
                    ->visibleForUser();
                break;
            default:
                $collection = app()->make(Competition::class);
                break;
        }
        return $collection;
    }
}
