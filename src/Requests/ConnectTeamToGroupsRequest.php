<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ConnectTeamToGroupsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*' => ['required', 'array'],
            '*.group_id' => ['required', 'exists:groups,id'], //group verbonden met phase
            '*.teams' => ['sometimes', 'nullable', 'array'],
            '*.teams.*' => ['sometimes', 'required', 'exists:teams,id'], // team verbonden met competitie?
        ];
    }
}