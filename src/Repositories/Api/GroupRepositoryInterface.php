<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories\Api;


use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;

interface GroupRepositoryInterface
{

    public function model(): Group;

    public function save(Group $competition):Group;

    public function find(int $id);

    public function findBy(String $column, $value);

    public function makeSystemCreated(Phase $phase, $competitionId);

    public function all();
}