<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\PhaseRepositoryInterface;

class PhaseRepository implements PhaseRepositoryInterface
{
    public function model(): Phase{
        return new Phase();
    }

    public function save(Phase $phase):Phase{
        $phase->save();
        return $phase;
    }


    public function find(int $id)
    {
        return $this->model()->find($id);
    }

    public function findBy(String $column, $value)
    {
        $model = $this->model();
        $model = $model->where($column, $value);
        return $model->get();
    }

    public function all(): Collection
    {
        return $this->model()->get();
    }
}