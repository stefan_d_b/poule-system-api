<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface;

class CompetitionRepository implements CompetitionRepositoryInterface
{
    public function model(): Competition
    {
        return app()->make(Competition::class);
    }

    public function save(Competition $competition): Competition
    {
        $competition->save();
        return $competition;
    }

    public function getAllVisible(){
        $model = $this->model();
        $model = $model->where('visible', true);
        return $model->get();
    }

    /**
     * @param $apiId
     * @return Competition
     */
    public function getByApiId($apiId){
        $model = $this->model();
        $model = $model->where('api_id', $apiId);
        $competition = $model->get()->first();
        if(is_null($competition)) $competition = $this->model();
        return $competition;
    }

    public function find($id){
        return $this->model()->find($id);
    }

    public function all()
    {
        return $this->model()->get();
    }
}