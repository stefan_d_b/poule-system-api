<?php namespace Stefandebruin\PouleSystem\Middleware;

use App\User;
use Bouncer;
use Closure;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Phase;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 25-10-17
 * Time: 17:26
 */

class PouleSystem
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);

        if($request->competition instanceof Competition){

            $visibleCompetitions = app()->make(Competition::class)->visibleForUser()->get();
            $competition = $visibleCompetitions->where('id', $request->competition->id)->first();

            $manage = Bouncer::allows('manage', $competition);
            if(!$manage && !$competition instanceof Competition){
                return response()->json(['message' => 'Not allowd to view this competition'], 403);
            }

            if($request->phase instanceof Phase) {
                if(!in_array($request->phase->id, $request->competition->phases->pluck('id')->toArray())){
                    return response()->json(['message' => 'phase not connected or not exist'], 404);
                }
            }
        }

        if($request->has('user')){
            $user = app()->make(User::class)->where('id', $request->get('user'))->first();
            if(!$user instanceof User){
                abort(400);
            }
            app()->singleton('predictionUser', function() use($user){
                return $user;
            });
        }
    }
}