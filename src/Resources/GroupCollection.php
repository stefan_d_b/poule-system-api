<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Stefandebruin\PouleSystem\Models\Competition as CompetitionModel;

class OldGroupCollection extends ResourceCollection
{
    use RelationTrait;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => ['self' => $request->url()],
        ];
    }
//    /**
//     * Get additional data that should be returned with the resource array.
//     *
//     * @param \Illuminate\Http\Request  $request
//     * @return array
//     */
//    public function with($request)
//    {
//        $includeRelations = array_filter(
//            explode(",",
//                $request->get('include')
//            ),
//            function($value) {
//                return !empty($value);
//            }
//        );
//
//
//        $includes = [];
//        $items = $this->collection->pluck('resource');
//        foreach($includeRelations as $includeRelation){
//            foreach($items->pluck($includeRelation) as $items){
//                /** @var Model $item */
//                foreach ($items as $item){
//                    $className = class_basename($item);
//                    if(!array_has($includes, $className.'.'.$item[$item->getKeyName()])){
//                        $c = $item->getResourceClass();
//                        $includes[$className.'.'.$item[$item->getKeyName()]] = new $c($item);
//                    }
//                }
//            }
//        }
//
//        if(count($includes) > 0){
//            return ['includes' => array_values($includes)];
//        }
//
//
//        return [];
//    }
}
