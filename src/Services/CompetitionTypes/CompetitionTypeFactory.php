<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-07-18
 * Time: 16:38
 */

namespace Stefandebruin\PouleSystem\Services\CompetitionTypes;


use Stefandebruin\PouleSystem\Models\Competition;

class CompetitionTypeFactory
{
    public function create(Competition $competition): CompetitionTypeInterface
    {
        switch ($competition->type) {
            case Competition::$WORLD:
                return app()->make(WorldCup::class, ['competition' => $competition]);
                break;
            case Competition::$EUROPE:
                return app()->make(Euro::class, ['competition' => $competition]);
                break;
            case Competition::$TYPECOMPETITIONMONTH:
                return app()->make(Month::class, ['competition' => $competition]);
                break;
            case Competition::$TYPECOMPETITIONWEEK:
                return app()->make(Week::class, ['competition' => $competition]);
                break;
            default:
                return app()->make(Custom::class, ['competition' => $competition]);
        }
    }
}