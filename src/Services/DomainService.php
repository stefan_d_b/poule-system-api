<?php namespace Stefandebruin\PouleSystem\Services;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 03-11-17
 * Time: 08:04
 */

use Illuminate\Http\Request;
use Stefandebruin\PouleSystem\Models\Domain;

class DomainService
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function registerDomain($domain = false)
    {
        $domain = !empty($domain) ? $domain : $this->request->server('HTTP_ORIGIN');
        $domain = rtrim($domain, '/');
        /** @var Domain $domain */
        $domainModel = app()->make(Domain::class)
            ->where('domain', $domain)
            ->where('enabled', true)->first();

        if (!$domainModel instanceof Domain && config('poulesystem.domain.checkExist')) {
            throw new \HttpRequestException("Domain not allowed");
        } else if (!$domainModel instanceof Domain && !config('poulesystem.domain.checkExist')) {
            $domainModel = app()->make(Domain::class);
            $domainModel->domain = (string)$domain;
            $domainModel->enabled = true;
            $domainModel->save();
        }

        app()->singleton('pouleDomain', function () use ($domainModel) {
            return $domainModel;
        });

        return $domainModel;
    }

    /**
     * test if it works correctly build to repo functions
     * @param null $origin
     * @return Domain
     */
    public function getDomain($origin = null)
    {
        $origin = $origin != null ?: request()->server('HTTP_ORIGIN');
        /** @var Domain $domain */
        $domain = app()->make(Domain::class)
            ->where('domain', $origin)
            ->where('enabled', true)->first();
        if (!$domain instanceof Domain) {
            $domain = app()->make(Domain::class);
            $domain->domain = (string)$origin;
            $domain->enabled = true;
            $domain->save();
        }

        return $domain;
    }

    /**
     *
     * @deprecated
     * @param null|array|int $user
     * @return bool
     * @throws \Exception
     */
    public function connectUser($user = null)
    {
        if (empty($user) && \Auth::check()) {
            $user = [\Auth::id()];
        } else if (!empty($user)) {
            $user = [$user];
        }

        if (count($user) === 0) {
            throw new \Exception("No users to connect");
        }

        /** @var Domain $domain */
        $domain = $this->getDomain();
        $domain->users()->attach($user);

        return true;
    }
}
