<?php

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Stefandebruin\PouleSystem\Controllers\PodiumController;
use Stefandebruin\PouleSystem\DataModels\Podium;
use Stefandebruin\PouleSystem\DataModels\PodiumRow;
use Stefandebruin\PouleSystem\Events\PodiumUpdate;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Repositories\Api\ScoreRepositoryInterface;
use Stefandebruin\PouleSystem\Services\PodiumService;
use stefandebruin\PouleSystem\tests\Services\PodiumServiceTestData;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 22-11-17
 * Time: 17:18
 */

class PodiumControllerTest extends \Stefandebruin\PouleSystem\Test\TestCase{

    /**
     * @var PodiumService
     */
    private $podiumService;

    /**
     * @var PodiumController
     */
    private $podiumController;

    private $competition = null;
    private $users;

    protected function setUp()
    {
        parent::setUp();
        config::set('poulesystem.domain.userConnected', false);
        $this->podiumService = Mockery::mock(podiumService::class);
        $this->podiumController = new PodiumController($this->podiumService);
        $this->competition = $this->_mockCompetition();
        $this->users = $this->_mockUsers(5);
    }

    protected function tearDown()
    {
        $this->podiumService = NULL;
        $this->podiumController = NULL;
    }

    private function _mockUsers($count){
        $users = factory(\App\User::class, $count)->make();
        for($index = 0; $index < $users->count(); $index++){
            $user = $users->get($index);
            $user->id = $index + 1;
        }

        return $users;
    }

    private function _mockCompetition(){
        $competition = factory(Competition::class, 1)
            ->states(['id', 'visible'])
            ->make();
        return $competition->first();
    }

    private function _getEmptyPodium($data = []){
        $countUserMapping = 0;
        foreach($data as $index => $item){
            $countUserMapping += (int) array_has($item, 'user_id');
        }

        $index = 0;
        $podiumResponse = new Podium();
        $podiumResponseRows = collect();
        foreach($this->users as $user){
            $dataRow = array_get($data, $index);
            if($countUserMapping == $this->users->count()){
                $found = array_where($data, function($value, $key) use($user){
                    return ($value['user_id'] === $user->id);
                });
                $dataRow = (array_first($found));
            }

            $podiumResponseRows->push(
                (new PodiumRow())->fill([
                    'positionChangeDirection' => null,
                    'positionChange' => null,
                    'userId' => $user->id,
                    'points' => array_get($dataRow, 'points', 0),
                    'matches' => array_get($dataRow, 'matches', []),
                    'user' => $user,
                ])
            );
            $index++;
        }
        $podiumResponse->connectRows($podiumResponseRows);
        $podiumResponse->sortRowsByDesc();
        return $podiumResponse;
    }

    public function testIndexFunctionEmptyResponse(){
        $podium = new Podium();
        $rows = collect();
        $podium->connectRows($rows);

        $this->podiumService
            ->shouldReceive('buildPodium')
            ->once()
            ->with($this->competition)
            ->andReturn($podium);

        $response = $this->podiumController->index($this->competition);

        $responseData = json_encode(['virtual' => false, 'podium' => [], 'legend' => []]);
        $this->assertEquals($responseData, $response->content());
    }

    public function testIndexWithPodiumData(){
        $podium = new Podium();
        $rows = collect();
        $podium->connectRows($rows);

        $podium = $this->_getEmptyPodium([['points' => 0, 'matches' => [1]], ['points' => 3, 'matches' => [1]]]);
        $this->podiumService
            ->shouldReceive('buildPodium')
            ->once()
            ->with($this->competition)
            ->andReturn($podium);

        $response = $this->podiumController->index($this->competition);

        $responseData = json_encode(['virtual' => false, 'podium' => $podium->rows, 'legend' => []]);
        $this->assertEquals($responseData, $response->content());
    }

    public function testIndexFunctionWithDomainFilter(){
        config::set('poulesystem.domain.userConnected', true);

        $domain = factory(\Stefandebruin\PouleSystem\Models\Domain::class)->make();
        $users = collect(
            [
                $this->users->get(0),
                $this->users->get(3)
            ]
        );

        $usersIds = $users->pluck('id')->toArray();
        $domain->setRelation('users', $users);

        app()->singleton('pouleDomain', function() use($domain){
            return $domain;
        });

        $podium = $this->_getEmptyPodium([['points' => 0, 'matches' => [1]], ['points' => 3, 'matches' => [1]]]);
        $podium->rows = $podium->rows
            ->filter(function ($value, $index) use($usersIds){

                if(config('poulesystem.domain.userConnected') && !in_array($value->userId, $usersIds)){
                    return false;
                }
                return true;
            });
        $podium->rows = $podium->rows->values();

        $podiumFunction = $this->_getEmptyPodium([['points' => 0, 'matches' => [1]], ['points' => 3, 'matches' => [1]]]);
        $this->podiumService
            ->shouldReceive('buildPodium')
            ->once()
            ->with($this->competition)
            ->andReturn($podiumFunction);

        $response = $this->podiumController->index($this->competition);

        $responseData = json_encode(['virtual' => false, 'podium' => $podium->rows, 'legend' => []]);
        $this->assertEquals($responseData, $response->content());
    }
}
