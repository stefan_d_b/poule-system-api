<?php namespace Stefandebruin\PouleSystem\Exceptions;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 23-09-17
 * Time: 13:15
 */

use Illuminate\Validation\ValidationException;

class FormValidationException extends ValidationException
{
    public function __construct($validator, $response = null)
    {
        parent::__construct($validator, $response);
    }

    /**
     * Get all of the validation error messages.
     *
     * @return array
     */
    public function errors()
    {
        $messages = [];
        if (!$this->bulkHeader() && !$this->bulkRoute()) {
            $messages = $this->formatSingle();
        } elseif ($this->bulkHeader() && !$this->bulkRoute()) {
            $messages = $this->formatBulk();
        } else {
            foreach (array_keys(request()->all()) as $requestMethod) {
                $messages[$requestMethod] = $this->formatBulk($requestMethod);
            }
        }

        return $messages;
    }

    private function bulkRoute()
    {
        return ends_with(request()->route()->uri, '/bulk');
    }

    private function bulkHeader()
    {
        return str_contains(request()->header('accept'), 'ext=bulk');
    }


    private function formatBulk($method = null)
    {
        $errorMessages = [];
        $prefix = is_null($method) ? '' : $method . '.';
        foreach ($this->validator->errors()->messages() as $path => $messages) {
            if ($method !== null && !str_contains($path, $method)) {
                continue;
            }
            $pathInfo = array_first(explode('.', str_replace($prefix . 'data.', '', $path)));
            $bulkPath = str_replace('data.' . $pathInfo . '.', '', $path);
            if (!array_has($errorMessages, $pathInfo)) {
                $errorMessages[$pathInfo] = [];
            }
            foreach ($messages as $message) {
                $errorMessages[$pathInfo][] = [
                    'source' => ['pointer' => str_replace('.', '/', $bulkPath)],
                    'title' => "Invalid Attribute",
                    'details' => $message,
                    'id' => $pathInfo,
                ];
            }
        }
        return $errorMessages;
    }

    private function formatSingle()
    {
        $errorMessages = [];
        foreach ($this->validator->errors()->messages() as $path => $messages) {
            foreach ($messages as $message) {
                $errorMessages[] = [
                    'source' => ['pointer' => trim(str_replace(['data', '.'], ['', '/'], $path), '/')],
                    'title' => "Invalid Attribute",
                    'details' => $message
                ];
            }
        }
        return $errorMessages;
    }
}
