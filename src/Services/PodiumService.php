<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 22-11-17
 * Time: 17:29
 */

namespace Stefandebruin\PouleSystem\Services;

use Event;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use phpDocumentor\Reflection\Types\Boolean;
use Stefandebruin\PouleSystem\DataModels\Podium;
use Stefandebruin\PouleSystem\DataModels\PodiumRow;
use Stefandebruin\PouleSystem\Events\PodiumUpdate;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Prediction;
use Stefandebruin\PouleSystem\Models\Result;
use Stefandebruin\PouleSystem\Models\Score;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;
use Stefandebruin\PouleSystem\Repositories\Api\ScoreRepositoryInterface;

class PodiumService
{
    /**
     * Cache key for the virtual podium data
     */
    const CACHE_KEY_VIRTUAL = "virtual_podium";

    /**
     * Cache key for the podium
     */
    const CACHE_KEY_PODIUM = "podium";
    /**
     * @var ScoreRepositoryInterface
     */
    private $scoreRepository;

    /**
     * PodiumService constructor.
     * @param ScoreRepositoryInterface $scoreRepository
     */
    public function __construct(ScoreRepositoryInterface $scoreRepository)
    {
        $this->scoreRepository = $scoreRepository;
    }

    /**
     * Check if the cache exist
     *
     * @param $key
     * @return bool
     */
    public function cacheExist($key): bool
    {
        return Cache::has($key);
    }

    /**
     * @param $key
     */
    public function getCacheData($key, $default = null)
    {
        return Cache::get($key, $default);
    }

    /**
     * @param int $competitionId
     * @param $data
     */
    public function saveCacheData(int $competitionId, $data, $minutes = 60 * 24)
    {
//        Cache::put($this->createCacheKeyPodium($competitionId), $data, $minutes);
    }

    /**
     * Get the podium data if exist and create of not exist
     *
     * @param Competition $competition
     * @return Podium
     */
    public function getPodiumData(Competition $competition): Podium
    {
        $found = $this->cacheExist($this->createCacheKeyPodium($competition->id));
        if($found){
            $data = $this->getCacheData($this->createCacheKeyPodium($competition->id));
        }else{
            $data = $this->calculatePodium($competition);
        }

        $data = $this->_addMissingUsers($competition, $data);

        $this->saveCacheData($competition->id, $data);

        $podium = new Podium();

        $collection = collect();
        foreach ($data as $userId => $rowData) {
            $collection->push(
                app()->make(PodiumRow::class)
                    ->fill($rowData)
                    ->setAttribute('user_id', $userId)
            );
        }
        $podium->setRelation('podiumRows', $collection);

        $podium->sortRowsByDesc();
        return $podium;
    }




    public function getVirtualPodiumData($competitionId)
    {
        return $this->getCacheData($this->createCacheKeyVirtual($competitionId), []);
    }

    public function buildPodium(Competition $competition, $virtual = true): Podium
    {
        $podium = $this->getPodiumData($competition);

        if(!$virtual || !$this->cacheExist($this->createCacheKeyVirtual($competition->id))) return $podium;

        /** @var Podium $virtual */
        $virtual = $this->getPodiumData($competition);
        $virtual = $this->_addVirtualPoints($competition->id, $virtual);

        $podium->sortRowsByDesc();
        $virtual->sortRowsByDesc();

        $virtual = $this->addPositionChange($competition->id, $podium, $virtual);

        return $virtual;
    }

    public function storeVirtualPodiumScores(Competition $competition, Collection $resultChanges)
    {
        $podium = $this->getPodiumData($competition);
        $points = $this->getVirtualPodiumData($competition->id);

        $scores = Prediction::whereIn('fixture_id', $resultChanges->pluck('fixture_id'))->get();
        $scores->pluck('user_id')->unique()->each(function ($item, $key) use ($podium, $points) {
            $podium->AddUserPodiumRowIfMissing($item);
            if (!array_key_exists($item, $points)) {
                $points[$item] = [];
            }
        });

        $resultChanges = $resultChanges->keyBy('fixture_id');
        $scores->groupBy('fixture_id')->each(function ($users, $key) use ($points, $resultChanges, $podium) {
            $users->each(function($prediction, $index) use ($points, $resultChanges, $podium){
                if (!array_key_exists($prediction->fixture_id, $points[$prediction->user_id])) {
                    $points[$prediction->user_id][$prediction->fixture_id] = 0;
                }

                $fixturePoints = $this->_calculate($prediction, $resultChanges->get($prediction->fixture_id));

                if ($prediction->fixture->status == Fixture::STATUS_FINISHED) {
                    if (!in_array($prediction->fixture_id, $podium->rows->where('userId', $prediction->user_id)->first()->matches)) {
                        $podium->rows->where('userId', $prediction->user_id)->first()->points += $fixturePoints;
                        $podium->rows->where('userId', $prediction->user_id)->first()->addMatch($prediction->fixture_id);
                    }
                    unset($points[$prediction->user_id][$prediction->fixture_id]);
                } else {
                    $points[$prediction->user_id][$prediction->fixture_id] = $fixturePoints;
                }
            });
        });

        Cache::put($this->createCacheKeyVirtual($competition->id), $points, 60 * 6);
        Cache::put($this->createCacheKeyPodium($competition->id), $podium->toCacheArray(), 60 * 24);

        return $podium;
    }

    /**
     * @param Competition $competition
     * @param Podium $normal
     * @param Podium $before
     * @param Podium $after
     * @return bool
     */
    public function broadcastChanges(
        Competition $competition,
        Podium $normal,
        Podium $before,
        Podium $after
    ) {
        $before->sortRowsByDesc();
        $after->sortRowsByDesc();

        if( $after->rows instanceof Collection){
            $afterRows = $after->rows->map(function ($item, $index) use ($before, $after) {
                $afterKeys = $after->rows->pluck('userId')->toArray();
                $beforeKeys = $before->rows->pluck('userId')->toArray();
                $positionA = array_search($item->userId, $afterKeys) + 1;
                $positionB = array_search($item->userId, $beforeKeys) + 1;

                $item->positionChangeDirection = 'same';
                $item->positionChange = 0;

                if ($positionA < $positionB) {
                    $item->positionChangeDirection = 'up';
                    $item->positionChange = $positionB - $positionA;
                } elseif ($positionA > $positionB) {
                    $item->positionChangeDirection = 'down';
                    $item->positionChange = $positionA - $positionB;
                }

                $searchedRow = array_search($item->userId, $beforeKeys);
                $item['pointsChange'] = $item['points'] - $before->rows->get($searchedRow)->points;
                $item['newPosition'] = $positionA;
                return $item;
            });

            $after->connectRows($afterRows);

            $changedItems = collect();
            $beforeRows = $before->rows;

            /** @var PodiumRow $item */
            /** @var Podium $after */
            foreach ($after->rows as $index => $item) {
                $beforeItem = $beforeRows->get($index);

                if ($beforeItem instanceof \Stefandebruin\PouleSystem\DataModels\PodiumRow) {
                    /** @var PodiumRow $beforeItem */
                    if ($item->userId === $beforeItem->userId && $item->points !== $beforeItem->points) {
                        $changedItems->push($item);
                    } elseif ($item->userId !== $beforeItem->userId) {
                        $changedItems->push($item);
                    }
                }
            }

            event::dispatch(new PodiumUpdate($competition, $changedItems->toArray()));
        }
        
        return true;
    }



    /**
     * @param $competitionID
     * @return String
     */
    private function createCacheKeyVirtual($competitionID): String
    {
        return self::CACHE_KEY_VIRTUAL . '_' . $competitionID;
    }

    /**
     * @param $competitionID
     * @return String
     */
    private function createCacheKeyPodium($competitionID) : String
    {
        return self::CACHE_KEY_PODIUM . '_' . $competitionID;
    }

    /**
     * @param Competition $competition
     * @return array
     */
    private function calculatePodium(Competition $competition): array
    {
        $podium = [];

        foreach ($this->scoreRepository->getAllUsers($competition->id) as $user) {
            $podium[$user->user_id] = ['points' => 0, 'matches' => []];
        }

        /** @var Fixture $fixture */
        foreach ($competition->fixtures as $fixture) {
            if($fixture->status != Fixture::STATUS_FINISHED) {
                continue;
            }

            /** @var Prediction $prediction */
            foreach ($fixture->predictions as $prediction){
                if(in_array($fixture->id, $podium[$prediction->user_id]['matches'])) continue;

                $result = $fixture->result;
                if(!$result instanceof Result){
                    $result = app()->make(Result::class);
                }
                $podium[$prediction->user_id]['points'] += $this->_calculate($prediction, $result);

                $podium[$prediction->user_id]['matches'][] = $fixture->id;
            }
        }

        return $podium;
    }

    private function _addVirtualPoints($competitionId, Podium $podium){
        $points = $this->getVirtualPodiumData($competitionId);

        $rows = $podium->rows->map(function ($item, $index) use ($points){
            $item->virtual = 0;
            $virtual = 0;
            if(array_key_exists($item->userId, $points)){
                foreach($points[$item->userId] as $matchPoints){
                    $item->points += $matchPoints;
                    $virtual += $matchPoints;
                }
            }

            $item->virtual = $virtual;
            return $item;
        });
        $podium->rows = $rows;
        return $podium;
    }

    private function addPositionChange($competitionId, Podium $before, Podium $after){
        $rows = $after->rows->map(function ($item, $index) use ($before, $after){
            $afterKeys = $after->rows->pluck('userId')->toArray();
            $beforeKeys = $before->rows->pluck('userId')->toArray();

            $positionA = array_search ($item->userId, $afterKeys) + 1;
            $positionB = array_search ($item->userId, $beforeKeys) + 1;

            if($positionA > $positionB){
                $change = ($positionA - $positionB);
                $direction = "down";
            }elseif($positionA < $positionB){
                $change = ($positionB - $positionA);
                $direction = "up";
            }else{
                $change = 0;
                $direction = "same";
            }

            unset($item->matches);
            $item->positionChangeDirection = $direction;
            $item->positionChange = $change;

            return $item;
        });

        $before->rows = $rows;

        return $after;
    }

    private function _calculate(Prediction $prediction, Score $result)
    {
        return $prediction->calculate($result, 'result') +
            $prediction->calculate($result, 'halftime') +
            $prediction->calculate($result, 'extratime') +
            $prediction->calculatePenalties($result);
    }

    private function _addMissingUsers(Competition $competition, $data)
    {
        foreach ($this->scoreRepository->getAllUsers($competition->id) as $user){
            if(!array_key_exists($user->user_id, $data)){
                $data[$user->user_id] = ['points' => 0, 'matches' => []];
            }
        }

        return $data;
    }

}