<?php

use Stefandebruin\PouleSystem\Services\ApiGateway\FootballData;
use Stefandebruin\PouleSystem\Test\Data\Service\ApiGateway\LiveScores;
use Stefandebruin\PouleSystem\Test\TestCase;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-20
 * Time: 19:54
 */

class TeamsTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        Config::set('poulesystem.defaultProvider', 'football-data');
        Config::set(
            'poulesystem.gateways',
            [
                'football-data' => FootballData::class,
                'live-scores' => LiveScores::class,
            ]
        );
    }

    public function testHandle(){
        /** @var \Mockery\MockInterface|FootballData $footballDataGateway */
        $footballDataGateway = Mockery::mock(FootballData::class . '[getLeagues]');
        app()->instance(FootballData::class, $footballDataGateway);
        /** @var \Mockery\MockInterface|LiveScores $liveScoresGateway */
        $liveScoresGateway = Mockery::mock(LiveScores::class . '[getLeagues]');
        app()->instance(LiveScores::class, $liveScoresGateway);

        $footballDataGateway
            ->shouldReceive('getLeagues')
            ->with(\Mockery::on(function($data) {
                return $data instanceof \Illuminate\Console\OutputStyle;
            }))
            ->once()
            ->andReturn(collect());

        $liveScoresGateway
            ->shouldReceive('getLeagues')
            ->with(\Mockery::on(function($data) {
                return $data instanceof \Illuminate\Console\OutputStyle;
            }))
            ->once()
            ->andReturn(collect());

        $this->artisan('poulesystem:import:competitions')
            ->assertExitCode(0);
    }
}