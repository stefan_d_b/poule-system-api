<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface;

class GroupRepository implements GroupRepositoryInterface
{
    public function model(): Group{
        return new Group();
    }

    public function save(Group $competition):Group{
        $competition->save();
        return $competition;
    }


    public function find(int $id)
    {
        return $this->model()->find($id);
    }

    public function findBy(String $column, $value)
    {
        $model = $this->model();
        $model = $model->where($column, $value);
        return $model->get();
    }

    public function makeSystemCreated(Phase $phase, $competitionId)
    {
        $found = $phase->groups->where('system_created', 1);
        if ($found->count() > 0) {
            return $found->first();
        }

        /** @var Group $model */
        $model = $this->model();

        $model->title = "";
        $model->system_created = 1;
        $model->save();

        $phase->groups()->attach($model, ['competition_id' => $competitionId, 'order_by' => 1]);

        return $model;
    }

    public function all()
    {
        return $this->model()->all();
    }
}