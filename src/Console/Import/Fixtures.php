<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:42
 */

namespace Stefandebruin\PouleSystem\Console\Import;

use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Models\Competition;

class Fixtures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poulesystem:import:fixtures';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the fixtures';

    public function handle()
    {
        /** @var Competition $competition */
        foreach (Competition::visible()->get() as $competition) {
            $this->line(sprintf('Import fixtures for %s', $competition->title));
            $competition->getGateway()->getFixtures($competition, $this->output);
        }
    }
}
