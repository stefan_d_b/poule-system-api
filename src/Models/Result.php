<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

namespace Stefandebruin\PouleSystem\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Stefandebruin\PouleSystem\Facades\OverrideModels;
use Stefandebruin\PouleSystem\Scopes\ResultScope;

/**
 * Class Result
 *
 * @method LengthAwarePaginator jsonPaginate
 * @package Stefandebruin\PouleSystem\Models
 * @property int $id
 * @property int $fixture_id
 * @property int|null $user_id
 * @property int $home_goals
 * @property int $away_goals
 * @property int $halftime_home_goals
 * @property int $halftime_away_goals
 * @property int $extratime_home_goals
 * @property int $extratime_away_goals
 * @property int $penalties_home
 * @property int $penalties_away
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Stefandebruin\PouleSystem\Models\Fixture $fixture
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereExtratimeAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereExtratimeHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereFixtureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereHalftimeAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereHalftimeHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result wherePenaltiesAway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result wherePenaltiesHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Result whereUserId($value)
 * @mixin \Eloquent
 */
class Result extends Score
{
    protected $table = 'scores';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ResultScope());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\User
     */
    public function user()
    {
        return $this->belongsTo(OverrideModels::getUserModel(), 'user_id');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('poulesystem.predictions.show', $this->id);
    }
}
