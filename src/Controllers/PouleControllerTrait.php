<?php namespace Stefandebruin\PouleSystem\Controllers;

use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 27-12-17
 * Time: 10:54
 */

trait PouleControllerTrait
{

    protected function getPageSize(Request $request, String $model, $countAll = 0)
    {
        $size = null;
        if ($request->has('size')) {
            $size = $request->get('size', null);
            if ($size == 0) {
                $size = app()->make($model)->count();
            }
        }
        return $size;
    }
}
