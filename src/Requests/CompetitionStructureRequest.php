<?php

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;


class CompetitionStructureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*' => ['required', 'array'],
            '*.phaseId' => ['required', 'exists:phases,id'],
            '*.groups' => ['required', 'array'],
            '*.groups.*.id' => ['required', 'exists:groups,id'],
            '*.groups.*.teams' => ['required', 'array'],
            '*.groups.*.teams.*.id' => ['required', 'exists:teams,id'],
        ];
    }
}
