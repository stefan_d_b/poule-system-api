<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories\Api;


use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;

interface FixtureRepositoryInterface
{

    public function model(): Fixture;

    public function save(Fixture $competition):Fixture;

    public function find(int $id);

    public function findByExternalId($id);
}