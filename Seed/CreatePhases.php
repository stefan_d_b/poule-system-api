<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 01-09-17
 * Time: 08:20
 */

namespace Stefandebruin\PouleSystem\Seed;

use Illuminate\Database\Seeder;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Phase;

class CreatePhases extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phases = [
            app()->make(Phase::class)->fill(['key' => 'group', 'title' => 'Groups']),
            app()->make(Phase::class)->fill(['key' => '16fin', 'title' => '16 Finals']),
            app()->make(Phase::class)->fill(['key' => '8fin', 'title' => '8 Finals']),
            app()->make(Phase::class)->fill(['key' => 'quart', 'title' => 'Quarter final']),
            app()->make(Phase::class)->fill(['key' => 'semi', 'title' => 'Semi Final']),
            app()->make(Phase::class)->fill(['key' => 'final', 'title' => 'Final']),
            app()->make(Phase::class)->fill(['key' => '34place', 'title' => '3/4 place']),
        ];

        $foundComeptitions = app()->make(Phase::class)->All();

        foreach($phases as $phase){
            if($foundComeptitions->where('key', $phase->key)->count() > 0){
                $found = $foundComeptitions->where('key', $phase->key)->first();
                $found->fill($phase->toArray());
                $found->save();
            }else{
                $phase->save();
            }
        }
    }
}