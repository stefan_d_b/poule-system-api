<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 19-11-17
 * Time: 13:32
 */

namespace Stefandebruin\PouleSystem\DataModels;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Str;
use JsonSerializable;

class DataModel // implements ArrayAccess, Arrayable, Jsonable, JsonSerializable
{
    use \Illuminate\Database\Eloquent\Concerns\HasAttributes;
    use \Illuminate\Database\Eloquent\Concerns\HasEvents;
//        \Illuminate\Database\Eloquent\Concerns\HasGlobalScopes,
    use \Illuminate\Database\Eloquent\Concerns\HasRelationships;
    use \Illuminate\Database\Eloquent\Concerns\HasTimestamps;
//        \Illuminate\Database\Eloquent\Concerns\HidesAttributes,
    use \Illuminate\Database\Eloquent\Concerns\GuardsAttributes;

//        \Illuminate\Support\Traits\ForwardsCalls;

//    /**
//     * The table associated with the model.
//     *
//     * @var string
//     */
//    protected $table;
//
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
//
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'int';
//
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
//
//    /**
//     * The number of models to return for pagination.
//     *
//     * @var int
//     */
//    protected $perPage = 15;
//
//    /**
//     * Indicates if the model exists.
//     *
//     * @var bool
//     */
//    public $exists = false;
//
//    /**
//     * Indicates if the model was inserted during the current request lifecycle.
//     *
//     * @var bool
//     */
//    public $wasRecentlyCreated = false;
//
//    /**
//     * The event dispatcher instance.
//     *
//     * @var \Illuminate\Contracts\Events\Dispatcher
//     */
//    protected static $dispatcher;
//
    /**
     * The array of booted models.
     *
     * @var array
     */
    protected static $booted = [];

    /**
     * The array of trait initializers that will be called on each new instance.
     *
     * @var array
     */
    protected static $traitInitializers = [];
//
//    /**
//     * The array of global scopes on the model.
//     *
//     * @var array
//     */
//    protected static $globalScopes = [];
//
//    /**
//     * The list of models classes that should not be affected with touch.
//     *
//     * @var array
//     */
//    protected static $ignoreOnTouch = [];
//
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';
//

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();

        $this->initializeTraits();

        $this->fill($attributes);
    }
//

    /**
     * Check if the model needs to be booted and if so, do it.
     *
     * @return void
     */
    protected function bootIfNotBooted()
    {
        if (!isset(static::$booted[static::class])) {
            static::$booted[static::class] = true;

            $this->fireModelEvent('booting', false);

            static::boot();

            $this->fireModelEvent('booted', false);
        }
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        self::bootTraits();
    }

    /**
     * Boot all of the bootable traits on the model.
     *
     * @return void
     */
    protected static function bootTraits()
    {
        $class = static::class;

        $booted = [];

        static::$traitInitializers[$class] = [];

        foreach (class_uses_recursive($class) as $trait) {
            $method = 'boot' . class_basename($trait);

            if (method_exists($class, $method) && !in_array($method, $booted)) {
                forward_static_call([$class, $method]);

                $booted[] = $method;
            }

            if (method_exists($class, $method = 'initialize' . class_basename($trait))) {
                static::$traitInitializers[$class][] = $method;

                static::$traitInitializers[$class] = array_unique(
                    static::$traitInitializers[$class]
                );
            }
        }
    }
//

    /**
     * Initialize any initializable traits on the model.
     *
     * @return void
     */
    protected function initializeTraits()
    {
        foreach (static::$traitInitializers[static::class] as $method) {
            $this->{$method}();
        }
    }

//    /**
//     * Clear the list of booted models so they will be re-booted.
//     *
//     * @return void
//     */
//    public static function clearBootedModels()
//    {
//        static::$booted = [];
//
//        static::$globalScopes = [];
//    }
//
//    /**
//     * Disables relationship model touching for the current class during given callback scope.
//     *
//     * @param  callable  $callback
//     * @return void
//     */
//    public static function withoutTouching(callable $callback)
//    {
//        static::withoutTouchingOn([static::class], $callback);
//    }
//
//    /**
//     * Disables relationship model touching for the given model classes during given callback scope.
//     *
//     * @param  array  $models
//     * @param  callable  $callback
//     * @return void
//     */
//    public static function withoutTouchingOn(array $models, callable $callback)
//    {
//        static::$ignoreOnTouch = array_values(array_merge(static::$ignoreOnTouch, $models));
//
//        try {
//            call_user_func($callback);
//        } finally {
//            static::$ignoreOnTouch = array_values(array_diff(static::$ignoreOnTouch, $models));
//        }
//    }
//
//    /**
//     * Determine if the given model is ignoring touches.
//     *
//     * @param  string|null  $class
//     * @return bool
//     */
//    public static function isIgnoringTouch($class = null)
//    {
//        $class = $class ?: static::class;
//
//        foreach (static::$ignoreOnTouch as $ignoredClass) {
//            if ($class === $ignoredClass || is_subclass_of($class, $ignoredClass)) {
//                return true;
//            }
//        }
//
//        return false;
//    }
//
    /**
     * Fill the model with an array of attributes.
     *
     * @param  array $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        $totallyGuarded = $this->totallyGuarded();

        foreach ($this->fillableFromArray($attributes) as $key => $value) {
            // The developers may` choose to place some attributes in the "fillable" array
            // which means only those attributes may be set through mass assignment to
            // the model, and all others will just get ignored for security reasons.
            if ($this->isFillable($key)) {
                $this->setAttribute($key, $value);
            } elseif ($totallyGuarded) {
                throw new MassAssignmentException(sprintf(
                    'Add [%s] to fillable property to allow mass assignment on [%s].',
                    $key,
                    get_class($this)
                ));
            }
        }

        return $this;
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return $this->incrementing;
    }

//
//    /**
//     * Fill the model with an array of attributes. Force mass assignment.
//     *
//     * @param  array  $attributes
//     * @return $this
//     */
//    public function forceFill(array $attributes)
//    {
//        return static::unguarded(function () use ($attributes) {
//            return $this->fill($attributes);
//        });
//    }
//
//    /**
//     * Qualify the given column name by the model's table.
//     *
//     * @param  string  $column
//     * @return string
//     */
//    public function qualifyColumn($column)
//    {
//        if (Str::contains($column, '.')) {
//            return $column;
//        }
//
//        return $this->getTable().'.'.$column;
//    }
//
//    /**
//     * Create a new instance of the given model.
//     *
//     * @param  array  $attributes
//     * @param  bool  $exists
//     * @return static
//     */
//    public function newInstance($attributes = [], $exists = false)
//    {
//        // This method just provides a convenient way for us to generate fresh model
//        // instances of this current model. It is particularly useful during the
//        // hydration of new objects via the Eloquent query builder instances.
//        $model = new static((array) $attributes);
//
//        $model->exists = $exists;
//
//        $model->setConnection(
//            $this->getConnectionName()
//        );
//
//        return $model;
//    }
//
//    /**
//     * Create a new model instance that is existing.
//     *
//     * @param  array  $attributes
//     * @param  string|null  $connection
//     * @return static
//     */
//    public function newFromBuilder($attributes = [], $connection = null)
//    {
//        $model = $this->newInstance([], true);
//
//        $model->setRawAttributes((array) $attributes, true);
//
//        $model->fireModelEvent('retrieved', false);
//
//        return $model;
//    }
//
//    /**
//     * Eager load relations on the model if they are not already eager loaded.
//     *
//     * @param  array|string  $relations
//     * @return $this
//     */
//    public function loadMissing($relations)
//    {
//        $relations = is_string($relations) ? func_get_args() : $relations;
//
//        $this->newCollection([$this])->loadMissing($relations);
//
//        return $this;
//    }
//
//    /**
//     * Increment the underlying attribute value and sync with original.
//     *
//     * @param  string  $column
//     * @param  float|int  $amount
//     * @param  array  $extra
//     * @param  string  $method
//     * @return void
//     */
//    protected function incrementOrDecrementAttributeValue($column, $amount, $extra, $method)
//    {
//        $this->{$column} = $this->{$column} + ($method == 'increment' ? $amount : $amount * -1);
//
//        $this->forceFill($extra);
//
//        $this->syncOriginalAttribute($column);
//    }
//    /**
//     * Create a new Eloquent Collection instance.
//     *
//     * @param  array  $models
//     * @return \Illuminate\Database\Eloquent\Collection
//     */
//    public function newCollection(array $models = [])
//    {
//        return new Collection($models);
//    }
//
//    /**
//     * Create a new pivot model instance.
//     *
//     * @param  \Illuminate\Database\Eloquent\Model  $parent
//     * @param  array  $attributes
//     * @param  string  $table
//     * @param  bool  $exists
//     * @param  string|null  $using
//     * @return \Illuminate\Database\Eloquent\Relations\Pivot
//     */
//    public function newPivot(self $parent, array $attributes, $table, $exists, $using = null)
//    {
//        return $using ? $using::fromRawAttributes($parent, $attributes, $table, $exists)
//            : Pivot::fromAttributes($parent, $attributes, $table, $exists);
//    }
//
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return array_merge($this->attributesToArray(), $this->relationsToArray());
    }
//
    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     *
     * @throws \Illuminate\Database\Eloquent\JsonEncodingException
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }
//
    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
//
//    /**
//     * Clone the model into a new, non-existing instance.
//     *
//     * @param  array|null  $except
//     * @return \Illuminate\Database\Eloquent\Model
//     */
//    public function replicate(array $except = null)
//    {
//        $defaults = [
//            $this->getKeyName(),
//            $this->getCreatedAtColumn(),
//            $this->getUpdatedAtColumn(),
//        ];
//
//        $attributes = Arr::except(
//            $this->attributes, $except ? array_unique(array_merge($except, $defaults)) : $defaults
//        );
//
//        return tap(new static, function ($instance) use ($attributes) {
//            $instance->setRawAttributes($attributes);
//
//            $instance->setRelations($this->relations);
//        });
//    }
//
//    /**
//     * Determine if two models have the same ID and belong to the same table.
//     *
//     * @param  \Illuminate\Database\Eloquent\Model|null  $model
//     * @return bool
//     */
//    public function is($model)
//    {
//        return ! is_null($model) &&
//            $this->getKey() === $model->getKey() &&
//            $this->getTable() === $model->getTable() &&
//            $this->getConnectionName() === $model->getConnectionName();
//    }
//
//    /**
//     * Determine if two models are not the same.
//     *
//     * @param  \Illuminate\Database\Eloquent\Model|null  $model
//     * @return bool
//     */
//    public function isNot($model)
//    {
//        return ! $this->is($model);
//    }
//
    /**
     * Get the primary key for the model.
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->primaryKey;
    }
//
//    /**
//     * Set the primary key for the model.
//     *
//     * @param  string  $key
//     * @return $this
//     */
//    public function setKeyName($key)
//    {
//        $this->primaryKey = $key;
//
//        return $this;
//    }
//
    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return $this->keyType;
    }

//
//    /**
//     * Set the data type for the primary key.
//     *
//     * @param  string  $type
//     * @return $this
//     */
//    public function setKeyType($type)
//    {
//        $this->keyType = $type;
//
//        return $this;
//    }
//
//
//    /**
//     * Get the value of the model's primary key.
//     *
//     * @return mixed
//     */
//    public function getKey()
//    {
//        return $this->getAttribute($this->getKeyName());
//    }
//
//    /**
//     * Get the default foreign key name for the model.
//     *
//     * @return string
//     */
//    public function getForeignKey()
//    {
//        return Str::snake(class_basename($this)).'_'.$this->getKeyName();
//    }
//
//    /**
//     * Get the number of models to return per page.
//     *
//     * @return int
//     */
//    public function getPerPage()
//    {
//        return $this->perPage;
//    }
//
//    /**
//     * Set the number of models to return per page.
//     *
//     * @param  int  $perPage
//     * @return $this
//     */
//    public function setPerPage($perPage)
//    {
//        $this->perPage = $perPage;
//
//        return $this;
//    }
//
    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }
//
//    /**
//     * Determine if the given attribute exists.
//     *
//     * @param  mixed  $offset
//     * @return bool
//     */
//    public function offsetExists($offset)
//    {
//        return ! is_null($this->getAttribute($offset));
//    }
//
//    /**
//     * Get the value for a given offset.
//     *
//     * @param  mixed  $offset
//     * @return mixed
//     */
//    public function offsetGet($offset)
//    {
//        return $this->getAttribute($offset);
//    }
//
//    /**
//     * Set the value for a given offset.
//     *
//     * @param  mixed  $offset
//     * @param  mixed  $value
//     * @return void
//     */
//    public function offsetSet($offset, $value)
//    {
//        $this->setAttribute($offset, $value);
//    }
//
//    /**
//     * Unset the value for a given offset.
//     *
//     * @param  mixed  $offset
//     * @return void
//     */
//    public function offsetUnset($offset)
//    {
//        unset($this->attributes[$offset], $this->relations[$offset]);
//    }
//
    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return $this->offsetExists($key);
    }

    /**
    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        $this->offsetUnset($key);
    }

    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (in_array($method, ['increment', 'decrement'])) {
            return $this->$method(...$parameters);
        }

        return $this->forwardCallTo($this->newQuery(), $method, $parameters);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }

    /**
     * Convert the model to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * When a model is being unserialized, check if it needs to be booted.
     *
     * @return void
     */
    public function __wakeup()
    {
        $this->bootIfNotBooted();
    }
}