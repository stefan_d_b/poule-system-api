<?php namespace Stefandebruin\PouleSystem\Requests;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 21-01-18
 * Time: 17:41
 */

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Validation\Validator;
use Stefandebruin\PouleSystem\Exceptions\FormValidationException;

class OLDJsonApiRequest extends Request{
    protected $dataType = '';
    /**
     * Create the default validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Factory  $factory
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function createDefaultValidator(ValidationFactory $factory)
    {
        if(!$this->bulkHeader() && !$this->bulkRoute()){
            $method = "rulesCreate";
            if($this->method() === "PUT" || $this->method() === "PATCH") $method = "rulesUpdate";

            $rules = $this->addDefaultRules(
                method_exists($this, $method) ? $this->$method() : $this->rules(),
                $this->method(),
                'data.'
            );
        }else if($this->bulkHeader() && !$this->bulkRoute()){
            $method = "rulesCreate";
            if($this->method() === "PUT" || $this->method() === "PATCH") $method = "rulesUpdate";

            $rules = $this->addDefaultRules(
                method_exists($this, $method) ? $this->$method() : $this->rules(),
                $this->method(),
                'data.*.'
            );
        }else{
            $rules = [];
            foreach(array_keys($this->all()) as $postMethod){
                $method = "rulesCreate";
                if($postMethod === "PUT" || $postMethod === "PATCH") $method = "rulesUpdate";

                $rules = array_merge($this->addDefaultRules(
                    method_exists($this, $method) ? $this->$method() : $this->rules(),
                    $postMethod,
                    $postMethod.'.data.*.'
                ), $rules);
            }
        }

        return $factory->make(
            $this->validationData(), $rules,
            $this->messages(), $this->attributes()
        );
    }

    public function bulkRoute(){
        return ends_with($this->route()->uri, '/bulk');
    }

    public function bulkHeader(){
        return str_contains(request()->header('accept'), 'ext=bulk');
    }

    private function addDefaultRules($attributeRules, $method, $dataPrefix = ''){
        $attributeRules = array_combine(
            array_map(function($key){ return 'attributes.'.$key; }, array_keys($attributeRules)),
            $attributeRules
        );

        $rules = array_merge([
            'type' => ['required', 'string', 'equals:'.$this->dataType],
            'id' => ['string', 'exists:scores,id', $method === 'POST' ? 'notAllowed' : 'required'],
            'attributes' => ['required', 'array']
        ], $attributeRules);


        return array_combine(
            array_map(function($key) use ($dataPrefix) { return $dataPrefix.$key; }, array_keys($rules)),
            $rules
        );
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new FormValidationException($validator));
    }
}