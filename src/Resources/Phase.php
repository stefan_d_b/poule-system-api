<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\PouleSystem\Models\DynamicRelationTrait;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;

class OldPhase extends Resource
{
    use RelationTrait;

    /**
     * @var array
     */
    protected $primaryRelations = [
        'competition',
        'group',
        'fixtures',
    ];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->links = [
            'phases' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'teams' => route('poulesystem.competitions.teams', ['competition' => $this->resource->id]),
            'groups' => route('poulesystem.competitions.groups', ['competition' => $this->resource->id]),
            'fixtures' => route('poulesystem.competitions.fixtures', ['competition' => $this->resource->id]),
        ];
        $this->dataRoot = $dataRoot;
    }

    public function toArray($request)
    {
        $relations = $this->getRelationships($request);
        return [
            'type' => 'phases',
            'id' => (string) $this->resource->id,
            'attributes' => [
                'key' => $this->resource->key,
                'title' => $this->resource->title,
            ],
            'links' => [
                'self' => route('poulesystem.phases.show', ['phase' => $this->resource->id])
            ],
            'relationship' => $this->when(count($relations) > 0, $relations)
        ];
    }
}
