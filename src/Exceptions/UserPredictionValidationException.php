<?php namespace Stefandebruin\PouleSystem\Exceptions;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 23-09-17
 * Time: 13:15
 */

use Illuminate\Validation\ValidationException;

class UserPredictionValidationException extends ValidationException
{
    public function __construct($validator, $response = null)
    {
        parent::__construct($validator, $response);
    }

    /**
     * Get all of the validation error messages.
     *
     * @return array
     */
    public function errors()
    {
        $messages = [];
        foreach ($this->validator->errors()->messages() as $key => $value) {
            list($index, $key) = explode(".", $key);
            $messages[$index][$key] = $value;
        }

        return $messages;
    }
}
