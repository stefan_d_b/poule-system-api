<?php

namespace Stefandebruin\PouleSystem\Resources;

use Stefandebruin\JsonApi\Exceptions\UnknownModelType;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Service\ModelInformation;

class Fixture extends AbstractJsonApi
{
    public function toArray($request)
    {
        $type = array_search(get_class($this->resource), config('json-api.types'));
        if (empty($type)) {
            throw new UnknownModelType('Unknown type for model: ' . get_class($this->resource));
        }

        return [
            'type' => $type,
            'id' => (string)$this->resource->getKey(),
            'attributes' => $this->getResourceAttributes($request, $type),
            'links' => $this->getResourceLinks($type, $request),
            'relationships' => $this->getRelationships($this->resource)
        ];
    }

    protected function buildInclude($resource, $path)
    {
        dd(234);
        $parameters = str_plural(strtolower(class_basename($resource)));
        $information = app()->make(ModelInformation::class);
        $modelRelations = $information->relations($resource);

//        var_dump(get_class($resource), $this->getRelationsResources($parameters));
        foreach ($this->getRelationsResources($parameters) as $relation) {
//            var_dump(get_class($resource), $relation, $path, $this->hasChildRelation($path.'.'.$relation));
            $path = trim($path.'.'.$relation, '.');
//            var_dump($path, $relation, $this->hasChildRelation($path));
//            echo '<br />';

            if (empty($relation)) {
                continue;
            }
            $resource->load($relation);
            $modelRelationData = $resource->getRelation($relation);
            if (!is_null($modelRelationData)) {
                if (!$modelRelations[$relation]['manyRelation']) {
                    if (!array_has($this->with, $relation . '.' . $modelRelationData->getKey())) {
                        $resourceClass = !is_null($modelRelationData->getResourceClass()) ?
                            $modelRelationData->getResourceClass() : AbstractJsonApi::class;
//                        echo $relation . '.' . $modelRelationData->getKey();

                        $this->with[$relation . '.' . $modelRelationData->getKey()] = new $resourceClass($modelRelationData, false);
                        if($this->hasChildRelation($path)){
                            $this->buildInclude($modelRelationData, $path);
                        }
                    }
                } else {
                    foreach ($modelRelationData as $relationItem) {
//                        var_dump($relation . '.' . $relationItem->getKey(), $path);
                        if (!array_has($this->with, $relation . '.' . $relationItem->getKey())) {
                            $resourceClass = !is_null($relationItem->getResourceClass()) ?
                                $relationItem->getResourceClass() : AbstractJsonApi::class;
                            $this->with[$relation . '.' . $relationItem->getKey()] = new $resourceClass($relationItem, false);
                            if($this->hasChildRelation($path)){
//                                echo 1;
                                $this->buildInclude($relationItem, $path);
                            }
                        }
                    }
                }
            }
        }
    }
}
