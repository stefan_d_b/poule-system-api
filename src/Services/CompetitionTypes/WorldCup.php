<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-07-18
 * Time: 11:32
 */

namespace Stefandebruin\PouleSystem\Services\CompetitionTypes;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Repositories\Api\FixtureRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\TeamRepository;

class WorldCup implements CompetitionTypeInterface
{
    private $competition;
    /**
     * @var FixtureRepositoryInterface
     */
    private $fixtureRepository;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * WorldCup constructor.
     * @param FixtureRepositoryInterface $fixtureRepository
     * @param TeamRepository $teamRepository
     * @param GroupRepositoryInterface $groupRepository
     * @param $competition
     */
    public function __construct(
        FixtureRepositoryInterface $fixtureRepository,
        TeamRepository $teamRepository,
        GroupRepositoryInterface $groupRepository,
        $competition
    ) {
        $this->fixtureRepository = $fixtureRepository;
        $this->teamRepository = $teamRepository;
        $this->groupRepository = $groupRepository;
        $this->competition = $competition;
    }

    /**
     * @param Phase $phase
     * @param $fixture
     * @return mixed
     */
    public function connectedToPhase(Phase $phase, $fixture): bool
    {
        $selfUrl = explode("/", $fixture->_links->self->href);
        $fixtureId = last($selfUrl);

        try {
            /** @var Fixture $fixtureModel */
            $fixtureModel = $this->fixtureRepository->findByExternalId($fixtureId);
            return !(!is_null($fixtureModel->phase_id) && $fixtureModel->phase_id != $phase->id);
        } catch (ModelNotFoundException $modelNotFoundException) {
            return true;
        }
    }

    /**
     * @param Phase $phase
     * @param $fixture
     * @return Group
     */
    public function findCorrectGroup(Phase $phase, $fixture): Group
    {
        $homeTeamUrl = explode("/", $fixture->_links->homeTeam->href);
        $awayTeamUrl = explode("/", $fixture->_links->awayTeam->href);

        $homeTeamId = last($homeTeamUrl);
        $awayTeamId = last($awayTeamUrl);

        try {
            $firstGroupId = $this->getGroupIdByTeamId(
                $phase->groups,
                $this->teamRepository->getByApiId($homeTeamId)->id
            );
            $secondGroupId = $this->getGroupIdByTeamId(
                $phase->groups,
                $this->teamRepository->getByApiId($awayTeamId)->id
            );

            if ($firstGroupId === $secondGroupId && $firstGroupId > 0) {
                return $this->groupRepository->find($secondGroupId);
            }
        } catch (ModelNotFoundException $exception) {
            throw $exception;
        }
    }

    /**
     * @param Collection $fixtures
     * @return Competition
     */
    public function beforeCreatingFixtures(Collection $fixtures): Competition
    {
        return $this->competition;
    }

    /**
     * @param $groups
     * @param $teamId
     * @return int
     */
    private function getGroupIdByTeamId($groups, $teamId)
    {
        /** @var Group $group */
        foreach ($groups as $group) {
            if ($group->teams->where('id', $teamId)->count() > 0) {
                return $group->id;
            }
        }

        return 0;
    }

    public function needSwitchPhase(Phase $phase, $index): bool
    {
        if ($phase->key === 'group' && $index === (8 * 6) || $phase->fixtures->count() === (8 * 6)) {
            return true;
        } elseif ($phase->key === '8finale' && $index === 8) {
            return true;
        } elseif ($phase->key === '8finale' && $index === 4) {
            return true;
        }
        return false;
    }
}
