<?php

namespace Stefandebruin\PouleSystem\Test;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\WithoutMiddleware;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use WithoutMiddleware;

    protected function getPackageProviders($app)
    {
        return [
            'Stefandebruin\PouleSystem\PouleSystemServiceProvider',
            \Stefandebruin\PouleSystem\PouleSystemServiceProvider::class
        ];
    }

    /**
     * Resolve application HTTP Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return void
     */
    protected function resolveApplicationHttpKernel($app)
    {
        $app->singleton('Illuminate\Contracts\Http\Kernel', 'Stefandebruin\PouleSystem\Test\Data\Http\Kernel');
    }
}
