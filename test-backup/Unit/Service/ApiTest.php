<?php

use Stefandebruin\PouleSystem\Test\TestCase;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 21/10/2018
 * Time: 12:54
 */
class ApiTest extends TestCase
{
    /**
     *
     */
    public function testIfClassExist()
    {
        $class = new \Stefandebruin\PouleSystem\Services\Api(new \GuzzleHttp\Client([]));
        $this->assertInstanceOf("\Stefandebruin\PouleSystem\Services\Api", $class);
    }

    public function testGetCompetitions()
    {
        $response_200 = json_encode(array("status" => "successful"));

        $mock = new MockHandler([
            new Response(200, [], $response_200),
        ]);

        $handler = HandlerStack::create($mock);


        $class = new \Stefandebruin\PouleSystem\Services\Api(new \GuzzleHttp\Client(['handler' => $handler]));
        $response = $class->getCompetitions();
        $this->assertInstanceOf(\Illuminate\Support\Collection::class, $response);
    }
}
