<?php

namespace Stefandebruin\PouleSystem\Resources;

use ClassesWithParents\E;
use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;

class OldGroup extends Resource
{
    use RelationTrait;

    /**
     * @var array
     */
    protected $primaryRelations = [
        'phase',
        'fixture',
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $rootResource = null;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->links = [
            'phases' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'phase' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'fixtures' => route('poulesystem.competitions.fixtures', ['competition' => $this->resource->id]),
        ];
    }

    public function toArray($request)
    {
        $relations = $this->getRelationships($request);

        return [
            'type' => 'groups',
            'id' => (string) $this->resource->id,
            'attributes' => [
                'title' => $this->resource->title,
                'created_at' => (string) $this->resource->created_at,
                'updated_at' => (string) $this->resource->updated_at,
            ],
            'links' => [
                'self' => route('poulesystem.group.show', ['group' => $this->resource->id])
            ],
            'relationships' => $this->when(count($relations) > 0, $relations)
        ];
    }

    public function with($request)
    {
        dd(1234);
    }
}
