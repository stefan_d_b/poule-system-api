<?php namespace Stefandebruin\PouleSystem;

use App\User;
use Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Log;
use Route;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\PouleSystem\Console\Import\Competitions;
use Stefandebruin\PouleSystem\Console\Import\Fixtures;
use Stefandebruin\PouleSystem\Console\Import\OfficialResult;
use Stefandebruin\PouleSystem\Console\Import\Teams;
use Stefandebruin\PouleSystem\Console\Install;
use Stefandebruin\PouleSystem\Console\Notifications\UpgradeDatabase;
use Stefandebruin\PouleSystem\Console\Notifications\UserMissingScores;
use Stefandebruin\PouleSystem\Console\Testing\Users;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Illuminate\Database\Eloquent\Relations\Relation;
use Stefandebruin\PouleSystem\Models\Prediction;
use Stefandebruin\PouleSystem\Models\Team;
use Stefandebruin\PouleSystem\Services\Api;
use Stefandebruin\PouleSystem\Services\DomainService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PouleSystemServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    public static $relations = [];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/middleware.php' => config_path('middleware.php'),
        ], 'config');

        $this->loadMigrationsFrom(__DIR__ . '/../migrations');

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        $this->publishes([
            __DIR__ . '/../factories/factories.php' => database_path('factories') . '/PoueSystemFactory.php'
        ], 'migrations');

        $this->_bootValidationRules();
        $this->_bootBindings();
        $this->_bootCli();
        $this->_bootRouteModels();
        $this->_boundDomain();
        $this->_bootEvents();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('poulesystem-override-models', OverrideModels::class);
    }

    private function _bootValidationRules()
    {
        Validator::extend('translate', function ($attribute, $value, $parameters, $validator) {
            $attributeKey = JsonApi::getResourceType($attribute, '.');
            $resource = JsonApi::getResourceType();
            $class = JsonApi::getResourceClass($resource);

            if (empty($value)) return false;
            if (is_string($value)) return true;
            if (!is_array($value)) return false;

            $correct = true;
            foreach ($value as $language => $translation) {
                if (strlen($language) != 2 || empty($translation) || !is_string($translation)) {
                    $correct = false;
                }
            }

            return $correct;
        });

        Validator::extend('goals', function ($attribute, $value, $parameters) {
            list($index, $key) = explode(".", $attribute);
            $keyChange = [
                'home_goals' => 'away_goals',
                'away_goals' => 'home_goals',
            ];
            $postData = request()->get($index);

            if (!empty($value) && !empty($postData[$keyChange[$key]])) return true;
            return false;
        });

        Validator::extendImplicit('penalty_requiredfffeef', function ($attribute, $value, $parameters) {
            list($index, $key) = explode(".", $attribute);
            $postData = request()->get($index);

            /** @var Competition $competition */
            $competition = Competition::find(request()->route()->parameters['competition_id']);
            /** @var Fixture $fixture */
            $fixture = Fixture::find($index);
            if (!$fixture instanceof Fixture) return true;

            /** @var Phase $phase */
            $phase = $competition->phases->where('id', $fixture->phase_id)->first();
            if (!$phase->pivot->penalties) return true;

            if (is_null($value) &&
                array_has($postData, 'home_goals') && array_has($postData, 'away_goals') &&
                $postData['home_goals'] == $postData['away_goals']) return false;

            return true;
        });


        Validator::extend('equals', function ($attribute, $value, $parameters, $validator) {
            return $value == $parameters[0];
        });

        Validator::extend('fixtureExist', function ($attribute, $value, $parameters, $validator) {
            $fixture = app()->make(Fixture::class)->find($value);
            return $fixture instanceof Fixture;
        });

        Validator::extend('TeamConnected', function ($attribute, $value, $parameters, $validator) {
            /** @var Fixture $fixture */
            $fixture = app()->make(Fixture::class)->find($parameters[0]);
            return $value == $fixture->home_team || $value == $fixture->away_team;
        });

        Validator::extendImplicit('penaltiesRequired', function ($attribute, $value, $parameters, $validator) {
//            //TODO-stefan add check for setting
//            $attributes = $validator->getData()['data']['attributes'];
//            if(!array_has($attributes, 'fixture_id')) return true;
//
//                $fixture = app()->make(Fixture::class)->find($attributes['fixture_id']);
//            if($fixture instanceof Fixture && $attributes['home_goals'] === $attributes['away_goals']){
//                return array_has($attributes, 'penalty');
//            }
            return true;
        });
    }

    private function _bootCli()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
                Competitions::class,
                Teams::class,
                Fixtures::class,
                OfficialResult::class,
                Users::class,
                UserMissingScores::class,
                UpgradeDatabase::class,
                \Stefandebruin\PouleSystem\Console\Testing\OfficialResult::class,
                \Stefandebruin\PouleSystem\Console\Testing\PodiumBroadcast::class,
            ]);
        }
    }

    private function _bootBindings()
    {
        $this->app->bind(
            'Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface',
            'Stefandebruin\PouleSystem\Repositories\GroupRepository'
        );

        $this->app->bind(
            'Stefandebruin\PouleSystem\Repositories\Api\FixtureRepositoryInterface',
            'Stefandebruin\PouleSystem\Repositories\FixtureRepository'
        );

        $this->app->bind(
            'Stefandebruin\PouleSystem\Repositories\Api\ScoreRepositoryInterface',
            'Stefandebruin\PouleSystem\Repositories\ScoreRepository'
        );

        $this->app->bind(
            'Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface',
            'Stefandebruin\PouleSystem\Repositories\CompetitionRepository'
        );

        $this->app->bind(
            'Stefandebruin\PouleSystem\Repositories\Api\UserRepositoryInterface',
            'Stefandebruin\PouleSystem\Repositories\UserRepository'
        );

        $this->app->bind(
            'Stefandebruin\PouleSystem\Repositories\Api\PhaseRepositoryInterface',
            'Stefandebruin\PouleSystem\Repositories\PhaseRepository'
        );

        $this->app->singleton(
            \Illuminate\Contracts\Console\Kernel::class,
            \Stefandebruin\PouleSystem\Console\Kernel::class
        );

        Relation::morphMap([
            'teams' => 'Stefandebruin\PouleSystem\Models\Team',
            'phases' => 'Stefandebruin\PouleSystem\Models\Phase',
            'groups' => 'Stefandebruin\PouleSystem\Models\Group',
            'competitions' => 'Stefandebruin\PouleSystem\Models\Competition',
        ]);

        $this->app->bind('Api', function ($app) {
            $client = new \GuzzleHttp\Client([
                'base_uri' => 'http://api.football-data.org/v1/',
                'headers' => [
                    'X-Auth-Token' => config('poulesystem.api_key'),
                    'X-Response-Control' => 'full',
                ]
            ]);

            return new Api($client);
        });

        self::addDataProvider(
            \Stefandebruin\PouleSystem\Services\ApiGateway\FootballData::class,
            'football-data'
        );
    }

    private function _bootRouteModels()
    {
        Route::model('phase', Phase::class, function () {
            throw new NotFoundHTTPException;
        });

        Route::model('user', User::class, function () {
            throw new NotFoundHTTPException;
        });

        Route::model('group', Group::class, function () {
            throw new NotFoundHTTPException;
        });

        Route::model('prediction', Prediction::class, function () {
            throw new NotFoundHTTPException;
        });

        Route::model('predictions', Prediction::class, function () {
            throw new NotFoundHTTPException;
        });

        Route::model('team', Team::class, function () {
            throw new NotFoundHTTPException;
        });
    }

    private function _boundDomain()
    {
//        dd(request());
    }

    private function _bootEvents()
    {
//        Event::listen(\Illuminate\Auth\Events\Login::class, function ($user) {
//            dd($user);
//        });

//        Event::listen(
//            \Illuminate\Auth\Events\Login::class,
//            \Stefandebruin\PouleSystem\Listeners\UserLoggedIn::class
//        );
//
//        Event::listen(
//            \Illuminate\Routing\Events\RouteMatched::class,
//            \Stefandebruin\PouleSystem\Listeners\RouteMatchedListener::class
//        );
    }

    /**
     * @return int
     * @throws \Exception
     */
    public static function boundCompetitionId($id = null)
    {
        $competitionId = !is_null($id) ? $id : false;
        if (!app()->runningInConsole() && request()->competition instanceof Competition)
            $competitionId = request()->competition->id;

        if (app()->bound('competitionId')) $competitionId = app()->make('competitionId')->id;
        if (!$competitionId && !app()->runningInConsole()) {
            throw new \Exception('no competition given to filter');
        } elseif (!$competitionId && app()->runningInConsole()) {
            return 0;
        }

        return $competitionId;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public static function boundPhaseId()
    {
        $phaseId = false;
        if (!app()->runningInConsole() && request()->phase instanceof Phase)
            $phaseId = request()->phase->id;

        if (app()->bound('phaseId')) $phaseId = app()->make('phaseId')->id;

        if (!$phaseId && !app()->runningInConsole()) {
            throw new \Exception('no competition given to filter');
        } elseif (!$phaseId && app()->runningInConsole()) {
            return 0;
        }

        return $phaseId;
    }

    public static function getUsersForDomain()
    {
        return [];

        if (app()->bound('pouleDomain')) {
            return app()->make('pouleDomain')->getRelation('users')->pluck('id')->toArray();
        }
        return [];
    }


    public static function __callStatic($method, $value = null)
    {
        if (starts_with($method, 'load')) {
            $relation = str_replace("load", "", str_replace("Relation", "", $method));
            return array_get(self::$relations, strtolower($relation), false);
        } else if (starts_with($method, "set")) {
            $relation = str_replace("set", "", str_replace("Relation", "", $method));
            self::$relations[strtolower($relation)] = $value[0];
        }
    }

    public static function addDataProvider($class, $alias = null){
        if (is_null($alias)) {
            $alias = (new \ReflectionClass($class))->getShortName();
        }

        if (!app()->bound('poulesystem.gateway.' . $alias)) {
            app()->singleton($alias, $class);
        }
    }
}
