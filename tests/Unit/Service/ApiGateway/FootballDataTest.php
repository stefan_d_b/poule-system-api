<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-16
 * Time: 15:08
 */

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class FootballDataTest extends \Stefandebruin\PouleSystem\Test\TestCase
{
    /**
     * @var \GuzzleHttp\Handler\MockHandler;
     */
    private $mockHandler;

    public function setup(){
        parent::setup();



    }

    /**
     * @dataProvider getLeaguesProvider
     */
    public function testGetLeagues($guzzleResponseBody){


                // Create a mock and queue two responses.
        $mockHandler = new MockHandler([
            new Response(
                200,
                ['Content-Type' => 'application/json'],
                json_encode($guzzleResponseBody)
            ),
        ]);

        $handler = HandlerStack::create($mockHandler);
        $client = new Client(['handler' => $handler]);

        $gateway = new \Stefandebruin\PouleSystem\Services\ApiGateway\FootballData($client);

        $gateway->getLeagues();
    }

    public function getLeaguesProvider()
    {
        return [
            //aanmaken
            [
                ['dfg' => 'dfg']
            ]
        ];
    }
}