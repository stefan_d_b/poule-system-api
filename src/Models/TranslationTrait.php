<?php namespace Stefandebruin\PouleSystem\Models;
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 26-10-17
 * Time: 17:15
 */

trait TranslationTrait
{
    /**
     * Get all of the translations
     */
    public function translations()
    {
        return $this->morphMany('Stefandebruin\PouleSystem\Models\Translation', 'itemable');
    }

    public function getTranslateAttribute()
    {
        $translations = $this->translations->pluck('translation', 'language')->toArray();
        $default = ["nl" => "", "en" => ""];
        return array_merge($default, $translations);
    }
}
