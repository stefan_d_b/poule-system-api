<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 14/10/2018
 * Time: 13:48
 */

namespace Stefandebruin\PouleSystem\Requests;


use Stefandebruin\JsonApi\Requests\JsonApiRequest;

class CompetitionRequest extends JsonApiRequest
{
    public function attributeRules()
    {
        return [
            'title' => [],
            'visible' => ['boolean'],
            'key' => ['alpha'],
            'year' => ['date_format:Y'],
            'type' => []
        ];
    }
}
