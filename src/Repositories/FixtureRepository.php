<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Repositories\Api\FixtureRepositoryInterface;

class FixtureRepository implements FixtureRepositoryInterface
{
    public function model(): Fixture{
        return new Fixture();
    }

    public function save(Fixture $competition):Fixture{
        $competition->save();
        return $competition;
    }

    public function find(int $id)
    {
        return $this->model()->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findByExternalId($id)
    {
        $model = $this->model();
        $model = $model->where('api_id', $id);
        $fixture = $model->get()->first();
        if (is_null($fixture)) {
            throw new ModelNotFoundException();
        }
        return $fixture;
    }
}