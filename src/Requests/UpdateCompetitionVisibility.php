<?php

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;


class UpdateCompetitionVisibility extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'competitions' => ['required', 'exists:competitions,id'],
            'visible' => ['required', 'boolean']
        ];
    }
}
