<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CompetitionVisibleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'competitions' => ['required', 'array', 'exists:competitions,id'],
            'visible' => ['required', 'boolean'],
            'translate' => ['required'],
        ];
    }
}