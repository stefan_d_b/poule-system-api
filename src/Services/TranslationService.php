<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:47
 */

namespace Stefandebruin\PouleSystem\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Stefandebruin\PouleSystem\Models\Translation;

class TranslationService
{

    /**
     * @param Model $model
     * @param Request $request
     * @deprecated verplaatst naar spatie package
     */
    public function store(Model $model, Request $request, $type){
        $translations = $request->translate;
        if(!is_array($translations)){
            $translations = [\Config::get('app.locale') => $translations];
        }

        /** @var Collection $existingTranslations */
        $existingTranslations = ($model->translations);

        foreach($translations as $languageCode => $translation){
            if(!is_numeric($languageCode)){
                if($existingTranslations->where('language', $languageCode)->count() === 0){
                    $translationModel = app()->make(Translation::class);
                }else{
                    $translationModel = $existingTranslations->where('language', $languageCode)->first();
                }
                $translationModel->language = $languageCode;
                $translationModel->translation = $translation;
                $translationModel->itemable_id = $model->id;
                $translationModel->itemable_type = $type;
                $translationModel->save();
            }
        }
    }
}