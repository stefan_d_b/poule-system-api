<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnTableForFixtureSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixture_settings', function (Blueprint $table) {
            $table->integer('competition_id')->unsigned()->index();
            $table->foreign('competition_id')->references('id')->on('competitions')->onDelete('cascade');

            $table->integer('phase_id')->unsigned()->index();
            $table->foreign('phase_id')->references('id')->on('phases')->onDelete('cascade');

            $table->smallInteger('max_groups');
            $table->smallInteger('fixtures_in_group');
            $table->tinyInteger('penalties');
        });

        Schema::table('competition_phase', function (Blueprint $table) {
            $table->dropColumn('max_groups');
            $table->dropColumn('fixtures_in_group');
            $table->dropColumn('penalties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fixture_settings');

        Schema::table('competition_phase', function (Blueprint $table) {
            $table->smallInteger('max_groups');
            $table->smallInteger('fixtures_in_group');
            $table->tinyInteger('penalties');
        });
    }
}
