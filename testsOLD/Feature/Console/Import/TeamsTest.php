<?php

use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Team;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 09-12-17
 * Time: 13:47
 */

class TeamsTest extends \Stefandebruin\PouleSystem\Test\TestCase{
    /**
     * @var \Stefandebruin\PouleSystem\Console\Import\Competitions
     */
    private $_command = null;

    private $_footbalApiClass;
    private $_CompetitionRepositoryClass;
    private $_teamModelMock;
    private $_TeamRepositoryClass;
    private $_competitionModelMock;

    private $_visibleCompetitions = null;
    public function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->_teamModelMock = Mockery::mock(Team::class.'[isDirty]');
        $this->app->instance(Team::class, $this->_teamModelMock);

        $this->_competitionModelMock = Mockery::mock(Competition::class.'[isDirty, teams]');
        $this->app->instance(Competition::class, $this->_competitionModelMock);

        $this->_CompetitionRepositoryClass = Mockery::mock(\Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface::class);
        $this->_TeamRepositoryClass = Mockery::mock(\Stefandebruin\PouleSystem\Repositories\TeamRepository::class);
        $this->_footbalApiClass = Mockery::mock(\Stefandebruin\PouleSystem\Services\Api::class);
        $this->_command = Mockery::mock(
            '\Stefandebruin\PouleSystem\Console\Import\Teams[error, line]',
            [
                $this->_footbalApiClass,
                $this->_CompetitionRepositoryClass,
                $this->_TeamRepositoryClass
            ]
        );
        $this->app['Illuminate\Contracts\Console\Kernel']->registerCommand($this->_command);
        $this->_mockGetAllVisible();
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
    }


    private function _mockGetAllVisible(){
        $this->_visibleCompetitions = collect();

        foreach(factory(Competition::class, 2)->make() as $item){
            $new = app()->make(Competition::class)
                ->fill($item->getAttributes());

            $this->_visibleCompetitions->push($new);
        }

        $this->_CompetitionRepositoryClass->shouldReceive('getAllVisible')->andReturn($this->_visibleCompetitions);
    }

    private function _mockApiTestData(){

    }

    /**
     *
     */
    private function _mockGetCompetitionTeams($exist = false){
        foreach($this->_visibleCompetitions as $competition){
            $teams = [];
            $api_id = 100;
            $id = 76;
            /** @var Team $team */
            $ids = [];
            foreach(factory(Team::class, 5)->make() as $t){
                $team = app()->make(Team::class);
                $team->fill($t->getAttributes());
                $team->setRelation('translations', new Illuminate\Database\Eloquent\Collection());
                $teams[] = [
                    '_links' => [
                        'self' => [
                            'href' => 'http://api.football-data.org/v1/teams/'.$api_id
                        ],
                        'fixtures' => [
                            'href' => 'http://api.football-data.org/v1/teams/'.$api_id.'/fixtures'
                        ],
                        'players' => [
                            'href' => 'http://api.football-data.org/v1/teams/'.$api_id.'/players'
                        ]
                    ],
                    'name' => $team->name,
                    'code' => null,
                    'shortName' => $team->name,
                    'squadMarketValue' => null,
                    'crestUrl' => null,
                ];

                $getByIdResponse = app()->make(Team::class);
                $getByIdResponse->setRelation('translations', new Illuminate\Database\Eloquent\Collection());
                if($exist){
                    $getByIdResponse = clone $team;
                    $getByIdResponse->api_id = $api_id;
                    $getByIdResponse->name = $getByIdResponse->name . '' . rand();
                }
                $this->_TeamRepositoryClass
                    ->shouldReceive('getByApiId')
                    ->once()
                    ->with($api_id)
                    ->andReturn($getByIdResponse);

                $saveResponse = clone $team;
                $saveResponse->id = $id;

                $saveMock = clone $team;
                $saveMock->api_id = $api_id;
                $this->_TeamRepositoryClass->shouldReceive('save')
                    ->with(\Mockery::on(function ($argument) use ($saveMock){
                        return $argument->getAttributes() == $saveMock->getAttributes();
                    }))
                    ->andReturn($saveResponse);

                $api_id += 10;
                $ids[] = $id;

                $id++;
            }

            $apiData = [
                '_links' => [
                    'self' => [
                        'href' => 'http://api.football-data.org/v1/competitions/'.$competition->api_id.'/teams'
                    ],
                    'competition' => [
                        'href' => 'http://api.football-data.org/v1/competitions/'.$competition->api_id
                    ]
                ],
                'count' => 5,
                'teams' => $teams
            ];

            $this->_footbalApiClass
                ->shouldReceive('getCompetitionTeams')
                ->once()
                ->with($competition->api_id)
                ->andReturn(json_decode(json_encode($apiData)));

            $BelongsToMany = Mockery::mock(
                Stefandebruin\PouleSystem\Test\BelongsToManyStub::class.'[sync]'
            );

            $BelongsToMany->shouldReceive('sync')
                ->with($ids)
                ->once()
                ->andReturn($ids);

            $competition->shouldReceive('teams')
                ->once()
                ->andReturn($BelongsToMany);
        }

    }

    public function testException(){
        $this->_footbalApiClass->shouldReceive('getCompetitionTeams')->times(2)->andThrow(new Exception());

        $this->_command->shouldReceive('error')->times(2)->with($this->_visibleCompetitions->first()->toArray()['title'] . ': Can not connect to the api');

        $this->artisan('poulesystem:import:Teams', ['--no-interaction' => true]);
    }

    public function testClientExceptionOtherStatusCode(){
        $exception = Mockery::mock(\GuzzleHttp\Exception\ClientException::class);
        $this->_footbalApiClass->shouldReceive('getCompetitionTeams')->times(2)->andThrow($exception);
        $this->_command->shouldReceive('error')->times(2)->with($this->_visibleCompetitions->first()->toArray()['title']. ': The result status is not 200');

        $this->artisan('poulesystem:import:Teams', ['--no-interaction' => true]);
    }

    public function testAddNewTeams(){
        $this->_mockGetCompetitionTeams();
//        $this->_command->shouldReceive('line')->once()->with('There are 2 competitions added');

        $this->artisan('poulesystem:import:Teams', ['--no-interaction' => true]);
    }

    public function testUpdateCompetitions(){
        $this->_mockGetCompetitionTeams(true);
//        $this->_command->shouldReceive('line')->once()->with('There are 2 competitions added');

        $this->artisan('poulesystem:import:Teams', ['--no-interaction' => true]);
    }
}