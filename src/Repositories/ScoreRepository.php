<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use Auth;
use DB;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Prediction;
use Stefandebruin\PouleSystem\Models\Score;
use Stefandebruin\PouleSystem\Models\Team;
use Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\ScoreRepositoryInterface;

class ScoreRepository implements ScoreRepositoryInterface
{
    public function model($data = []):Score{
        return new Prediction($data);
    }

    public function save(Score $competition): Score{
        $competition->save();
        return $competition;
    }

    /**
     * @param $apiId
     * @return Score
     */
    public function getByApiId($apiId):Score{
        $model = $this->model();
        $model = $model->where('api_id', $apiId);
        $team = $model->get()->first();
        if(is_null($team)) $team = $this->model();
        return $team;
    }

    public function find(int $id)
    {
        $model = $this->model();
        $model = $model->where('fixture_id', $id)->where('user_id', Auth::id());

        $team = $model->get()->first();
        if(is_null($team)) $team = $this->model();
        return $team;
    }

    public function findBy(String $column, $value, $user = true)
    {
        $model = $this->model();
        if($user){
            $model = $model->where($column, $value)->whereNotNull('user_id');
        }else{
            $model = $model->where($column, $value);
        }
        return $model->get();
    }

    public function getForUser($userId = null)
    {
        if(is_null($userId)) $userId = Auth::id();
        $model = $this->model();
        $model = $model->where('user_id', $userId);

        $team = $model->get();
        return $team;
    }

    public function getAllUsers($competitionId)
    {
        return DB::table($this->model()->getTable())
            ->select('user_id')
            ->distinct()
            ->get();
    }
}