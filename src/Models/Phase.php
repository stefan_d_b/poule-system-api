<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Stefandebruin\JsonApi\JsonApi;
use Stefandebruin\JsonApi\Models\JsonModel;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;

/**
 * Stefandebruin\PouleSystem\Models\Phase
 *
 * @method LengthAwarePaginator jsonPaginate
 * @property string|null $key
 * @property array $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Competition[] $competitions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Fixture[] $fixtures
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Group[] $groups
 * @method static \Illuminate\Database\Eloquent\Builder|Phase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phase whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phase whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phase whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id
 */
class Phase extends JsonModel
{
    use HasTranslations, JsonApi;

    public $translatable = ['title'];

    protected $fillable = [
        'key',
        'title',
    ];

    protected $hidden = [
        'pivot',
    ];

    protected $appends = [
        'first_fixture',
        'last_fixture',
    ];

    protected $dates = [
        'first_fixture',
        'last_fixture',
    ];

    protected $hiddenRelations = [
        'getFirstFixtureAttribute',
        'getLastFixtureAttribute',
    ];

    protected $with = ['competitions'];

    /**
     * Phase constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (request()->has('with')) {
            $with = explode(",", request()->get('with', []));
            foreach ($with as $item) {
                if (in_array($item, $this->withData())) {
                    $this->with[] = $item;
                }
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function competitions()
    {
        return $this->belongsToMany(
            'Stefandebruin\PouleSystem\Models\Competition'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @throws \Exception
     */
    public function fixtures()
    {
        $relation = $this->hasMany(
            'Stefandebruin\PouleSystem\Models\Fixture',
            'phase_id'
        );

        if (app()->bound('competitionId')) {
            $relation->where('competition_id', PouleSystemServiceProvider::boundCompetitionId());
        }
        return $relation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @throws \Exception
     */
    public function groups()
    {
        $objectId = null;
        if (!empty($this->pivot) && !empty($this->pivot->competition_id)) {
            $objectId = $this->pivot->competition_id;
        }

        $relation = $this->belongsToMany(
            'Stefandebruin\PouleSystem\Models\Group',
            'competition_phase_group'
        )->withPivot('competition_id', 'order_by')
            ->orderBy('order_by');

        if (app()->bound('competitionId')) {
            $relation->where('competition_id', PouleSystemServiceProvider::boundCompetitionId());
        }

        return $relation;
    }

    /**
     * @return array
     */
    public static function getUrlIncludeRelations()
    {
        return ['fixtures', 'groups', 'teams', 'result'];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('poulesystem.phases.show', ['phase' => $this->id]);
    }

    public function getFirstFixtureAttribute()
    {
//        if($this->id == 11){
//            dd($this, $this->groups()->with(['fixtures'])->get());
//        }
        $found = $this->fixtures()->select(['id', 'date'])->orderBy('date')->limit(1)->first();

        if ($found instanceof Fixture) {
            return $found->date;
        }

        return null;
//        return \Carbon\Carbon::now();
    }

    public function getLastFixtureAttribute()
    {
        $found = $this->fixtures()->select(['id', 'date'])->orderBy('date', 'desc')->limit(1)->first();

        if ($found instanceof Fixture) {
            return $found->date;
        }
        return null;
    }
}
