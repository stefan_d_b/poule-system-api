<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OldResult extends Resource
{
    use RelationTrait;

    /**
     * @var array
     */
    protected $primaryRelations = [
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->links = [
            'competition' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'fixture' => route('poulesystem.competitions.teams', ['competition' => $this->resource->id]),
            'groups' => route('poulesystem.competitions.groups', ['competition' => $this->resource->id]),
            'fixtures' => route('poulesystem.competitions.fixtures', ['competition' => $this->resource->id]),
        ];
        $this->dataRoot = $dataRoot;
    }

    public function toArray($request)
    {
        $relations = $this->getRelationships($request);
        return [
            'type' => 'result',
            'id' => (string) $this->resource->id,
            'attributes' => [
                'home_goals' => $this->resource->home_goals,
                'away_goals' => $this->resource->away_goals,
                'halftime_home_goals' => $this->resource->halftime_home_goals,
                'halftime_away_goals' => $this->resource->halftime_away_goals,
                'extratime_home_goals' => $this->resource->extratime_home_goals,
                'extratime_away_goals' => $this->resource->extratime_away_goals,
                'penalties_home' => $this->resource->penalties_home,
                'penalties_away' => $this->resource->penalties_away,
            ],
            'links' => [
//                'self' => route('poulesystem.team.show', ['team' => $this->resource->id])
            ],
            'relationship' => $this->when(count($relations) > 0, $relations)
        ];
    }
}
