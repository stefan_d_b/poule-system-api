<?php namespace Stefandebruin\PouleSystem\Listeners;

use App\User;
use Illuminate\Auth\Events\Login;
use Stefandebruin\PouleSystem\Models\Domain;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 06-11-17
 * Time: 20:22
 */
class UserLoggedIn
{
    /**
     * @param Login $login
     * @throws \Exception
     */
    public function handle(Login $login)
    {
        if (!app()->bound('pouleDomain')) {
            throw new \Exception("Domain not set");
        }

        /** @var Domain $domain */
        $domain = app()->make('pouleDomain');

        /** @var User $user */
        $user = $login->user;
        $check = $domain->users->where('id', $user->id)->first();
        if (!$check instanceof User) {
            $domain->users()->attach($user);
        }
    }
}
