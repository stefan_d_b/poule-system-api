<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\PouleSystem\Models\Competition as CompetitionModel;

class OldCompetition123 extends Resource
{
    use RelationTrait;

    /**
     * @var array
     */
    protected $primaryRelations = [
        'phases',
        'teams'
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->links = [
            'phases' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'teams' => route('poulesystem.competitions.teams', ['competition' => $this->resource->id]),
            'groups' => route('poulesystem.competitions.groups', ['competition' => $this->resource->id]),
            'fixtures' => route('poulesystem.competitions.fixtures', ['competition' => $this->resource->id]),
        ];
        $this->dataRoot = $dataRoot;
    }

    public function toArray($request){
        $relations = $this->getRelationships($request);

        return [
            'type' => 'competitions',
            'id' => (string) $this->resource->id,
            'attributes' => [
                'title' => $this->when($this->addField($request, 'title'), $this->resource->title),
                'key' => $this->when($this->addField($request, 'key'), $this->resource->key),
                'year' => $this->when($this->addField($request, 'year'), $this->resource->year),
                'visible' => $this->when($this->addField($request, 'visible'), $this->resource->visible),
                'type' => $this->when($this->addField($request, 'type'), $this->resource->type),
                'typeLabel' => $this->when($this->addField($request, 'typeLabel'), $this->resource->typeLabel),
                'created_at' => $this->when($this->addField($request, 'typeLabel'), $this->resource->created_at->toIso8601String()),
                'updated_at' => $this->when($this->addField($request, 'typeLabel'), $this->resource->updated_at->toIso8601String()),
            ],
            'links' => [
                'self' => route('poulesystem.competitions.show', ['competition' => $this->resource->id])
            ],
//            'relationships' => $this->when(count($relations) > 0, $relations)
        ];
    }

    public function addField($request, $field)
    {
        $model = strtolower(class_basename(get_class($this->resource)));
        if ($request->has('fields')) {
            $fields = $request->get('fields');
            if (!array_has($fields, $model)) return true;

            $allFields = explode(",", $fields[$model]);

            return in_array($field, $allFields);
        }
        return true;
    }
}
