<?php

namespace Stefandebruin\PouleSystem\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Stefandebruin\PouleSystem\Models\Competition;

class PodiumStatusUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Competition
     */
    public $competition;

    /**
     * @var boolean
     */
    public $virtual;

    /**
     * Create a new event instance.
     *
     * @param Competition $competition
     * @param boolean $virtual
     * @internal param $updatedRows
     * @internal param $updates
     */
    public function __construct(Competition $competition, bool $virtual)
    {
        $this->competition = $competition;
        $this->virtual = $virtual;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('podium.'.$this->competition->id);
    }
}
