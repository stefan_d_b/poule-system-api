<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\PouleSystem\Models\Competition as CompetitionModel;

class OldSetting extends Resource
{
    use RelationTrait;

    /**
     * @var array
     */
    protected $primaryRelations = [
        'phases',
        'teams'
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->links = [
            'phases' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'teams' => route('poulesystem.competitions.teams', ['competition' => $this->resource->id]),
            'groups' => route('poulesystem.competitions.groups', ['competition' => $this->resource->id]),
            'fixtures' => route('poulesystem.competitions.fixtures', ['competition' => $this->resource->id]),
        ];
        $this->dataRoot = $dataRoot;
    }

    public function toArray($request){
        $relations = $this->getRelationships($request);

        return [
            'type' => 'settings',
            'id' => (string) $this->resource->fixture_id,
            'attributes' => $this->resource->toArray(),
            'links' => [
                'self' => ''
            ],
            'relationships' => $this->when(count($relations) > 0, $relations)
        ];
    }
}
