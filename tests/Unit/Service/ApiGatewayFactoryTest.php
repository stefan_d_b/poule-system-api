<?php

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Stefandebruin\PouleSystem\Exceptions\GatewayException;
use Stefandebruin\PouleSystem\Services\ApiGateway\ApiGatewayInterface;
use Stefandebruin\PouleSystem\Services\ApiGatewayFactory;
use Stefandebruin\PouleSystem\Test\TestCase;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-19
 * Time: 07:40
 */
class ApiGatewayFactoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        Config::set('poulesystem.defaultProvider', 'football-data');
        Config::set(
            'poulesystem.gateways',
            [
                'football-data' => \Stefandebruin\PouleSystem\Services\ApiGateway\FootballData::class,
                'live-scores' => \Stefandebruin\PouleSystem\Test\Data\Service\ApiGateway\LiveScores::class,
                'incorrect' => \Stefandebruin\PouleSystem\Test\Data\Service\ApiGateway\Incorrect::class,
            ]
        );
    }

    public function testMakeDefaultGateway()
    {
        $gateway = ApiGatewayFactory::make();
        $this->assertInstanceOf(ApiGatewayInterface::class, $gateway);
        $this->assertContains(get_class($gateway), config('poulesystem.gateways'));
        $key = array_search(get_class($gateway), config('poulesystem.gateways'));
        $this->assertEquals('football-data', $key);
    }

    public function testMakeOtherGateway()
    {
        $gateway = ApiGatewayFactory::make('live-scores');
        $this->assertInstanceOf(ApiGatewayInterface::class, $gateway);
        $this->assertContains(get_class($gateway), config('poulesystem.gateways'));
        $key = array_search(get_class($gateway), config('poulesystem.gateways'));
        $this->assertEquals('live-scores', $key);
    }

    /**
     * @dataProvider incorrectGatewaysDataProvider
     */
    public function testMakeIncorrectGateway($gateway, $errorCode)
    {
        $this->expectException(GatewayException::class);
        $this->expectExceptionCode($errorCode);

        $gateway = ApiGatewayFactory::make($gateway);
    }

    public function testAllGateways()
    {
        $values = Config::get('poulesystem.gateways');
        unset($values['incorrect']);
        Config::set('poulesystem.gateways', $values);

        $gateways = ApiGatewayFactory::all();
        $this->assertInstanceOf(Collection::class, $gateways);
        $this->assertCount(2, $gateways);
        $this->assertEquals(['football-data', 'live-scores'], array_keys($gateways->toArray()));
    }

    public function testAllGatewaysIgnoreIncorrect()
    {
        $gateways = ApiGatewayFactory::all();
        $this->assertInstanceOf(Collection::class, $gateways);
        $this->assertCount(2, $gateways);
        $this->assertEquals(['football-data', 'live-scores'], array_keys($gateways->toArray()));
    }

    public function incorrectGatewaysDataProvider()
    {
        return [
            ['not-exists', 100,],
            ['incorrect', 110,],
        ];
    }
}