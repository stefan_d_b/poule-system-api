<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:42
 */

namespace Stefandebruin\PouleSystem\Console\Testing;

use App\User;
use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Prediction;
use Stefandebruin\PouleSystem\Models\Score;
use Stefandebruin\PouleSystem\Repositories\CompetitionRepository;
use Stefandebruin\PouleSystem\Repositories\TeamRepository;
use Stefandebruin\PouleSystem\Services\Api;

class Users extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poulesystem:testing:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the team';

    /**
     * @var Api
     */
    protected $Api = null;

    /**
     * @var TeamRepository
     */
    protected $teamRepository = null;

    /**
     * @var CompetitionRepository
     */
    protected $competitionRepository = null;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = factory(User::class, 20)->create();
//        $users = DB::table('users')
//            ->leftJoin('usertokens', 'usertokens.id', '=', 'users.id')
//            ->whereNull('token')
//            ->select('users.*')
//            ->get()->random(5);

        /** @var Collection $competitions */
        $competitions = Competition::where('visible', 1)->get();

        /** @var Competition $competition */

        $index = 1;
        /** @var User $user */
        foreach($users as $user){
            echo $index . ": " . $user->name . "\n";
            $index++;
            foreach($competitions->random(2) as $competition){
                echo "--" . $competition->title . "\n";
                /** @var Fixture $fixture */
                foreach ($competition->fixtures as $fixture){
                    $score = factory(Prediction::class)->create([
                        'user_id' => $user->id,
                        'fixture_id' => $fixture->id,
                    ]);
                }
            }
        }
    }
}