<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 28/10/2018
 * Time: 10:48
 */

class CompetitionControllerTest extends \Stefandebruin\PouleSystem\Test\TestCase
{
    public function testClassExists()
    {
        $class = app()->make('\Stefandebruin\PouleSystem\Controllers\CompetitionController');
        $this->assertInstanceOf('\Stefandebruin\PouleSystem\Controllers\CompetitionController', $class);
    }

    public function testIndex()
    {
        $response = $this->json('GET', 'competitions');
    }

    /**
     * datasource
     */
    public function testCreateCompetition()
    {

    }

    public function testShowCompetition()
    {

    }

    public function testUpdateCompetition()
    {

    }

    public function testDeleteCompetition()
    {
        
    }
}