<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories;

use App\User;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Team;
use Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\UserRepositoryInterface;

/**
 * Class UserRepository
 * @package Stefandebruin\PouleSystem\Repositories
 * @TODO-stefan remove this class
 * @deprecated
 */
class UserRepository implements UserRepositoryInterface
{
//    public function model(){
//        return new Team();
//    }
//
//    public function save(Team $competition){
//        $competition->save();
//        return $competition;
//    }
//
//    /**
//     * @param $apiId
//     * @return Competition
//     */
//    public function getByApiId($apiId){
//        $model = $this->model();
//        $model = $model->where('api_id', $apiId);
//        $team = $model->get()->first();
//        if(is_null($team)) $team = $this->model();
//        return $team;
//    }
    public function model(): User
    {
        return new User();
    }

    public function save(User $user): User
    {
        $user->save();
        return $user;
    }

    public function find(int $id)
    {
        return $this->model()->find($id);
    }

    public function findBy(String $column, $value)
    {
        // TODO: Implement findBy() method.
    }
}