<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 03-11-17
 * Time: 08:06
 */

use Illuminate\Database\Eloquent\Model;

/**
 * Stefandebruin\PouleSystem\Models\FixtureSetting
 *
 * @property string domain
 * @property bool enabled
 * @property int $competition_id
 * @property int $phase_id
 * @property int $order_by
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureSetting whereCompetitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureSetting whereOrderBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixtureSetting wherePhaseId($value)
 * @mixin \Eloquent
 */
class FixtureSetting extends Model
{
    use PouleModelTrait;

    protected $table = 'competition_phase';

    protected $primaryKey = 'fixture_id';

    protected $hidden = [
        'competition_id',
        'phase_id',
        'max_groups',
        'fixtures_in_group',
        'order_by',
        'fixture_id'
    ];
}
