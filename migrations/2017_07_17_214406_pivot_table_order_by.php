<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotTableOrderBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_phase', function (Blueprint $table) {
            $table->integer('order_by');
        });

        Schema::table('competition_phase_group', function (Blueprint $table) {
            $table->integer('order_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_phase', function (Blueprint $table) {
            $table->dropColumn('order_by');
        });

        Schema::table('competition_phase_group', function (Blueprint $table) {
            $table->dropColumn('order_by');
        });
    }
}
