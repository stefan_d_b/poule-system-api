<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\PouleSystem\Models\Prediction;
use Stefandebruin\PouleSystem\Repositories\Api\UserRepositoryInterface;
use Stefandebruin\PouleSystem\Requests\PredictionBulkRequest;
use Stefandebruin\PouleSystem\Requests\PredictionRequest;
use Stefandebruin\PouleSystem\Services\PredictionService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PredictionController extends Controller
{
    /**
     * @var PredictionService
     */
    private $predictionService;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * PredictionController constructor.
     * @param PredictionService $predictionService
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        PredictionService $predictionService,
        UserRepositoryInterface $userRepository
    ) {
        $this->predictionService = $predictionService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return AbstractJsonApiCollection
     */
    public function index(Request $request)
    {
        $userId = auth()->id();

        if ($request->has('user_id')) {
            $userId = $request->get('user_id');
            $user = $this->userRepository->find($userId);
            if (is_null($user)) {
                throw new BadRequestHttpException("User does not exist");
            }
        }

        $model = app(Prediction::class);

        return new AbstractJsonApiCollection(
            app()->make(FilterCollectionService::class)->filter($model)->jsonPaginate()
        );
    }

    public function show(Prediction $prediction)
    {
        return new \Stefandebruin\PouleSystem\Resources\Prediction($prediction);
    }

    public function store(Request $request)
    {
        $result = $this->predictionService->createByFixtureId(
            \Auth::id(),
            $request->input('data.relationships.fixture.data.id'),
            $request->input('data.attributes')
        );

        return new \Stefandebruin\PouleSystem\Resources\Prediction($result);
    }

    public function storeBulk(PredictionBulkRequest $request)
    {
        $collection = $this->predictionService->savePredictions(
            $request->input('data'),
            \Auth::id()
        );

        return (new AbstractJsonApiCollection($collection))
            ->response()
            ->setStatusCode(200);
    }

    public function update(PredictionRequest $request, Prediction $prediction)
    {
        $attributes = array_except($request->allAttributes(), 'fixture_id');
        $prediction = $this->predictionService->fillAttributes($prediction, $attributes);
        $prediction->save();
        return (new \Stefandebruin\PouleSystem\Resources\Prediction($prediction))
            ->response()
            ->setStatusCode(200);
    }
}
