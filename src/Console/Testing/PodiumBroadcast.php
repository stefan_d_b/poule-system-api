<?php namespace Stefandebruin\PouleSystem\Console\Testing;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 05-01-18
 * Time: 12:28
 */

use App\Events\ServerCreated;
use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Events\PodiumUpdate;
use Stefandebruin\PouleSystem\Models\Competition;
use Event;
use Stefandebruin\PouleSystem\Resources\PodiumRow;


class PodiumBroadcast extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poulesystem:testing:podiumBroadcast';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        echo 1234;

        $competition = Competition::find(6);

        $changes = [
            [
                'rowId' => sha1(671),
                'positionChangeDirection' => 'up',
                'positionChange' => 1,
                'userId' => "71",
                'points' => 140,
//                'user' => $this->users->get(1)->toArray(),
                'pointsChange' => 11,
                'newPosition' => 1,
            ],
            [
                'rowId' => sha1(665),
                'positionChangeDirection' => 'down',
                'positionChange' => 1,
                'userId' => "65",
                'points' => 137,
//                'user' => $this->users->get(3)->toArray(),
                'pointsChange' => 2,
                'newPosition' => 2
            ],
            [
                'rowId' => sha1(672),
                'positionChangeDirection' => 'same',
                'positionChange' => 0,
                'userId' => "72",
                'points' => 52,
//                'user' => $this->users->get(0)->toArray(),
                'pointsChange' => 3,
                'newPosition' => 20
            ]
        ];

        $data[] = [
            'type' => 'podiumRows',
            'id' => (string) sha1('62'),
            'attributes' => ['position' => 3, 'positionChange' => -2, 'points' => 3, 'pointsChange' => 3], //
        ];
        $data[] = [
            'type' => 'podiumRows',
            'id' => (string) sha1('65'),
            'attributes' => ['position' => 1, 'positionChange' => 1, 'points' => 6, 'pointsChange' => 6], //
        ];
        $data[] = [
            'type' => 'podiumRows',
            'id' => (string) sha1('650'),
            'attributes' => ['position' => 2, 'positionChange' => 2, 'points' => 4, 'pointsChange' => 4], //
        ];
//        dd($data);
        $a= Event::dispatch(new PodiumUpdate($competition, $data, true));
//        var_dump($a);
    }
}