<?php

namespace Stefandebruin\PouleSystem\Resources;

use Stefandebruin\JsonApi\Exceptions\UnknownModelType;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;

class Prediction extends AbstractJsonApi
{
    protected function getAttributes($request, $type)
    {
        $attributes = parent::getAttributes($request, $type);
        $fixture = $this->resource->fixture;
        $attributes['penalty_winner'] = $this->resource->penalties_home === 5 ? $fixture->home_team : $fixture->away_team;
        unset($attributes['penalties_away']);
        unset($attributes['penalties_home']);
        return $attributes;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws UnknownModelType
     */
    public function toArraybllk($request)
    {
        $fixture = $this->resource->fixture;

        $type = array_search(get_class($this->resource), config('json-api.types'));
        if(empty($type)){
            throw new UnknownModelType('Unknown type for model: ' . get_class($this->resource));
        }

        $links = [
            'self' => $this->resource->getUrl()
        ];

        foreach($this->getIncludeRelations($request) as $relation){
            $links[$relation] = $this->resource->getUrl() . '/' . $relation;
        }

        return [
            'type' => 'predictions',
            'id' => (string) $this->resource->id,
            'attributes' => [
                'home_goals' => $this->resource->home_goals,
                'away_goals' => $this->resource->away_goals,
                'halftime_home_goals' => $this->resource->halftime_home_goals,
                'halftime_away_goals' => $this->resource->halftime_away_goals,
                'extratime_home_goals' => $this->resource->extratime_home_goals,
                'extratime_away_goals' => $this->resource->extratime_away_goals,
                'penalty_winner' => $this->resource->penalties_home === 5 ? $fixture->home_team : $fixture->away_team,
                'fixture_id' => $this->resource->fixture_id,
            ],
            'links' => $links,
            'relationships' => $this->when(count($this->getRelationships($request, $type)) > 0, $this->getRelationships($request, $type))
        ];
    }
}
