<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:42
 */

namespace Stefandebruin\PouleSystem\Console\Testing;

use App\User;
use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stefandebruin\PouleSystem\Console\OfficialResultTrait;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Score;
use Stefandebruin\PouleSystem\Repositories\Api\FixtureRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\ScoreRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\CompetitionRepository;
use Stefandebruin\PouleSystem\Repositories\TeamRepository;
use Stefandebruin\PouleSystem\Services\Api;
use Stefandebruin\PouleSystem\Services\PodiumService;
use Symfony\Component\Console\Input\InputOption;

class OfficialResult extends Command
{
    use OfficialResultTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poulesystem:testing:officialResult {--competitionId=} {--input}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the team';

    /**
     * @var Api
     */
    protected $Api = null;

    /**
     * @var TeamRepository
     */
    protected $teamRepository = null;

    /**
     * @var CompetitionRepository
     */
    protected $competitionRepository = null;

    /**
     * @var FixtureRepositoryInterface
     */
    protected $fixtureRepository;


    /**
     * @var ScoreRepositoryInterface
     */
    private $scoreRepository;

    /**
     * @var PodiumService
     */
    private $podiumService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->Api = app()->make(Api::class);

        $this->competitionRepository = app()->make(CompetitionRepository::class);
        $this->teamRepository = app()->make(TeamRepository::class);
        $this->fixtureRepository = app()->make(FixtureRepositoryInterface::class);
        $this->scoreRepository = app()->make(ScoreRepositoryInterface::class);
        $this->podiumService = app()->make(PodiumService::class);
    }

    public function selectCompetitions(){
        $this->call('cache:clear');

        if(is_null($this->option('competitionId'))){
            $this->error("competitionId is missing");
            return false;
        }

        /** @var Competition $competition */
        $competition = Competition::find($this->option('competitionId'));
        if(!$competition instanceof Competition){
            $this->error("selected competition does not exist");
            return false;
        }

        if(!$competition->visible){
            $this->error("selected competitionId is not visible");
            return false;
        }

        $collection = new Collection();
        $collection->push($competition);
        return $collection;
    }

    /**
     * @param Competition $competition
     */
    public function importResults(Competition $competition){
        $fixturesSetResult = 3;
        /** @var Fixture $fixture */
        foreach($competition->fixtures as $fixture){
            if(is_null($fixture->result) && $fixturesSetResult > 0){
                $fixture->status = Fixture::STATUS_IN_PLAY;
                $fixture->save();
                /** @var Score $score */
                $score = factory(Score::class)->make([
                    'user_id' => null,
                    'fixture_id' => $fixture->id,
                ]);

                if($this->option('input')){

                    $this->info("Fixture " . $fixture->id . ' | ' . $fixture->homeTeam->name . ' - ' . $fixture->awayTeam->name);
                    $homeScore = $this->ask("Home score");
                    if(is_numeric($homeScore)){
                        $score->home_goals = $homeScore;
                    }

                    $awayScore = $this->ask("Away score");
                    if(is_numeric($awayScore)){
                        $score->away_goals = $awayScore;
                    }
                }
                $score->save();
                $response = $this->podiumService->storeVirtualPodiumScores($fixture, $score);
                $fixturesSetResult--;
            }else if(!is_null($fixture->result) && $fixture->status == Fixture::STATUS_IN_PLAY){
                $ask = $this->confirm("Fixture finished", false);
                if($ask){
                    $fixture->status = Fixture::STATUS_FINISHED;
                }

                $response = $this->podiumService->storeVirtualPodiumScores($fixture, $fixture->result);
                $fixture->save();
            }
        }
    }
}