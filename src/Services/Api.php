<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:47
 */

namespace Stefandebruin\PouleSystem\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class Api
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getCompetitions()
    {

    }





//    //COMPETITION/LEAGUE
//    public function getCompetitions($year="")
//    {
//    	return $this->doRequest("competitions/?season={$year}");
//    }

    public function getCompetitionTeams($objectId)
    {
        return $this->doRequest("competitions/{$objectId}/teams");
    }

    public function getLeagueFixtures($objectId)
    {
        return $this->doRequest("competitions/{$objectId}/fixtures");
    }

    public function getLeagueFixturesTest($objectId)
    {
        if (!Storage::disk('local')->has('fixtures-' . $objectId . '.json')) {
            $leagueFixtures = $this->client->get("competitions/{$objectId}/fixtures")->getBody();
            Storage::disk('local')->put('fixtures-' . $objectId . '.json', $leagueFixtures);
        } else {
            $leagueFixtures = Storage::get('fixtures-' . $objectId . '.json');
        }

        return json_decode($leagueFixtures);
    }

    private function doRequest($path)
    {
        try {
            $response = $this->client->get($path)->getBody();
            return json_decode($response);
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}