<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 07/11/2018
 * Time: 12:59
 */

namespace Stefandebruin\PouleSystem;


class OverrideModels
{
    private $userModel;

    /**
     * @return mixed
     */
    public function getUserModel()
    {
        return $this->userModel;
    }

    /**
     * @param mixed $userModel
     */
    public function setUserModel($userModel)
    {
        $this->userModel = $userModel;
    }
}