<?php namespace Stefandebruin\PouleSystem\Facades;
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 07/11/2018
 * Time: 12:57
 */
use Illuminate\Support\Facades\Facade;

class OverrideModels extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'poulesystem-override-models';
    }
}
