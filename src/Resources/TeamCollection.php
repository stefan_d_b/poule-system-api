<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OldTeamCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => ['self' => $request->url()],
        ];
    }
}
