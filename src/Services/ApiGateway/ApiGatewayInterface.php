<?php namespace Stefandebruin\PouleSystem\Services\ApiGateway;

use Stefandebruin\PouleSystem\Models\Competition;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-16
 * Time: 14:37
 */

interface ApiGatewayInterface
{
    /**
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection|mixed
     */
    public function getLeagues($output): \Illuminate\Support\Collection;

    /**
     * @param Competition $competition
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection
     */
    public function getTeams(Competition $competition, $output): \Illuminate\Support\Collection;

    /**
     * @param Competition $competition
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection
     */
    public function getFixtures(Competition $competition, $output): \Illuminate\Support\Collection;

    /**
     * @param Competition $competition
     * @param $output
     * @return \Illuminate\Support\Collection
     */
    public function getScores(Competition $competition, $output): \Illuminate\Support\Collection;
}