<?php

namespace Stefandebruin\PouleSystem\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Stefandebruin\PouleSystem\Models\Competition;

class CompetitionUpdated implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels, Dispatchable;
    /**
     * @var Competition
     */
    public $competition;
    /**
     * @var
     */
    public $update;

    /**
     * Create a new event instance.
     *
     * @param Competition $competition
     * @param $update
     */
    public function __construct(Competition $competition, $update)
    {
        $this->competition = $competition;
        $this->update = $update;
    }
//
//    public function broadcastAs()
//    {
//        return 'competition-updated';
//    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('competition.'.$this->competition->id);
    }
}
