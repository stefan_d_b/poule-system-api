<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;

class FixtureController extends Controller
{
    public function index(){
        $model = app(Fixture::class);

        return new AbstractJsonApiCollection(
            app()->make(FilterCollectionService::class)->filter($model)->jsonPaginate()
        );
    }
    public function competition(Competition $competition)
    {
        return new AbstractJsonApiCollection($competition->fixtures()->jsonPaginate());
    }

    public function show(Fixture $fixture)
    {
        return new AbstractJsonApi($fixture);
    }

    public function groupsRelationList(Group $group)
    {
        return new AbstractJsonApiCollection($group->fixtures()->jsonPaginate());
    }

    public function phaseRelationList(Phase $phase)
    {
        return new AbstractJsonApiCollection($phase->fixtures()->jsonPaginate());
    }
}
