<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories\Api;


use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;

interface PhaseRepositoryInterface
{
    public function model(): Phase;

    public function save(Phase $competition):Phase;

    public function find(int $id);

    public function all();
}