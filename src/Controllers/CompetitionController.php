<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stefandebruin\JsonApi\Controllers\JsonController;
use Stefandebruin\JsonApi\Requests\JsonApiRelationRequest;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Resources\JsonRelationShipCollection;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;
use Stefandebruin\PouleSystem\Requests\CompetitionRequest;
use Stefandebruin\PouleSystem\Requests\ConnectPhasesRequest;
use Stefandebruin\PouleSystem\Services\CompetitionService;

class CompetitionController extends JsonController
{
    /**
     * @var CompetitionService
     */
    private $competitionService;
    /**
     * @var FilterCollectionService
     */
    private $filterCollection;

    /**
     * CompetitionController constructor.
     * @param FilterCollectionService $filterCollection
     * @param CompetitionService $competitionService
     */
    public function __construct(
        FilterCollectionService $filterCollection,
        CompetitionService $competitionService
    )
    {
        $this->competitionService = $competitionService;
        $this->filterCollection = $filterCollection;
    }

    /**
     * @return AbstractJsonApiCollection
     * @throws \Exception
     */
    public function index()
    {
        /** @var Competition $collection */
        $model = app(Competition::class);

        return new AbstractJsonApiCollection(
            app()->make(FilterCollectionService::class)->filter($model)->jsonPaginate()
        );
    }

    /**
     * @param CompetitionRequest|\Illuminate\Http\Request $request
     * @return mixed
     */
    public function create(CompetitionRequest $request)
    {
        return parent::createModel($request);
    }

    /**
     * @param Competition $competition
     * @return AbstractJsonApi
     */
    public function show(Competition $competition)
    {
        return new AbstractJsonApi($competition);
    }

    /**
     * @param CompetitionRequest $request
     * @param Competition $competition
     * @return AbstractJsonApi
     */
    public function update(CompetitionRequest $request, Competition $competition)
    {
        \Bouncer::refresh();
        if (!\Bouncer::can('manage-all', $competition)) {
            return abort(403);
        }

        $competition->fill($request->allAttributes());
        $competition->save();
        return new AbstractJsonApi($competition);
    }

    public function destroy(Competition $competition)
    {
        $competition->delete();
        return response('', 204);
    }

    public function teams(Request $request, $competition)
    {
        $pathInfo = $request->getPathInfo();


    }

    public function groups(Competition $competition)
    {
        return new JsonRelationShipCollection($competition->groups);
    }

    public function phases(Competition $competition)
    {
        return new JsonRelationShipCollection($competition->groups);
    }

    public function users(Competition $competition)
    {
        return new JsonRelationShipCollection($competition->users);
    }

    public function fixtures(Competition $competition)
    {
        return new JsonRelationShipCollection($competition->fixtures);
    }


    public function connectPhases(JsonApiRelationRequest $request, Competition $competition)
    {
        $ids = array_flip($request->getRelationIds());
        $relationData = array_map(
            function ($row) {
                return ['order_by' => $row + 1];
            },
            $ids
        );
        $competition->phases()->sync($relationData);
        return response()->json()->setStatusCode(204);
    }

    public function connectUsers(ConnectPhasesRequest $request, Competition $competition)
    {
        $competition->users()->sync($request->getRelationIds());
        return response()->json()->setStatusCode(204);
    }

    /**
     * @param Request $request
     * @param Competition $competition
     * @param Phase $phase
     * @return \Stefandebruin\PouleSystem\Resources\Fixture|\Stefandebruin\PouleSystem\Resources\Phase
     */
    public function officialResult(Request $request, Competition $competition, Phase $phase)
    {
        $phaseCheck = $competition->phases->where('id', $phase->id)->first();
        $settings = $phaseCheck->pivot->toArray();
        unset($settings['competition_id']);
        unset($settings['phase_id']);
        unset($settings['order_by']);
        $collection = collect($settings);
        $phase->setRelation('settings', $collection);

        PouleSystemServiceProvider::setResultRelation(true);
        if ($request->get('filterby', 'normal') == "group") {
            PouleSystemServiceProvider::setFixturesRelation(true);
            PouleSystemServiceProvider::setGroupsRelation(true);
            return new \Stefandebruin\PouleSystem\Resources\Phase($phase);
        } else {
            $collection = $competition->fixtures->where('phase_id', $phase->id);
            return new \Stefandebruin\PouleSystem\Resources\Fixture($collection);
        }
    }

    public function groupsRelationList(Group $group)
    {
        return new AbstractJsonApiCollection($group->competitions()->jsonPaginate());
    }

    public function PhaseRelationList(Phase $phase)
    {
        return new AbstractJsonApiCollection($phase->competitions()->jsonPaginate());
    }
}
