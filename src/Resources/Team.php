<?php

namespace Stefandebruin\PouleSystem\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OldTeam extends Resource
{
    use RelationTrait;

    /**
     * @var array
     */
    protected $primaryRelations = [
        'competition',
        'fixture',
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->links = [
            'competition' => route('poulesystem.competitions.phases', ['competition' => $this->resource->id]),
            'fixture' => route('poulesystem.competitions.teams', ['competition' => $this->resource->id]),
            'groups' => route('poulesystem.competitions.groups', ['competition' => $this->resource->id]),
            'fixtures' => route('poulesystem.competitions.fixtures', ['competition' => $this->resource->id]),
        ];
        $this->dataRoot = $dataRoot;
    }

    public function toArray($request)
    {
        $relations = $this->getRelationships($request);
        return [
            'type' => 'teams',
            'id' => (string) $this->resource->id,
            'attributes' => [
                'code' => $this->resource->code,
                'name' => $this->resource->name,
                'image' => $this->resource->image,
            ],
            'links' => [
                'self' => route('poulesystem.team.show', ['team' => $this->resource->id])
            ],
            'relationship' => $this->when(count($relations) > 0, $relations)
        ];
    }
}
