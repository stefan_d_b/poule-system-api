<?php namespace Stefandebruin\PouleSystem\Services;

use Illuminate\Support\Collection;
use Stefandebruin\PouleSystem\Models\Prediction;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 22-01-18
 * Time: 07:37
 */

class PredictionService
{
    public function hasPrediction($userId, $fixtureId): bool
    {
        $model = app()->make(Prediction::class)
            ->where('user_id', $userId)
            ->where('fixture_id', $fixtureId)
            ->first();
        return $model instanceof Prediction;
    }

    /**
     * @param $data
     * @param $userId
     * @return Collection
     */
    public function savePredictions(array $data, int $userId): Collection
    {
        $result = new Collection();
        foreach ($data as $row) {
            $result->push($this->savePrediction($row['attributes'], $userId, array_get($row, 'id', null)));
        }
        return $result;
    }

    /**
     * @param array $data
     * @param int $userId
     * @param int|null $objectId
     * @return Prediction
     */
    public function savePrediction(array $data, int $userId, int $objectId = null): Prediction
    {
        $model = is_null($objectId) ? $this->model() : $this->find($objectId);
        $model->fill($data);
        $model->user_id = $userId;
        if (array_has($data, 'penalty_winner')) {
            if ($data['penalty_winner'] == $model->fixture->home_team) {
                $model->penalties_home = 5;
                $model->penalties_away = 0;
            } else {
                $model->penalties_home = 0;
                $model->penalties_away = 5;
            }
        }
        $model->save();
        return $model;
    }

    /**
     * @param int $objectId
     * @return Prediction
     */
    public function find(int $objectId): Prediction
    {
        return app()->make(Prediction::class)->find($objectId);
    }

    /**
     * @return Prediction
     */
    public function model(): Prediction
    {
        return app()->make(Prediction::class);
    }

    public function createByFixtureId($userId, $fixtureId, $attributes)
    {
        /** @var Prediction $prediction */
        $prediction = app()->make(Prediction::class);
        $prediction->user_id = $userId;
        $prediction->fixture_id = $fixtureId;
        $prediction = $this->fillAttributes($prediction, $attributes);
        $prediction->fill($attributes);
        $prediction->save();

        \Log::debug($prediction);
        return $prediction;
    }

    public function updateById($objectId, $attributes)
    {
        /** @var Prediction $prediction */
        $prediction = app()->make(Prediction::class)->find($objectId);
        $prediction = $this->fillAttributes($prediction, $attributes);
        $prediction->save();
        return $prediction;
    }

    public function bulkCreate($userId, $attributes)
    {
        $result = collect();

        foreach ($attributes as $row) {
            $scores = $row['attributes'];
            unset($scores['fixture_id']);
            $result->push($this->createByFixtureId($userId, $row['attributes']['fixture_id'], $scores));
        }
        return $result;
    }

    public function bulkUpdate($userId, $items)
    {
        $result = collect();

        foreach ($items as $row) {
            $scores = $row['attributes'];
            unset($scores['fixture_id']);
            $result->push($this->updateById($row['id'], $scores));
        }
        return $result;
    }

    public function fillAttributes(Prediction $prediction, $attributes)
    {
        if (array_has($attributes, 'home_goals')) {
            $prediction->home_goals = $attributes['home_goals'];
        }
        if (array_has($attributes, 'away_goals')) {
            $prediction->away_goals = $attributes['away_goals'];
        }

        if (array_has($attributes, 'penalty_winner')) {
            if ($attributes['penalty_winner'] == $prediction->fixture->home_team) {
                $prediction->penalties_home = 5;
                $prediction->penalties_away = 0;
            } else {
                $prediction->penalties_home = 0;
                $prediction->penalties_away = 5;
            }
        }

        return $prediction;
    }
}
