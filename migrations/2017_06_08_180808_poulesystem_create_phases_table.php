<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoulesystemCreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->timestamps();
        });

        Schema::create('competition_phase', function (Blueprint $table) {
            $table->integer('competition_id')->unsigned();
            $table->foreign('competition_id')
                ->references('id')
                ->on('competitions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('phase_id')->unsigned();
            $table->foreign('phase_id')
                ->references('id')
                ->on('phases')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->smallInteger('max_groups');
            $table->smallInteger('fixtures_in_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_phase');
        Schema::dropIfExists('phases');
    }
}
