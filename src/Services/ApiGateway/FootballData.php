<?php namespace Stefandebruin\PouleSystem\Services\ApiGateway;
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-16
 * Time: 14:37
 */

use Carbon\Carbon;
use GuzzleHttp\Client;
use Football;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Models\Result;
use Stefandebruin\PouleSystem\Models\Score;
use Stefandebruin\PouleSystem\Models\Team;
use Stefandebruin\PouleSystem\Services\FixtureService;

class FootballData implements ApiGatewayInterface
{
    /**
     * FootballData constructor.
     * @param Client $client
     */
    public function __construct()
    {
    }

    /**
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection|mixed
     */
    public function getLeagues($output): \Illuminate\Support\Collection
    {

        $competitions = Football::getLeagues();

        $bar = $output->createProgressBar(count($competitions));

        $collection = collect();
        foreach ($competitions as $competition) {
            if (!empty($competition->currentSeason)) {
                /** @var Competition $competition */
                $competitionModel = Competition::whereApiId($competition->id)->first();
                if (!$competitionModel instanceof Competition) {
                    $competitionModel = new Competition();
                }

                $competitionModel->api_id = $competition->id;
                $competitionModel->setTranslation('title', 'en', $competition->name . ' ' . \Carbon\Carbon::parse($competition->currentSeason->startDate)->year)
                    ->setTranslation('title', 'nl', $competition->name . ' ' . \Carbon\Carbon::parse($competition->currentSeason->startDate)->year);
                $competitionModel->key = $competition->code ?? '';
                $competitionModel->year = \Carbon\Carbon::parse($competition->currentSeason->startDate)->year;

                if (!$competitionModel->exists) {
                    $competitionModel->visible = 0;
                    $competitionModel->type = $this->mapApiTypeToLocalType($competition->code ?? "");
                }

                $competitionModel->save();

                $collection->push($competitionModel);
            }
            $bar->advance();
        }
        $bar->finish();
        echo "\n";
        return $collection;
    }

    /**
     * @param Competition $competition
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection
     */
    public function getTeams(Competition $competition, $output): \Illuminate\Support\Collection
    {
        $teams = Football::getLeagueTeams($competition->api_id, ['season' => $competition->year]);
        $collection = Collect();

        $bar = $output->createProgressBar(count($teams));
        foreach ($teams as $row) {
            $team = Team::whereApiId($row->id)->first();
            if (!$team instanceof Team) {
                $team = new Team();
            }

            $team->api_id = $row->id;
            $team->code = $row->tla;
            $team->name = $row->name;
            $team->image = $row->crestUrl;
            $team->save();
            $collection->push($team);
            $bar->advance();
        }
        $bar->finish();
        echo "\n";
        return $collection;
    }

    /**
     * @param Competition $competition
     * @param \Illuminate\Console\OutputStyle $output
     * @return \Illuminate\Support\Collection
     */
    public function getFixtures(Competition $competition, $output): \Illuminate\Support\Collection
    {
        /** @var Collection $matches */
        $matches = Football::getLeagueMatches($competition->api_id, ['season' => $competition->year]);
        $matches->each(function ($item) {
            $item->utcDate = Carbon::createFromFormat(
                "Y-m-d H:i:s",
                str_replace(['T', 'Z'], "", $item->utcDate),
                'UTC'
            );
        });
        $competition->getHelper()->beforeCreatingFixtures($matches);
        $collection = collect();
        $bar = $output->createProgressBar(count($matches));

        foreach ($competition->phases as $phase) {
            app()->singleton('phaseId', function () use ($phase) {
                return $phase;
            });

            if ($competition->getHelper()->needSwitchPhase($phase, $bar->getProgress())) {
                continue;
            }

            foreach ($matches as $index => $fixture) {
                try {
                    $created = $this->createFixture($competition, $phase, $fixture);
                    if ($created) {
                        $matches->forget($index);
                    }
                    $phase->relationLoaded('fixtures');
                } catch (\Exception $e) {
                    dd(12345, $e);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        return $collection;
    }


    /**
     * @param Competition $competition
     * @param $output
     * @return \Illuminate\Support\Collection
     */
    public function getScores(Competition $competition, $output): \Illuminate\Support\Collection
    {
        $allActiveFixtures = app()->make(FixtureService::class)->getAllActiveFixtures($competition);
        $allFixtures = Fixture::all();
        $allActiveFixturesIds = $allActiveFixtures->pluck('api_id')->toArray();

        $changedScores = collect();
        $bar = $output->createProgressBar(count($allActiveFixtures));
        if ($allActiveFixtures->count() > 0) {
            $matches = Football::getLeagueMatches(
                $competition->api_id,
                [
                    'season' => $competition->year,
                    'dateFrom' => Carbon::now()->subDays(7)->format('Y-m-d'),
                    'dateTo' => Carbon::now()->format('Y-m-d')
                ]
            );


            foreach ($matches as $row) {
//                if (in_array($row->id, $allActiveFixturesIds)) {
                    /** @var Fixture $fixture */
                $fixture = $allFixtures->where('api_id', $row->id)->first();
                if(!$fixture){
                    continue;
                }

                    if (!is_object($row->score)) {
                        return false;
                    }
                    $fixture->status = $fixture->formatStatus($row->status);
                    $fixture->save();

                    /** @var Result $result */
                    $result = $fixture->result;
                    if (!$result instanceof Result) {
                        $result = new Result(
                            [
                                'fixture_id' => $fixture->id,
                                'user_id' => null
                            ]
                        );
                    }

                    $this->fillScores($result, $row->score, 'fullTime');
                    $this->fillScores($result, $row->score, 'halfTime');
                    $this->fillScores($result, $row->score, 'extraTime');
                    $this->fillScores($result, $row->score, 'penalties');

                    if ($result->isDirty()) {
                        $result->Save();
                        $fixture->setRelation('result', $result);
                        $changedScores->push($result);
                    }
                    $bar->advance();
//                }
            }
        }
        $bar->finish();
        echo "\n";

        return $changedScores;
    }

    private function fillScores(Result $result, $apiData, string $type)
    {
        $result->setAttribute(
            $this->getColumnPrefix($type) . 'home' . $this->getColumnSuffix($type),
            $apiData->$type->homeTeam ?? 0
        );
        $result->setAttribute(
            $this->getColumnPrefix($type) . 'away' . $this->getColumnSuffix($type),
            $apiData->$type->awayTeam ?? 0
        );
        return $result;
    }

    private function getColumnSuffix($type)
    {
        return $type !== 'penalties' ? '_goals' : '';
    }

    private function getColumnPrefix($type)
    {
        switch ($type) {
            case 'halfTime':
                $columnPrefix = 'halftime_';
                break;
            case 'extraTime':
                $columnPrefix = 'extratime_';
                break;
            case 'penalties':
                $columnPrefix = 'penalties_';
                break;
            default:
                $columnPrefix = '';
                break;
        }

        return $columnPrefix;
    }

    private function mapApiTypeToLocalType($league)
    {
        $types = ['EC' => 3, 'WC' => 4, 'CL' => 3, 'DED' => 2];
        return array_get($types, $league, 1);
    }

    /**
     * @param CompetitionTypeInterface $competitionType
     * @param Competition $competition
     * @param Phase $phase
     * @param $fixture
     * @return bool
     * @throws \Exception
     */
    private function createFixture(
        Competition $competition,
        Phase $phase,
        $fixture
    ): bool
    {
        /** @var Fixture $fixtureModel */
        try {
            $fixtureModel = Fixture::whereApiId($fixture->id)->first();
            if (!$fixtureModel instanceof Fixture) {
                $fixtureModel = new Fixture();
            }
        } catch (ModelNotFoundException $modelNotFoundException) {
            $fixtureModel = new Fixture();
        }

        if (!$competition->getHelper()->connectedToPhase($phase, $fixture)) {
            return false;
        }
        $group = $competition->getHelper()->findCorrectGroup($phase, $fixture);

        if (!is_null($fixtureModel->phase_id) && $fixtureModel->phase_id != $phase->id) {
            return false;
        }

        $homeTeam = Team::whereApiId($fixture->homeTeam->id)->first()->id;
        $awayTeam = Team::whereApiId($fixture->awayTeam->id)->first()->id;
        $fixtureModel->api_id = $fixture->id;
        $fixtureModel->competition_id = $competition->id;
        $fixtureModel->matchday = $fixture->matchday;
        $fixtureModel->date = $fixture->utcDate->toDateTimeString();
        $fixtureModel->status = $fixtureModel->formatStatus($fixture->status);
        $fixtureModel->home_team = $homeTeam;
        $fixtureModel->away_team = $awayTeam;
        $fixtureModel->phase_id = $phase->id;
        $fixtureModel->group_id = $group->id;

        if (empty($fixtureModel->id) && $fixtureModel->status == Fixture::STATUS_FINISHED) {
            $fixtureModel->status = Fixture::STATUS_IN_PLAY;
        }

        $fixtureModel->save();

        if (!$group->teams->where('id', $homeTeam)->first() instanceof Team) {
            $group->teams()->attach(
                [
                    $homeTeam =>
                        [
                            'competition_id' => $competition->id,
                            'phase_id' => $phase->id
                        ]
                ]
            );
            $group->load('teams');
        }

        if (!$group->teams->where('id', $awayTeam)->first() instanceof Team) {
            $group->teams()->attach(
                [
                    $awayTeam =>
                        [
                            'competition_id' => $competition->id,
                            'phase_id' => $phase->id
                        ]
                ]
            );
            $group->load('teams');
        }
        return true;
    }

}