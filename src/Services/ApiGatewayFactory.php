<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-17
 * Time: 10:59
 */

namespace Stefandebruin\PouleSystem\Services;

use Stefandebruin\PouleSystem\Exceptions\GatewayException;
use Stefandebruin\PouleSystem\Services\ApiGateway\ApiGatewayInterface;

class ApiGatewayFactory
{
    /**
     * @param null $gateway
     * @return mixed
     * @throws GatewayException
     */
    public static function make($gateway = null)
    {
        if (is_null($gateway)) {
            $gateway = config('poulesystem.defaultProvider');
        }

        if (array_has(config('poulesystem.gateways'), $gateway)) {
            $class = app()->make(config('poulesystem.gateways.' . $gateway));
            if ($class instanceof ApiGatewayInterface) {
                return $class;
            }
            throw new GatewayException("", 110);
        }
        throw new GatewayException($gateway, 100);
    }

    public static function all()
    {
        $gateways = collect();

        foreach (config('poulesystem.gateways') as $key => $gateway) {
            $class = app()->make($gateway);
            if ($class instanceof ApiGatewayInterface) {
                $gateways->put($key, $class);
            }
        }
        return $gateways;
    }
}
