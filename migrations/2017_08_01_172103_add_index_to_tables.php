<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fixtures', function (Blueprint $table) {
            $table->index(['competition_id']);
            $table->index(['competition_id', 'phase_id']);
            $table->index(['competition_id', 'phase_id', 'group_id']);
        });

        Schema::table('scores', function (Blueprint $table) {
            $table->index(['fixture_id']);
            $table->index(['fixture_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fixtures', function (Blueprint $table) {
            $table->dropIndex(['competition_id']);
            $table->dropIndex(['competition_id', 'phase_id']);
            $table->dropIndex(['competition_id', 'phase_id', 'group_id']);
        });

        Schema::table('scores', function (Blueprint $table) {
            $table->dropIndex(['fixture_id']);
            $table->dropIndex(['fixture_id', 'user_id']);
        });
    }
}
