<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 01-04-18
 * Time: 15:26
 */

namespace Stefandebruin\PouleSystem\Rules;

use Illuminate\Contracts\Validation\Rule;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Repositories\Api\FixtureRepositoryInterface;

class PenaltyWinnerExist implements Rule
{
    /**
     * @var FixtureRepositoryInterface
     */
    private $fixtureRepository;

    /**
     * Create a new rule instance.
     *
     * @param FixtureRepositoryInterface $fixtureRepository
     */
    public function __construct(FixtureRepositoryInterface $fixtureRepository)
    {
        //
        $this->fixtureRepository = $fixtureRepository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var Fixture $fixture */
        return true;
        $fixture = $this->fixtureRepository->find(request()->input('data.attributes.fixture_id'));
        if(!$fixture instanceof Fixture) return false;

        return $value === $fixture->home_team || $value === $fixture->away_team;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Team not connected to this match';
    }
}
