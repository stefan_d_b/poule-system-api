<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Controllers;

use App\Http\Controllers\Controller;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\PouleSystemServiceProvider;
use Stefandebruin\PouleSystem\Services\PodiumService;

class PodiumController extends Controller
{
    /**
     * @var PodiumService
     */
    private $podiumService;

    /**
     * PodiumController constructor.
     * @param PodiumService $podiumService
     */
    public function __construct(PodiumService $podiumService)
    {
        $this->podiumService = $podiumService;
    }

    /**
     * @param Competition $competition
     * @return \Stefandebruin\PouleSystem\Resources\Podium
     */
    public function competitionShow(Competition $competition)
    {
        $podium = $this->podiumService->buildPodium($competition);
        $users = PouleSystemServiceProvider::getUsersForDomain();
//        $podium->allowedUsers($users);
        return new \Stefandebruin\PouleSystem\Resources\Podium($podium);
    }
}
