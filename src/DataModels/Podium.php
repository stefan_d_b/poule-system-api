<?php namespace Stefandebruin\PouleSystem\DataModels;

use Illuminate\Support\Collection;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 19-11-17
 * Time: 13:24
 */

/**
 * @property Collection rows
 * @package Stefandebruin\PouleSystem\DataModels
 */
class Podium extends DataModel
{

    protected $attributes = [
        'virtual' => false,
    ];

    public function connectRows(Collection $rows): self
    {
        $this->attributes['rows'] = $rows;
        return $this;
    }

    public function AddUserPodiumRowIfMissing($userId)
    {
        if ($this->attributes['rows']->where('user_id', $userId)->count() === 0) {
            $row = new PodiumRow();
            $row->userId = $userId;
            $row->points = 0;
            $row->matches = [];
            $this->attributes['rows']->push($row);
        }
    }

    public function allowedUsers(array $users): self
    {
        $rows = $this->getRelation('podiumRows')->filter(function ($value, $index) use ($users) {
            if (config('poulesystem.domain.userConnected') && !in_array($value->userId, $users)) {
                return false;
            }
            return true;
        })->values();

        $this->setRelation('podiumRows', $rows);
        return $this;
    }

    public function sortRowsByDesc()
    {
        $position = 1;

        $rows = $this->getRelation('podiumRows')
            ->sortByDesc('points')
            ->reject(function ($value, $key) {
                return empty($value->user_id);
            })
            ->each(function ($item, $key) use (&$position) {
                $item->position = $position;
                $position++;
            });
        $this->setRelation('podiumRows', $rows->values());
    }

    /**
     * @return bool
     */
    public function isVirtual(): bool
    {
        $virtual = $this->rows->first(function ($value, $key) {
            return $value['virtual'] > 0;
        });

        return $virtual != null;
    }

    public function toCacheArray()
    {
        $data = [];
        if(array_has($this->attributes, 'rows')){
            foreach ($this->attributes['rows'] as $row) {
                $data[$row->userId] = [
                    'points' => $row->points,
                    'matches' => $row->matches,
                ];
            }
        }
        return $data;
    }

    public static function fillFromCache($data)
    {
        $podium = new self();

        dd($data);
        /** @var Collection $podiumRows */
        $podiumRows = new Collection();
        foreach ($data as $userID => $podiumRow) {
//            /** @var PodiumRow $rowModel */
//            $rowModel = app()->make(PodiumRow::class)->fill(
//                [
//                    'userId' => $userID,
//                    'points' => $podiumRow['points'],
//                    'matches' => $podiumRow['matches'],
//                ]
//            );
//            $podiumRows->push($rowModel);
        }

        /** @var Podium $podium */
//        $podium->connectRows($podiumRows);
//        $podium->sortRowsByDesc();
        return $podium;
    }
}
