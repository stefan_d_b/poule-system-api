<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:42
 */

namespace Stefandebruin\PouleSystem\Console\Import;

use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Repositories\Api\CompetitionRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\CompetitionRepository;
use Stefandebruin\PouleSystem\Repositories\TeamRepository;

class Teams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'poulesystem:import:teams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the team';

    /**
     * @var TeamRepository
     */
    protected $teamRepository = null;

    /**
     * @var CompetitionRepository
     */
    protected $competitionRepo = null;


    /**
     * Create a new command instance.
     *
     * @param CompetitionRepositoryInterface $competitionRepo
     * @param TeamRepository $teamRepository
     */
    public function __construct(
        CompetitionRepositoryInterface $competitionRepo,
        TeamRepository $teamRepository
    ) {
        parent::__construct();
        $this->competitionRepo = $competitionRepo;
        $this->teamRepository = $teamRepository;
    }

    public function handle()
    {
        /** @var Competition $competition */
        foreach (Competition::visible()->get() as $competition) {
            $this->line(sprintf('Import teams for %s', $competition->title));
            $teams = $competition->getGateway()->getTeams($competition, $this->output);
            $competition->teams()->sync($teams->pluck('id', 'id'));
        }
    }
}
