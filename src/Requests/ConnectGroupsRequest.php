<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 31-03-18
 * Time: 22:23
 */

namespace Stefandebruin\PouleSystem\Requests;

use Stefandebruin\JsonApi\Requests\JsonApiRelationRequest;

class ConnectGroupsRequest extends JsonApiRelationRequest
{

}