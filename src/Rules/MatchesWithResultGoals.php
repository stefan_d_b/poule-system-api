<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 01-04-18
 * Time: 15:12
 */

namespace Stefandebruin\PouleSystem\Rules;


use Illuminate\Contracts\Validation\Rule;

class MatchesWithResultGoals implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $attributeKey = str_replace('data.attributes.', '', $attribute);
        $resultKey = str_contains($attributeKey, 'home') ? 'home_goals': 'away_goals';

        return request()->input('data.attributes.'.$resultKey, 0) >= $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Score can not be greater than the result score';
    }
}
