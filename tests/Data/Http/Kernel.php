<?php namespace Stefandebruin\PouleSystem\Test\Data\Http;
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 28/10/2018
 * Time: 11:07
 */

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Routing\Router;
use Illuminate\Contracts\Foundation\Application;

class Kernel extends HttpKernel
{
    public function __construct(Application $app, Router $router)
    {
        parent::__construct($app, $router);
        ;
    }

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'jsonApi' => \Stefandebruin\JsonApi\Middleware\JsonApi::class,
    ];
}
