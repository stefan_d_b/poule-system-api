<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 19-07-17
 * Time: 20:11
 */

namespace Stefandebruin\PouleSystem\Console\Notifications;

use App\NotificationSettings;
use App\User;
use Illuminate\Console\Command;

class UpgradeDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poulesystem:notification:upgrade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        $notifications = collect();
        foreach (config('notification') as $key => $configNotification) {
            $model = new NotificationSettings();
            $model->notification = $key;
            $model->providers = array_get($configNotification, 'providers', []);
            $model->options = array_get($configNotification, 'options', []);

            $notifications->push($model);
        }

        /** @var User $user */
        foreach (User::all() as $user) {
            /** @var NotificationSettings $notification */
            foreach ($notifications as $notification) {
                $userNotifictionTypes = $user->notificationSettings->pluck('notification')->toArray();
                if (!in_array($notification->notification, $userNotifictionTypes)) {
                    /** @var NotificationSettings $modelData */
                    $modelData = clone $notification;
                    $modelData->user_id = $user->id;
                    $modelData->save();
                }
            }
        }
    }
}
