<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 2019-03-17
 * Time: 13:55
 */

namespace Stefandebruin\PouleSystem\Console;

use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'poulesystem:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install this plugin';

    /**
     *
     */
    public function handle()
    {
        $this->addPhases();
        $this->addGroups();
    }

    private function addPhases()
    {
        $neededPhases = array_merge(
            [
                'group' => 'Groups Phase',
                'round-of-16' => 'Round of 16',
                'q-finals' => 'Quarter Finals',
                's-finals' => 'Semi finals',
                'third-place' => 'Third place play-off',
                'final' => 'Final',
            ],
            $this->getMonthPhases()
        );


        $phases = Phase::all();

        foreach ($neededPhases as $key => $missingItem) {
            $phase = $phases->where('key', $key)->first();
            if (!$phase instanceof Phase) {
                $phase = new Phase();
            }
            $phase->fill([
                'key' => $key,
            ])->setTranslation('title', 'en', $missingItem)
                ->setTranslation('title', 'nl', $missingItem);

            $phase->save();
        }
    }

    private function addGroups()
    {
        $groups = [
            'group-a' => 'Group A',
            'group-b' => 'Group B',
            'group-c' => 'Group C',
            'group-d' => 'Group D',
            'group-e' => 'Group E',
            'group-f' => 'Group F',
            'group-g' => 'Group G',
            'group-h' => 'Group H',
            'group-i' => 'Group I',
        ];
        for ($index = 1; $index <= 53; $index++) {
            $groups['w' . $index] = sprintf('Week %s', $index);
        }

        foreach (array_diff(array_keys($groups), Group::all()->pluck('key', 'id')->toArray()) as $missingItem) {
            $group = new Group();
            $group->fill([
                'key' => $missingItem,
                'title' => $groups[$missingItem]
            ]);
            $group->system_created = 1;
            $group->owner_id = 0;
            $group->save();
        }
    }

    private function getMonthPhases()
    {
        $start = Carbon::createFromDate(2019, 01, 01)->startOfDay();
        $end = Carbon::createFromDate(2019, 12, 31)->endOfDay();
        $interval = DateInterval::createFromDateString('1 month'); // 1 month interval
        $period = new DatePeriod($start, $interval, $end); // Get a set of date beetween the 2 period

        $months = array();

        foreach ($period as $dt) {
            $months['m' . $dt->format("n")] = $dt->format("F");
        }
        return $months;
    }
}