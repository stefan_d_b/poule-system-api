<?php namespace Stefandebruin\PouleSystem\Console;
use Illuminate\Database\Eloquent\Collection;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 15-11-17
 * Time: 17:13
 */

trait OfficialResultTrait{
    public function handle()
    {
        if (method_exists($this, 'selectCompetitions')) {
            $competitions = $this->selectCompetitions();
            if (!$competitions instanceof Collection) {
                $this->error("no collection");
                return false;
            }
        } else {
            $competitions = $this->competitionRepository->all();
        }


        foreach ($competitions as $competition) {
            //TODO-stefan build the podium
            $podiumBefore = $this->podiumService->buildPodium($competition->id);
            $this->info("import result: " . $competition->title);

            $this->importResults($competition);


            //TODO-stefan build the podium
            $podiumAfter = $this->podiumService->buildPodium($competition->id);
            //TODo-stefan get the differences between the 2 podiums with a broadcast event
            $this->podiumService->broadcastPodiumChanges($competition, $podiumBefore, $podiumAfter);
        }
        return true;
    }
}
