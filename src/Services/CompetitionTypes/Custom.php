<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-07-18
 * Time: 11:32
 */

namespace Stefandebruin\PouleSystem\Services\CompetitionTypes;

use Illuminate\Support\Collection;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Models\Fixture;
use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Phase;
use Stefandebruin\PouleSystem\Repositories\Api\GroupRepositoryInterface;
use Stefandebruin\PouleSystem\Repositories\Api\PhaseRepositoryInterface;

class Custom implements CompetitionTypeInterface
{
    /**
     * @var Competition
     */
    private $competition;
    /**
     * @var PhaseRepositoryInterface
     */
    private $phaseRepository;
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * Month constructor.
     * @param PhaseRepositoryInterface $phaseRepository
     * @param GroupRepositoryInterface $groupRepository
     * @param $competition
     */
    public function __construct(
        PhaseRepositoryInterface $phaseRepository,
        GroupRepositoryInterface $groupRepository,
        $competition
    )
    {
        $this->phaseRepository = $phaseRepository;
        $this->competition = $competition;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @param Phase $phase
     * @param $fixture
     * @return mixed
     */
    public function connectedToPhase(Phase $phase, $fixture): bool
    {
        return $phase->key === 'm' . $fixture->utcDate->month;
    }

    /**
     * @param Phase $phase
     * @param $fixture
     * @return Group
     */
    public function findCorrectGroup(Phase $phase, $fixture): Group
    {
        $phase->load('groups');

        $groupsCollection = $this->groupRepository->all();
        $key = 'w' . $fixture->utcDate->weekOfYear;

        $alreadyConnected = $phase->groups->where('key', $key)->count() > 0;
        $group = $groupsCollection->where('key', $key)->first();

        if (!$alreadyConnected) {
            $phase->groups()->attach(
                $group,
                [
                    'competition_id' => $this->competition->id,
                    'order_by' => $phase->groups->where('key', $key)->count() + 1,
                ]
            );
        }

        return $group;
    }

    /**
     * @param Collection|Fixture $fixtures
     * @return Competition
     */
    public function beforeCreatingFixtures(Collection $fixtures): Competition
    {
        $allPhases = $this->phaseRepository->all();
        $months = array_map(
            function ($date) use ($allPhases) {
                return $allPhases->where('key', 'm' . $date->month)->first()->id;
            },
            $fixtures->pluck('utcDate')->toArray()
        );
        $months = collect($months);

        $this->competition->load('phases');
        $this->competition->phases()->sync(array_values($months->unique()->toArray()));

        return $this->competition;
    }

    public function needSwitchPhase(Phase $phase, $index): bool
    {
        return false;
    }
}