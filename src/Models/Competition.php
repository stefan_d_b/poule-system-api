<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

namespace Stefandebruin\PouleSystem\Models;

use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Stefandebruin\JsonApi\JsonApi;
use Stefandebruin\JsonApi\Models\JsonModel;
use Stefandebruin\JsonApi\Scopes\UpdatedOnly;
use Stefandebruin\PouleSystem\Facades\OverrideModels;
use Stefandebruin\PouleSystem\Services\ApiGateway\ApiGatewayInterface;
use Stefandebruin\PouleSystem\Services\ApiGatewayFactory;
use Stefandebruin\PouleSystem\Services\CompetitionTypes\CompetitionTypeFactory;
use Stefandebruin\PouleSystem\Services\CompetitionTypes\CompetitionTypeInterface;

/**
 * Stefandebruin\PouleSystem\Models\Competition
 *
 * @method LengthAwarePaginator jsonPaginate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string typeLabel
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Fixture[] $fixtures
 * @property-read array|null|string $type_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Phase[] $phases
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Team[] $teams
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|Competition visible()
 * @method static \Illuminate\Database\Eloquent\Builder|Competition visibleForUser($userId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereApiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Competition whereYear($value)
 * @property mixed $id
 * @property mixed $api_id
 * @property array $title
 * @property string $key
 * @property mixed $year
 * @property bool $visible
 * @property int $type
 */
class Competition extends JsonModel
{
    use HasTranslations, JsonApi;

    public $translatable = ['title'];

    protected $attributes = ['api_id' => 0];

    protected $fillable = [
        'api_id',
        'title',
        'key',
        'year',
        'visible',
        'type'
    ];

    protected $hidden = [
        'api_id',
        'visible',
        'created_at',
        'updated_at',
        'pivot'
    ];

    protected $hiddenRelations = [
        'users'
    ];

    protected $casts = [
        'visible' => 'boolean',
        'id' => 'number',
        'api_id' => 'number',
        'year' => 'number',
        'title' => 'array',
    ];

    protected $appends = [
        'locked'
    ];

    public static $TYPECOMPETITIONWEEK = 1;
    public static $TYPECOMPETITIONMONTH = 2;
    public static $EUROPE = 3;
    public static $WORLD = 4;
    public static $CUSTOM = 5;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UpdatedOnly);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Stefandebruin\PouleSystem\Models\Fixture
     */
    public function fixtures()
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Fixture', 'competition_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Stefandebruin\PouleSystem\Models\Phase
     */
    public function phases()
    {
        return $this->belongsToMany('Stefandebruin\PouleSystem\Models\Phase')->orderBy('order_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Stefandebruin\PouleSystem\Models\Team
     */
    public function teams()
    {
        return $this->belongsToMany('Stefandebruin\PouleSystem\Models\Team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\User
     */
    public function users()
    {
        return $this->belongsToMany(OverrideModels::getUserModel());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Stefandebruin\PouleSystem\Models\Group
     */
    public function groups()
    {
        return $this->belongsToMany(
            'Stefandebruin\PouleSystem\Models\Group',
            'competition_phase_group'
        )->distinct();
    }

    /**
     * @return array|null|string
     */
    public function getTypeLabelAttribute()
    {
        switch ($this->type) {
            case 1:
                return __('Week');
                break;
            case 2:
                return __('Month');
                break;
            case 3:
                return __('EK');
                break;
            case 4:
                return __('WK');
                break;
            default:
                return __('Custom');
        }
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        return $query->where('visible', 1);
    }

    /**
     * Scope a query to only include popular users.
     *
     * TODO-stefan parameter $userId ombouwen naar een volledig model ddd
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param null $userId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisibleForUser($query, $userId = null)
    {
        if (config('poulesystem.enroll_competition') && (\Auth::check() || !is_null($userId))) {
            /** @var User $user */
            $user = \Auth::user();
            if (!is_null($userId)) {
                $user = User::find($userId);
            }
            $query->whereIn('id', $user->competitions->pluck('id')->toArray());
        }
        return $this->scopeVisible($query);
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        return $this->where($this->getRouteKeyName(), $value)->first();
    }

    public function getUrl()
    {
        return route('poulesystem.competitions.show', ['competition' => $this->id]);
    }

    public function getLockedAttribute()
    {
        return $this->api_id > 0;
    }

    /**
     * @return ApiGatewayInterface
     */
    public function getGateway()
    {
        return ApiGatewayFactory::make();
    }

    public function getHelper(): CompetitionTypeInterface
    {
        return app()->make(CompetitionTypeFactory::class)->create($this);
    }
}
