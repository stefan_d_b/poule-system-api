<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Stefandebruin\JsonApi\JsonApi;
use Stefandebruin\JsonApi\Models\JsonModel;

/**
 * Stefandebruin\PouleSystem\Models\Team
 *
 * @method LengthAwarePaginator jsonPaginate
 * @property string|null $image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Fixture[] $awayFixtures
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Competition[] $competitions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Fixture[] $homeFixtures
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereApiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id
 * @property int $api_id
 * @property string|null $code
 * @property array $name
 */
class Team extends JsonModel
{
    use HasTranslations, JsonApi;

    public $translatable = ['name'];

//    protected $resource = \Stefandebruin\PouleSystem\Resources\Team::class;

    protected $fillable = [
        'api_id',
        'code',
        'name',
        'image'
    ];

    protected $hidden = [
        'api_id',
        'created_at',
        'updated_at',
        'pivot',
        'competition_id',
        'team_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'api_id' => 'integer',
    ];

    protected $attributes = [
        'api_id' => 0
    ];


    public function homeFixtures()
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Fixture', 'home_team');
    }

    public function awayFixtures()
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Fixture', 'away_team');
    }

    public function competitions()
    {
        return $this->belongsToMany('Stefandebruin\PouleSystem\Models\Competition');
    }

    public function groups()
    {
        return $this->belongsToMany('Stefandebruin\PouleSystem\Models\Group');
    }

    public function loadFixtures()
    {
        $this->load('homeFixtures');
        $this->load('awayFixtures');

        $fixtures = $this->getRelation('homeFixtures')->merge($this->getRelation('awayFixtures'));
        $this->setRelation('fixtures', $fixtures);
        return $this;
    }

    public function getUrl()
    {
        return route('poulesystem.team.show', ['team' => $this->id]);
    }
}
