<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 17:24
 */


//Route toevoegen fases koppelen aan een competitie
use Illuminate\Support\Facades\Route;
use Stefandebruin\PouleSystem\Models\Phase;

Route::namespace('\Stefandebruin\PouleSystem\Controllers')->group(function () {
    Route::group(
        [
            'prefix' => Config::get('poulesystem.prefix'),
            'middleware' => Config::get('poulesystem.middleware')
        ],
        function () {

            Route::group(['prefix' => 'competitions'], function () {

                Route::get('/', 'CompetitionController@index')
                    ->name('poulesystem.competitions')
                    ->middleware(['jsonApi:include,fields,sort,filter']); //TEST

                Route::post('/', 'CompetitionController@create')
                    ->name('poulesystem.competitions.create');
                   // ->middleware(['can:create']); //TEST

                Route::group(['prefix' => '{competition}'], function () {
                    Route::get('/', 'CompetitionController@show')
                        ->name('poulesystem.competitions.show')
                        ->middleware(['jsonApi:include,fields']); //TEST

                    Route::put('/', 'CompetitionController@update')
                        ->name('poulesystem.competitions.update'); //TEST

                    Route::delete('/', 'CompetitionController@destroy')
                        ->name('poulesystem.competitions.destroy'); //TEST

                    Route::get('teams', 'TeamController@competitionShow')
                        ->name('competitions.teams')
                        ->middleware(['jsonApi:include,fields']); //TEST

                    Route::get('groups', 'GroupController@competitionShow')
                        ->name('competitions.groups')
                        ->middleware(['jsonApi:include,fields']); //TEST

                    Route::get('fixtures', 'FixtureController@competition')
                        ->name('competitions.fixtures')
                        ->middleware(['jsonApi:include,fields']); //TEST

                    Route::get('podium', 'PodiumController@competitionShow')
                        ->name('poulesystem.competitions.podium'); //TEST

                    Route::group(['prefix' => 'relationships'], function () {
                        Route::group(['prefix' => 'teams'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('competitions.relationship.teams'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.competitions.relationships.teams'); //TEST
                        });

                        Route::group(['prefix' => 'groups'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('competitions.relationship.groups'); //TEST

                            Route::put('/', 'GroupController@competitionShow')
                                ->name('poulesystem.competitions.relationships.groups');
                        });

                        Route::group(['prefix' => 'phases'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('competitions.relationship.phases'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.competitions.relationships.connect.phases');
                        });

                        Route::group(['prefix' => 'users'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('competitions.relationship.users'); //function added

                            Route::put('/', 'CompetitionController@connectUsers')
                                ->name('poulesystem.competitions.relationships.connect.users');
                        });

                        Route::group(['prefix' => 'fixtures'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('competitions.relationship.fixtures'); //function added

                            Route::put('/', 'FixtureController@competition')
                                ->name('poulesystem.competitions.relationships.connect.fixtures');
                        });
                    });

                    Route::group(['prefix' => 'phases'], function () {
                        Route::get('/', 'PhaseController@competitionShowCollection')
                            ->name('competitions.phases');

                        /**
                         * TODO-stefan dit blok verwijderen en aanroepen via resource zelf met filters
                         */
//                        Route::group(['prefix' => '{phase}'], function () {
//                            Route::group(['prefix' => 'relationships'], function () {
//                                Route::put('groups', 'PhaseController@connectGroups')
//                                    ->name('dsxfwfwffefewfewfwffeewff');
//
//                                Route::patch('groups', 'PhaseController@connectGroups')
//                                    ->name('dsxfwfwffefewfewfwffeewff');
//                            });
//
//                            Route::get('/', 'PhaseController@competition')
//                                ->name('poulesystem.competitions.phase.show')
//                                ->middleware(['enable.include.parameter']);
//
//                            Route::group(['prefix' => 'groups'], function () {
//                                Route::get('/', 'GroupController@phaseIndex')
//                                    ->name('poulesystem.competitions.phase.groups.all')
//                                    ->middleware(['json.api.urlParameters.enable:include,fields']);
//                            });
//                        });
                    });

                });
            });
            //
            Route::group(['prefix' => 'phases'], function () {
                Route::get('/', 'PhaseController@index')
                    ->name('poulesystem.phases.index')
                    ->middleware(['jsonApi:include,fields,sort,filter']); //TEST

                Route::post('/', 'PhaseController@store')
                    ->name('poulesystem.phases.create');

                Route::group(['prefix' => '{phase}'], function () {
                    Route::get('/', 'PhaseController@show')
                        ->name('poulesystem.phases.show')
                        ->middleware(['jsonApi:include,fields']); //TEST

                    Route::post('/', 'PhaseController@store')
                        ->name('poulesystem.phases.create'); //TEST

                    Route::put('/', 'PhaseController@update')
                        ->name('poulesystem.phases.update'); //TEST

                    Route::delete('/', 'PhaseController@destroy')
                        ->name('poulesystem.phases.destroy');

                    Route::get('competitions', 'CompetitionController@PhaseRelationList')
                        ->name('phases.competitions'); //TEST

                    Route::get('fixtures', 'FixtureController@phaseRelationList')
                        ->name('phases.fixtures'); //TEST

                    Route::get('groups', 'GroupController@phaseRelationList')
                        ->name('phases.groups'); //TEST

                    Route::group(['prefix' => 'relationships'], function () {
                        Route::group(['prefix' => 'competitions'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('phases.relationship.competitions'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.phases.relationship.competitions');
                        });

                        Route::group(['prefix' => 'fixtures'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('phases.relationship.fixtures'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.phases.relationship.fixtures');
                        });

                        Route::group(['prefix' => 'groups'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('phases.relationship.groups'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.phases.relationship.groups');
                        });
                    });
                });
            });

            Route::group(['prefix' => 'predictions'], function () {
                Route::get('/', 'PredictionController@index')
                    ->name('poulesystem.predictions.index');

                Route::post('/', 'PredictionController@store')
                    ->name('poulesystem.prediction.store')
                    ->middleware(['jsonApi:include,fields,sort', 'auth:api']); //TEST

                Route::post('/bulk', 'PredictionController@storeBulk')
                    ->name('poulesystem.prediction.store.bulk')
                    ->middleware(['jsonApi:include,fields,sort', 'auth:api']); //TEST

                Route::get('fixture', 'FixtureController@show')
                    ->name('groups.fixtures')
                    ->middleware(['jsonApi:include,fields,sort']); //TEST

                Route::group(['prefix' => '{prediction}'], function () {
                    Route::get('/', 'PredictionController@show')
                        ->name('poulesystem.predictions.show');

                    Route::patch('/', 'PredictionController@update')
                        ->name('poulesystem.prediction.update')
                        ->middleware(['jsonApi:include,fields,sort', 'auth:api']); //TEST

                    Route::put('/', 'PredictionController@update')
                        ->name('poulesystem.prediction.update');

                    Route::get('user', 'FixtureController@show')
                        ->name('prediction.fixture');

                    Route::group(['prefix' => 'relationships'], function () {
                        Route::group(['prefix' => 'user'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('prediction.relationship.user'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.prediction.relationship.user');
                        });

                        Route::group(['prefix' => 'fixture'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('prediction.relationship.fixture'); //TEST

                            Route::put('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@updateRelation')
                                ->name('poulesystem.prediction.relationship.fixture');
                        });
                    });
                });
            });

            Route::group(['prefix' => 'podiums'], function () {
                Route::group(['prefix' => '{competition}'], function () {
                    Route::get('/', 'PodiumController@competitionShow')
                        ->name('poulesystem.podium.show'); //TEST
                });
            });

            Route::group(['prefix' => 'teams'], function () {
                Route::get('/', 'TeamController@index')
                    ->name('poulesystem.team.index')
                    ->middleware(['jsonApi:include,fields,sort']); //TEST

                Route::post('/', 'TeamController@store');

                Route::group(['prefix' => '{team}'], function () {
                    Route::get('/', 'TeamController@show')
                        ->name('poulesystem.team.show'); //TEST

                    Route::put('/', 'TeamController@update')
                        ->name('poulesystem.team.update');

                    Route::get('homeFixtures', 'FixtureController@TeamHomeFixtures')
                        ->name('teams.homeFixtures');

                    Route::get('awayFixtures', 'FixtureController@TeamAwayFixtures')
                        ->name('teams.awayFixtures');

                    Route::get('competitions', 'FixtureController@competitions')
                        ->name('teams.competitions');

                    Route::get('groups', 'FixtureController@groups')
                        ->name('teams.groups');

                    Route::group(['prefix' => 'relationships'], function () {
                        Route::group(['prefix' => 'homeFixtures'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('teams.relationship.homeFixtures');
                        });

                        Route::group(['prefix' => 'awayFixtures'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('teams.relationship.awayFixtures');
                        });

                        Route::group(['prefix' => 'competitions'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('teams.relationship.competitions');
                        });

                        Route::group(['prefix' => 'groups'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('teams.relationship.groups');
                        });
                    });
                });
            });

            Route::group(['prefix' => 'groups', 'middleware' => ['auth:api']], function () {
                Route::get('/', 'GroupController@index')
                    ->name('poulesystem.group.index');
//                    ->middleware(['jsonApi:include,fields,sort,filter', 'auth:api']); //TEST

                Route::post('/', 'GroupController@store')
                    ->name('poulesystem.group.store'); //TEST

                Route::group(['prefix' => '{group}'], function () {
                    Route::get('/', 'GroupController@show')
                        ->name('poulesystem.group.show'); //TEST

                    Route::put('/', 'GroupController@update')
                        ->name('poulesystem.group.update'); //TEST

                    Route::delete('/', 'GroupController@destroy')
                        ->name('poulesystem.group.update');

                    Route::get('teams', 'TeamController@groupsRelationList')
                        ->name('groups.teams')
                        ->middleware(['jsonApi:include,fields,sort']); //TEST

                    Route::get('phases', 'PhaseController@groupRelationList')
                        ->name('groups.phase')
                        ->middleware(['jsonApi:include,fields,sort']); //TEST

                    Route::get('owner', 'UsersController@groupRelation')
                        ->name('groups.owner');

                    Route::get('competitions', 'CompetitionController@groupsRelationList')
                        ->name('groups.competitions')
                        ->middleware(['jsonApi:include,fields,sort']); //TEST

                    Route::get('fixtures', 'FixtureController@groupsRelationList')
                        ->name('groups.fixtures')
                        ->middleware(['jsonApi:include,fields,sort']); //TEST

                    Route::group(['prefix' => 'relationships'], function () {
                        Route::group(['prefix' => 'teams'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('groups.relationship.teams');
                        });

                        Route::group(['prefix' => 'phase'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('groups.relationship.phase');
                        });

                        Route::group(['prefix' => 'competitions'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('groups.relationship.competitions');
                        });

                        Route::group(['prefix' => 'owner'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('groups.relationship.owner');
                        });

                        Route::group(['prefix' => 'fixtures'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('groups.relationship.fixtures');
                        });
                    });
                });
            });

            Route::group(['prefix' => 'fixtures'], function () {

                Route::get('/', 'FixtureController@index')
                    ->name('poulesystem.fixtures.index'); //TEST

                Route::post('/', 'FixtureController@store');

                /**
                 * TODO-stefan settings relatie uitschakelen
                 * TODO-stefan settinsg toevoegen als directe attributen van de wedstrijd
                 */
                Route::group(['prefix' => '{fixture}'], function () {
                    Route::get('/', 'FixtureController@show')
                        ->name('fixtures.show')
                        ->middleware(['jsonApi:include,fields']); //TEST

                    Route::put('/', 'FixtureController@update');

                    Route::get('competitions', 'CompetitionController@fixtureShow')
                        ->name('fixtures.competition');

                    Route::get('phases', 'PhaseController@fixtureShow')
                        ->name('fixtures.phase');

                    Route::get('homeTeam', 'TeamController@fixtureShow')
                        ->name('fixtures.homeTeam');

                    Route::get('awayTeam', 'TeamController@fixtureShow')
                        ->name('fixtures.awayTeam');

                    Route::get('result', 'TeamController@fixtureShow')
                        ->name('fixtures.result');

                    Route::get('predictions', 'PredictionController@fixtureShow')
                        ->name('fixtures.predictions');

                    Route::get('prediction', 'PredictionController@fixtureShowOne')
                        ->name('fixtures.prediction');

                    Route::get('groups', 'GroupController@fixtureShow')
                        ->name('fixtures.group');


                    Route::group(['prefix' => 'relationships'], function () {
                        Route::group(['prefix' => 'competition'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.competition');
                        });

                        Route::group(['prefix' => 'phase'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.phase');
                        });

                        Route::group(['prefix' => 'homeTeam'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.homeTeam');
                        });

                        Route::group(['prefix' => 'awayTeam'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.awayTeam');
                        });

                        Route::group(['prefix' => 'result'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.result');
                        });

                        Route::group(['prefix' => 'predictions'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.predictions');
                        });

                        Route::group(['prefix' => 'prediction'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.prediction');
                        });

                        Route::group(['prefix' => 'group'], function () {
                            Route::get('/', '\Stefandebruin\JsonApi\Controllers\JsonRelationController@showRelation')
                                ->name('fixtures.relationship.group');
                        });
                    });
                });

            });

            Route::group(['prefix' => 'poule-configuration'], function () {
                Route::get('/', 'SettingsController@index')
                    ->name('poulesystem.configuration'); //TODO
            });
        }
    );
});
