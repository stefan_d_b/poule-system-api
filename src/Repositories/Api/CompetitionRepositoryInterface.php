<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories\Api;


use Stefandebruin\PouleSystem\Models\Competition;

interface CompetitionRepositoryInterface
{
    public function model():Competition;

    public function save(Competition $competition):Competition;

    public function getAllVisible();

    public function getByApiId($apiId);

    public function find($id);

    public function all();
}