<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 09:42
 */

namespace Stefandebruin\PouleSystem\Console\Import;

use Illuminate\Console\Command;
use Stefandebruin\PouleSystem\Models\Competition;
use Stefandebruin\PouleSystem\Services\PodiumService;

class OfficialResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'poulesystem:import:officialResult';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the team';

    /**
     * @var PodiumService
     */
    private $podiumService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->podiumService = app()->make(PodiumService::class);
    }

    public function handle()
    {
        /** @var Competition $competition */
        foreach (Competition::visible()->get() as $competition) {
            $podiumBefore = $this->podiumService->getPodiumData($competition);

            $this->line(sprintf('Import results for %s', $competition->title));
            $changedResults = $competition->getGateway()->getScores($competition, $this->output);

            $newPodium = $this->podiumService->storeVirtualPodiumScores($competition, $changedResults);
            $this->podiumService->broadcastChanges(
                $competition,
                $podiumBefore,
                $podiumBefore,
                $newPodium
            );
        }
    }
}
