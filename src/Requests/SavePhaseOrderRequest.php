<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 20:21
 */

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SavePhaseOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*' => ['required', 'array'],
            '*.phase_id' => ['required', 'nullable', 'integer', 'exists:phases,id'],
            '*.max_groups' => ['nullable', 'required', 'integer', 'min:0'],
            '*.fixtures_in_group' => ['nullable', 'required', 'integer'],
            '*.penalties' => ['nullable', 'required', 'boolean'],
        ];
    }
}