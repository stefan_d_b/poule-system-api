<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

namespace Stefandebruin\PouleSystem\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Stefandebruin\JsonApi\JsonApi;
use Stefandebruin\JsonApi\Models\JsonModel;

/**
 * Stefandebruin\PouleSystem\Models\Fixture
 *
 * @method LengthAwarePaginator jsonPaginate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Stefandebruin\PouleSystem\Models\Team $awayTeam
 * @property-read \Stefandebruin\PouleSystem\Models\Competition $competition
 * @property-read \Illuminate\Database\Eloquent\Collection|\Stefandebruin\PouleSystem\Models\Group[] $group
 * @property-read \Stefandebruin\PouleSystem\Models\Team $homeTeam
 * @property-read \Stefandebruin\PouleSystem\Models\Phase $phase
 * @property-read \Stefandebruin\PouleSystem\Models\Prediction $prediction
 * @property-read \Illuminate\Database\Eloquent\Collection|Prediction[] $predictionUser
 * @property-read \Illuminate\Database\Eloquent\Collection|Prediction[] $predictions
 * @property-read \Stefandebruin\PouleSystem\Models\Result $result
 * @property-read FixtureSetting $settings
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereApiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereAwayTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereCompetitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereHomeTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereMatchday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture wherePhaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fixture whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id
 * @property int $api_id
 * @property int $competition_id
 * @property int $phase_id
 * @property int $group_id
 * @property int $matchday
 * @property \Carbon\Carbon|null $date
 * @property int $status
 * @property int $home_team
 * @property int $away_team
 */
class Fixture extends JsonModel
{
    use JsonApi;

    const STATUS_SCHEDULED = 1;
    const STATUS_TIMED = 2;
    const STATUS_IN_PLAY = 3;
    const STATUS_POSTPONED = 4;
    const STATUS_CANCELED = 5;
    const STATUS_FINISHED = 6;

    protected $statusses = [
        'SCHEDULED' => self::STATUS_SCHEDULED,
        'TIMED' => self::STATUS_TIMED,
        'IN_PLAY' => self::STATUS_IN_PLAY,
        'POSTPONED' => self::STATUS_POSTPONED,
        'CANCELED' => self::STATUS_CANCELED,
        'FINISHED' => self::STATUS_FINISHED,
    ];

    protected $fillable = [
        'api_id',
        'competition_id',
        'phase_id',
        'matchday',
        'date',
        'status',
        'home_team',
        'away_team',
    ];

    protected $hidden = [
        'api_id',
        'competition_id',
        'phase_id',
        'group_id'
    ];

    protected $dates = [
        'date'
    ];

    protected $with = [
        'homeTeam',
        'awayTeam',
//        'settings'
    ];

    protected $casts = [
        'id' => 'integer',
        'api_id' => 'integer',
        'competition_id' => 'integer',
        'phase_id' => 'integer',
        'group_id' => 'integer',
        'matchday' => 'integer',
        'status' => 'integer',
    ];

    protected $resource = \Stefandebruin\PouleSystem\Resources\Fixture::class;

    protected $hiddenRelations = [
//        'predictions'
    ];

//    protected $appends = ['settings'];

    protected $persistentRelations = [
        'homeTeam',
        'awayTeam',
//        'settings'
    ];

    protected $appends = [
        'settings'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function competition()
    {
        return $this->belongsTo('Stefandebruin\PouleSystem\Models\Competition', 'competition_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function phase()
    {
        return $this->belongsTo('Stefandebruin\PouleSystem\Models\Phase', 'phase_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function homeTeam()
    {
        return $this->belongsTo('Stefandebruin\PouleSystem\Models\Team', 'home_team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function awayTeam()
    {
        return $this->belongsTo('Stefandebruin\PouleSystem\Models\Team', 'away_team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function result()
    {
        return $this->hasOne('Stefandebruin\PouleSystem\Models\Result', 'fixture_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function prediction()
    {
        $userId = array_get(request()->get('filter'), 'user_id', Auth::id());
        return $this->hasOne('Stefandebruin\PouleSystem\Models\Prediction', 'fixture_id')
            ->where('user_id', $userId);
    }

    /**
     * @param null $userId
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function predictionUser($userId = null)
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Prediction', 'fixture_id')
            ->where('user_id', $userId);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function predictions()
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Prediction', 'fixture_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function group()
    {
        return $this->hasMany('Stefandebruin\PouleSystem\Models\Group', 'id');
    }

    /**
     * @param $status
     * @return mixed
     */
    public function formatStatus($status)
    {
        return array_get($this->statusses, $status, 0);
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function getIdFromUrl($url)
    {
        $spit = explode("/", $url);
        return end($spit);
    }

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\HasOne
//     */
//    public function settings()
//    {
//        return $this->hasOne(FixtureSetting::class, 'competition_id', 'competition_id')
//            ->where('competition_phase.phase_id', '=', $this->phase_id);
//    }

//    /**
//     * @return array
//     */
//    public function getSettings()
//    {
////        $settings = DB::table('competition_phase')->where('competition_id', $this->competition_id)
////            ->where('phase_id', $this->phase_id)->first();
//
//        return [
//            'penalties' => false,
//        ];
//    }

    public function getSettingsAttribute(){
        return ['penalties' => false];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('fixtures.show', ['fixture' => $this->id]);
    }

    /**
     * TODO-stefan verplaatsen naar een service
     * @return bool
     */
    public function canSetPrediction()
    {
        $now = Carbon::now();
        if(!is_null($this->date)){
            $startDate = $this->date->subMinute(config('poulesystem.time_to_edit'));
            return $now->lessThan($startDate);
        }
        return false;
    }

    /**
     * @param mixed $value
     * @return Carbon|\Illuminate\Support\Carbon|mixed
     */
    protected function asDateTime($value)
    {

        // If this value is already a Carbon instance, we shall just return it as is.
        // This prevents us having to re-instantiate a Carbon instance when we know
        // it already is one, which wouldn't be fulfilled by the DateTime check.
        if ($value instanceof Carbon) {
            return $value;
        }

        // If the value is already a DateTime instance, we will just skip the rest of
        // these checks since they will be a waste of time, and hinder performance
        // when checking the field. We will just return the DateTime right away.
        if ($value instanceof DateTimeInterface) {
            return new Carbon(
                $value->format('Y-m-d H:i:s.u'),
                $value->getTimezone()
            );
        }

        // If this value is an integer, we will assume it is a UNIX timestamp's value
        // and format a Carbon object from this timestamp. This allows flexibility
        // when defining your date fields as they might be UNIX timestamps here.
        if (is_numeric($value)) {
            return Carbon::createFromTimestamp($value);
        }

        // If the value is in simply year, month, day format, we will instantiate the
        // Carbon instances from that format. Again, this provides for simple date
        // fields on the database, while still supporting Carbonized conversion.
        if ($this->isStandardDateFormat($value)) {
            return Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
        }

        // Finally, we will just assume this date is in the format used by default on
        // the database connection and use that format to create the Carbon object
        // that is returned back out to the developers after we convert it here.
        return Carbon::createFromFormat(
            str_replace('.v', '.u', $this->getDateFormat()),
            $value,
            'UTC'
        );
    }
}
