<?php namespace Stefandebruin\PouleSystem\Resources;
use App\Providers\JsonApiServiceProvider;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 26-12-17
 * Time: 20:27
 */

trait RelationTrait
{

    private $includes = [];

    private $resolved = false;

    public function getRelationships($request)
    {
        $parameters = str_plural(strtolower(class_basename($this->resource)));

        if (!property_exists($this, 'primaryRelations')) return [];

        $relations = $this->primaryRelations;

        $typeName = JsonApiServiceProvider::getTypeByClass(get_class($this->resource));
        $requestInclude = explode(",",$request->get('include'));
        $includeRelations = [];
        if(get_class($this->resource) === get_class(app()->make('rootResource'))){
            foreach ($requestInclude as $item){
                $includeRelations[] = array_first(explode(".", $item));
            }

            if(method_exists($this->resource, 'getPersistentRelations')){
                $includeRelations = array_merge($includeRelations, $this->resource->getPersistentRelations());
            }
        }else{
            $includeRelations = $this->getItemRelations($this->resource, $typeName, $requestInclude);
        }

        $relationData = [];
        foreach ($includeRelations as $relation) {
            if (!method_exists($this->resource, $relation)) continue;

            $data = [];

            $modelRelation = $relation;
            if (property_exists($this, 'renameRelations')) {
                $modelRelation = array_get($this->renameRelations, $relation, $relation);
            }

            if (!is_null($this->resource->$relation)) {
                $modelRelationData = $this->resource->$relation;
                if($modelRelationData instanceof Model){
                    $data = [
                        'type' => $modelRelation, 'id' => (string)$modelRelationData[$modelRelationData->getKeyName()]
                    ];
                }else{
                    foreach ($this->resource->$relation as $phase) {
                        $data[] = [
                            'type' => $modelRelation, 'id' => (string)$phase[$phase->getKeyName()]
                        ];
                    }
                }
            }

            $relationData[$relation] = [
                'links' => [
                    'self' => isset($this->links[$modelRelation]) ? $this->links[$modelRelation] : '',
                ],
                'data' => $data,
            ];
        }

        return $relationData;
    }

    public function with($request){
        if(ends_with(class_basename($this), 'Collection')){
            return $this->withCollection($request);
        }else{
            return $this->withSingle($request);
        }
    }


    public function withSingle($request)
    {
        $includes = [];
        $includeRelations = array_filter(
            explode(",",
                $request->get('include')
            ),
            function ($value) {
                return !empty($value);
            }
        );

        foreach ($includeRelations as $relation) {
            \Debugbar::error(get_class($this->resource));
            $this->resource->load($relation);
            foreach ($this->resource->getRelation($relation) as $relationItem) {
                if (!array_has($includes, $relation . '.' . $relationItem->id)) {
                    $class= $relationItem->getResourceClass();
                    $includes[$relation . '.' . $relationItem->id] = new $class($relationItem, false);
                }
            }
        }

        if (count($includes) > 0) {
            return ['included' => array_values($includes), 'weg' => 1234];
        }

        return [];
    }

    public function addField($request, $field)
    {
        $model = strtolower(class_basename(get_class($this->resource)));
        if ($request->has('fields')) {
            $fields = $request->get('fields');
            if (!array_has($fields, $model)) return true;

            $allFields = explode(",", $fields[$model]);

            return in_array($field, $allFields);
        }
        return true;
    }

    public function withCollection($request)
    {
        $includeRelations = array_filter(
            explode(",",
                $request->get('include')
            ),
            function ($value) {
                return !empty($value);
            }
        );

        $itemCollection = $this->collection->pluck('resource');

        $this->mapCollectionForRelations($itemCollection, $includeRelations);

        if (count($this->includes) > 0) {
            return ['included' => array_values($this->includes), 'weg' => 97897];
        }


        return [];
    }

    private function mapCollectionForRelations($resource, array $relations){
        foreach ($relations as $relation) {
            $fullIncludeRelation = $relation;
            $includeRelation = array_first(explode(".",$relation));

            if($resource instanceof Model){
                $this->includeRelation($includeRelation, $resource);
                $subRelations = $this->getItemRelations($resource, $includeRelation, $relations);
                $this->mapCollectionForRelations($resource, $subRelations);
            }else if($resource instanceof \Illuminate\Support\Collection){
                $subRelations = [];
                if(!is_null($resource->pluck($includeRelation)->first())){
                    $subRelations = $this->getItemRelations(
                        $resource->pluck($includeRelation)->first()->first(),
                        $includeRelation,
                        $relations
                    );
                }

                foreach ($resource->pluck($includeRelation) as $foundItems) {
                    $this->includeRelation($includeRelation, $foundItems);
                    $this->mapCollectionForRelations($foundItems, $subRelations);
                }
            }
        }
    }

    private function getItemRelations($resource, $currentRelation, $relations){
        $subRelations = [];
        foreach($relations as $key => $value){
            if(str_contains($value, $currentRelation) && str_contains($value, ".")){
                $last = explode($currentRelation, $value);
                if(!empty($last)) $subRelations[] = substr(array_last($last), 1);
            }
        }

        $persistentRelations = [];
        if(method_exists($resource, 'getPersistentRelations')){
            $persistentRelations = $resource->getPersistentRelations();
        }

        $subRelations = array_unique(array_merge($subRelations, $persistentRelations));

        return $subRelations;
    }

    private function includeRelation($relation, $items){
        if($items instanceof Model){
            $this->addIncludedItem($items);
        }else if ($items instanceof Collection){
            foreach ($items as $item) {
                $this->addIncludedItem($item);
            }
        }
    }

    private function addIncludedItem(Model $item) {
        $className = class_basename($item);
        if (!array_has($this->includes, $className . '.' . $item[$item->getKeyName()])) {

            if(!method_exists($item, 'getResourceClass')){
                throw new Exception('fucntion getResourceclass Oldis missing on ' . get_class($item));
            }

            $c = $item->getResourceClass();
            $this->includes[$className . '.' . $item[$item->getKeyName()]] = new $c($item);
        }
    }


    /**
     * Prepare the resource for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        if(!$this->resolved){
            $this->rootResource = false;
            if(!app()->bound('rootResource')){
                $this->rootResource = true;
                $class= clone $this->resource;
                app()->singleton('rootResource', function() use($class) {
                    return $class;
                });
            }
        }

        $this->resolved = true;
        return parent::jsonSerialize();
    }

    public function resolve($request = null){
        $block = ['Illuminate\Support\Collection'];
        if(!$this->resolved){
            $this->rootResource = 1;
            if(!app()->bound('rootResource') && !in_array(get_class($this->resource), $block)){
                $this->rootResource = true;
                $class= clone $this->resource;
                app()->singleton('rootResource', function() use($class) {
                    return $class;
                });
            }
        }
        $this->resolved = true;
        return parent::resolve($request);
    }
}