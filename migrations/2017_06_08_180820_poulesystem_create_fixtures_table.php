<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoulesystemCreateFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixtures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('api_id');

            $table->integer('competition_id')
                ->unsigned();
            $table->foreign('competition_id')
                ->references('id')
                ->on('competitions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('phase_id')
                ->unsigned();
            $table->foreign('phase_id')
                ->references('id')
                ->on('phases')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('matchday');
            $table->timestamp('date')->nullable();
            $table->tinyInteger('status');
            $table->integer('home_team')->unsigned();
            $table->foreign('home_team')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('away_team')->unsigned();
            $table->foreign('away_team')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixtures');
    }
}
