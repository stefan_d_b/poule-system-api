<?php namespace Stefandebruin\PouleSystem\Models;

/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 08-06-17
 * Time: 18:34
 */

use Illuminate\Database\Eloquent\Model;

/**
 * Stefandebruin\PouleSystem\Models\Translation
 *
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $itemable
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereItemableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereItemableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereTranslation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id
 * @property string $language
 * @property string $translation
 * @property int $itemable_id
 * @property string $itemable_type
 */
class Translation extends Model
{
    protected $fillable = [
        'translation',
        'language'
    ];
    public function itemable()
    {
        return $this->morphTo();
    }
}
