<?php

namespace Stefandebruin\PouleSystem\Resources;

use App\Http\Resources\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\PouleSystem\Facades\OverrideModels;

class Podium extends Resource
{
    /**
     * @var array
     */
    protected $primaryRelations = [
    ];

    protected $links = [];

    /**
     * @var bool
     */
    private $dataRoot;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param bool $dataRoot
     */
    public function __construct($resource, $dataRoot = true)
    {
        parent::__construct($resource);

        $this->dataRoot = $dataRoot;
    }

    public function toArray($request)
    {
        $relations = $this->getRelationships($request);
        return [
            'type' => 'podiums',
            'id' => (string)$request->competition->id,
            'attributes' => [
                'title' => 'Podium',
                'created_at' => Carbon::now()->toIso8601String(),
                'virtual' => false,
            ],
            'links' => [
                'self' => route('poulesystem.podium.show', ['competition' => $request->competition->id])
            ],
            'relationships' => $this->when(count($relations) > 0, $relations)
        ];
    }

    public function with($request)
    {
        $data = [];
        foreach ($this->resource->getRelation('podiumRows') as $row) {
            if (str_contains($request->get('include', ''), 'podiumRows')) {
                if (empty($row->getAttribute('user_id'))) continue;
                $data[] = new PodiumRow($row);
            }

//
            if (str_contains($request->get('include', ''), 'podiumRows.user')) {
                if (empty($row->getAttribute('user_id'))) continue;
                $data[] = new AbstractJsonApi($row->user());
            }

        }

        return ['included' => $data];
    }

    public function getRelationships($request)
    {
        $relations = [];
        foreach ($this->resource->getRelation('podiumRows') as $row) {
            if (empty($row->getAttribute('user_id'))) continue;
            $relations[] = ['type' => 'podiumRows', 'id' => sha1($request->competition->id . $row->user_id)];
        }
        return [
            'podiumRows' => [
                'links' => [
                    'self' => '',
                ],
                'data' => $relations,
            ]
        ];
    }

}
