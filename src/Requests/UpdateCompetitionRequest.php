<?php

namespace Stefandebruin\PouleSystem\Requests;

use App\Http\Requests\Request;
use Stefandebruin\JsonApi\Requests\JsonApiRequest;


class UpdateCompetitionRequest extends JsonApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function attributeRules()
    {
        return [
            'title' => ['translate'],
            'visible' => ['boolean'],
            'type' => ['in:1,2,3'],
        ];
    }
}
