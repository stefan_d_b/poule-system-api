<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 12-06-17
 * Time: 08:24
 */

namespace Stefandebruin\PouleSystem\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Kernel constructor.
     * @param Application $app
     * @param Dispatcher $events
     */
    public function __construct(
        Application $app,
        Dispatcher $events
    ) {
        parent::__construct($app, $events);
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('poulesystem:import:fixtures')
            ->name('import_fixtures')
            ->withoutOverlapping()
            ->hourlyAt(12);

        $schedule->command('poulesystem:import:officialResult')
            ->name('import_result')
            ->withoutOverlapping()
            ->everyFiveMinutes();
    }
}
