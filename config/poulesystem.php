<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 13-09-16
 * Time: 10:42
 */

return [
    'middleware' => ['api'], //, 'jwt-auth', 'jwt.auth',
    'prefix' => 'poulesystem',
    'api_key' => env('POULE_SYSTEM_API_KEY', ''),
    'time_to_edit' => 30,
    'enroll_competition' => false,

    'penaltyModes' => 'winner',

    'defaultProvider' => 'football-data',

    'gateways' => [
        'football-data' => \Stefandebruin\PouleSystem\Services\ApiGateway\FootballData::class,
    ],

    'points' => [
        'halfTime' => [
            'correct' => 2,
            'winner' => 1,
        ],
        'end' => [
            'correct' => 2,
            'winner' => 1,
        ],
        'penalties' => [
            'correct' => 2,
            'winner' => 1,
        ]
    ],

    'override' => [
        'models' => [
//            'Competition' => 'fds',
//            'Fixture' => '',
//            'Group' => '',
//            'Phase' => '',
//            'Score' => '',
//            'Team' => '',
        ],

        'notifications' => [
//            'NewTopUser' => '',
//            'UserEmptyScres' => '',
        ]
    ],


    'domain' => [
        'checkExist' => false, //TODO implement,
        'userConnected' => false,
        'block-routes' => [
            'login.token.callback',
            'login.token.request'
        ]
    ]
];