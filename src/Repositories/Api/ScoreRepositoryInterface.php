<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10-06-17
 * Time: 10:05
 */

namespace Stefandebruin\PouleSystem\Repositories\Api;


use Stefandebruin\PouleSystem\Models\Group;
use Stefandebruin\PouleSystem\Models\Score;

interface ScoreRepositoryInterface
{

    public function model($data = []): Score;

    public function save(Score $competition):Score;

    public function find(int $id);

    public function findBy(String $column, $value);

    public function getForUser($userId = null);

    public function getAllUsers($competitionId);
}